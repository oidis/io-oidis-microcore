# com-wui-framework-microcore v2019.2.0

> Engine that provides web browser with off-screen rendering based in Chromium Embedded Framework.

## Requirements

This application depends on the [WUI Builder](https://gitlab.com/oidis/io-oidis-builder).
See the WUI Builder requirements before you build this project.

## Project build

This project build is fully automated. For more information about the project build, 
see the [WUI Builder](https://gitlab.com/oidis/io-oidis-builder) documentation.

## Documentation

This project provides automatically generated documentation in [Doxygen](http://www.doxygen.org/index.html) 
from the C++ source by running the `docs` command.

> NOTE: The documentation is accessible also from the {projectRoot}/build/target/docs/index.html file after a successful creation.

## History

### v2018.0.6
Code synchronization between Windows and Linux implementation, code-base improvements.

## License

This software is owned or controlled by Oidis.
The use of this software is governed by the BSD-3-Clause Licence distributed with this material.
  
See the `LICENSE.txt` file for more details.

---

Author Karel Burda,
Copyright (c) 2018-2019 [NXP](http://nxp.com/)
Copyright (c) 2019 [Oidis](https://www.oidis.org/)

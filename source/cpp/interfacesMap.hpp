/** WARNING: this file has been automatically generated from C++ interfaces and classes, which exist in this package. */
// NOLINT (legal/copyright)

#ifndef IO_OIDIS_MICROCORE_INTERFACESMAP_HPP_  // NOLINT
#define IO_OIDIS_MICROCORE_INTERFACESMAP_HPP_

namespace Io {
    namespace Oidis {
        namespace Microcore {
            class Application;
            class Loader;
            namespace App {
                class Application;
                class Client;
            }
            namespace Bridges {
                class DragAndDropBridge;
            }
            namespace Commons {
                class BytesWriteHandler;
                class SharedLibraryManager;
            }
            namespace Configuration {
                class Configuration;
                class ProgramArguments;
            }
            namespace DragAndDrop {
                struct DragAndDrop;
                struct DragAndDrop;
                struct DragAndDropUnix;
                struct DragAndDrop;
                struct DragAndDropBase;
                class DragAndDropManager;
            }
            namespace Enums {
                struct PasteboardDataTypes;
                enum ProcessExitCodes : int;
            }
            namespace Exceptions {
                struct DragAndDropError;
                struct WindowManagerIsNotSet;
                struct KeyboardError;
                struct ProcedureNotExists;
                struct FailedToInitialize;
                struct IncorrectUserDataInformation;
                struct WindowNotExists;
            }
            namespace Handlers {
                class BrowserProcessHandler;
                class ContextMenuHandler;
                class DisplayHandler;
                class DragHandler;
                class LifeSpanHandler;
                class LoadHandler;
                class RenderHandler;
            }
            namespace Hardware {
                struct MouseData;
            }
            namespace Helpers {
            }
            namespace Interfaces {
                class IOsrDelegate;
            }
            namespace Logging {
            }
            namespace OpenGL {
                class OpenGLContext;
                class OpenGLContext;
                class OpenGLContext;
                class OpenGLContextBase;
            }
            namespace Renderers {
                class OsrRenderer;
            }
            namespace Runners {
                class Runner;
                class Runner;
                class RunnerUnix;
                class Runner;
                class RunnerBase;
            }
            namespace Structures {
                struct WindowDimensions;
            }
            namespace Types {
            }
            namespace Windows {
                class Window;
                class Window;
                class Window;
                class WindowBase;
                class WindowManager;
                class WindowManager;
                class WindowManager;
                class WindowManagerBase;
                namespace Native {
                }
            }
        }
    }
}

#endif  // IO_OIDIS_MICROCORE_INTERFACESMAP_HPP_  // NOLINT

/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_MICROCORE_HPP_  // NOLINT
#define IO_OIDIS_MICROCORE_HPP_

#include "../../dependencies/com-wui-framework-xcppcommons/source/cpp/reference.hpp"

#include "interfacesMap.hpp"
#include "Io/Oidis/Microcore/sourceFilesMap.hpp"

// generated-code-start
#include "Io/Oidis/Microcore/App/App.hpp"
#include "Io/Oidis/Microcore/App/Client.hpp"
#include "Io/Oidis/Microcore/Application.hpp"
#include "Io/Oidis/Microcore/Bridges/DragAndDropBridge.Windows.hpp"
#include "Io/Oidis/Microcore/Commons/BytesWriteHandler.OSX.hpp"
#include "Io/Oidis/Microcore/Commons/SharedLibraryManager.Windows.hpp"
#include "Io/Oidis/Microcore/Configuration/Configuration.hpp"
#include "Io/Oidis/Microcore/Configuration/Defaults.hpp"
#include "Io/Oidis/Microcore/Configuration/DefaultsCEF.hpp"
#include "Io/Oidis/Microcore/Configuration/DefaultsRenderer.hpp"
#include "Io/Oidis/Microcore/Configuration/DefaultsWindow.hpp"
#include "Io/Oidis/Microcore/Configuration/ProgramArguments.hpp"
#include "Io/Oidis/Microcore/DragAndDrop/DragAndDrop.hpp"
#include "Io/Oidis/Microcore/DragAndDrop/DragAndDrop.Linux.hpp"
#include "Io/Oidis/Microcore/DragAndDrop/DragAndDrop.OSX.hpp"
#include "Io/Oidis/Microcore/DragAndDrop/DragAndDrop.Unix.hpp"
#include "Io/Oidis/Microcore/DragAndDrop/DragAndDrop.Windows.hpp"
#include "Io/Oidis/Microcore/DragAndDrop/DragAndDropBase.hpp"
#include "Io/Oidis/Microcore/DragAndDrop/DragAndDropManager.Windows.hpp"
#include "Io/Oidis/Microcore/Enums/KeyboardCodes.Unix.hpp"
#include "Io/Oidis/Microcore/Enums/PasteboardDataTypes.OSX.hpp"
#include "Io/Oidis/Microcore/Enums/ProcessExitCodes.hpp"
#include "Io/Oidis/Microcore/Exceptions/DragAndDropExceptions.hpp"
#include "Io/Oidis/Microcore/Exceptions/DragAndDropManagerExceptions.hpp"
#include "Io/Oidis/Microcore/Exceptions/KeyboardExceptions.hpp"
#include "Io/Oidis/Microcore/Exceptions/SharedLibraryManagerExceptions.hpp"
#include "Io/Oidis/Microcore/Exceptions/WindowBaseExceptions.hpp"
#include "Io/Oidis/Microcore/Exceptions/WindowEventsExceptions.hpp"
#include "Io/Oidis/Microcore/Exceptions/WindowManagerBaseExceptions.hpp"
#include "Io/Oidis/Microcore/Handlers/BrowserProcessHandler.hpp"
#include "Io/Oidis/Microcore/Handlers/ContextMenuHandler.hpp"
#include "Io/Oidis/Microcore/Handlers/DisplayHandler.hpp"
#include "Io/Oidis/Microcore/Handlers/DragHandler.hpp"
#include "Io/Oidis/Microcore/Handlers/LifeSpanHandler.hpp"
#include "Io/Oidis/Microcore/Handlers/LoadHandler.hpp"
#include "Io/Oidis/Microcore/Handlers/RenderHandler.hpp"
#include "Io/Oidis/Microcore/Hardware/Keyboard.Linux.hpp"
#include "Io/Oidis/Microcore/Hardware/Keyboard.OSX.hpp"
#include "Io/Oidis/Microcore/Hardware/Keyboard.Windows.hpp"
#include "Io/Oidis/Microcore/Hardware/Mouse.hpp"
#include "Io/Oidis/Microcore/Hardware/Mouse.Windows.hpp"
#include "Io/Oidis/Microcore/Helpers/Utils.hpp"
#include "Io/Oidis/Microcore/Helpers/Utils.Linux.hpp"
#include "Io/Oidis/Microcore/Helpers/Utils.Windows.hpp"
#include "Io/Oidis/Microcore/Interfaces/IOsrDelegate.hpp"
#include "Io/Oidis/Microcore/Loader.hpp"
#include "Io/Oidis/Microcore/Logging/Logging.hpp"
#include "Io/Oidis/Microcore/OpenGL/OpenGL.hpp"
#include "Io/Oidis/Microcore/OpenGL/OpenGLContext.Linux.hpp"
#include "Io/Oidis/Microcore/OpenGL/OpenGLContext.OSX.hpp"
#include "Io/Oidis/Microcore/OpenGL/OpenGLContext.Windows.hpp"
#include "Io/Oidis/Microcore/OpenGL/OpenGLContextBase.hpp"
#include "Io/Oidis/Microcore/Renderers/OsrRenderer.hpp"
#include "Io/Oidis/Microcore/Runners/Runner.Linux.hpp"
#include "Io/Oidis/Microcore/Runners/Runner.OSX.hpp"
#include "Io/Oidis/Microcore/Runners/Runner.Unix.hpp"
#include "Io/Oidis/Microcore/Runners/Runner.Windows.hpp"
#include "Io/Oidis/Microcore/Runners/RunnerBase.hpp"
#include "Io/Oidis/Microcore/Structures/WindowDimensions.hpp"
#include "Io/Oidis/Microcore/Types/Aliases.OSX.hpp"
#include "Io/Oidis/Microcore/Types/WindowHandles.hpp"
#include "Io/Oidis/Microcore/Types/WindowHandles.Linux.hpp"
#include "Io/Oidis/Microcore/Types/WindowHandles.OSX.hpp"
#include "Io/Oidis/Microcore/Types/WindowHandles.Windows.hpp"
#include "Io/Oidis/Microcore/Windows/Native/NativeOpenGLView.OSX.hpp"
#include "Io/Oidis/Microcore/Windows/Native/NativeWindow.OSX.hpp"
#include "Io/Oidis/Microcore/Windows/Window.Linux.hpp"
#include "Io/Oidis/Microcore/Windows/Window.OSX.hpp"
#include "Io/Oidis/Microcore/Windows/Window.Windows.hpp"
#include "Io/Oidis/Microcore/Windows/WindowBase.hpp"
#include "Io/Oidis/Microcore/Windows/WindowManager.Linux.hpp"
#include "Io/Oidis/Microcore/Windows/WindowManager.OSX.hpp"
#include "Io/Oidis/Microcore/Windows/WindowManager.Windows.hpp"
#include "Io/Oidis/Microcore/Windows/WindowManagerBase.hpp"
// generated-code-end
#endif  // IO_OIDIS_MICROCORE_HPP_  NOLINT

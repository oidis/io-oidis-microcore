/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014 The Chromium Embedded Framework Authors
 * Copyright (c) 2018 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifdef WIN_PLATFORM

#include "../sourceFilesMap.hpp"

namespace Io::Oidis::Microcore::OpenGL {
    OpenGLContext::OpenGLContext(HDC $hdc, HGLRC $hglrc, const bool $swapBuffers)
            : OpenGLContextBase($swapBuffers),
              hdc($hdc) {
        wglMakeCurrent($hdc, $hglrc);
    }

    OpenGLContext::~OpenGLContext() {
        wglMakeCurrent(nullptr, nullptr);

        if (this->swapBuffers) {
            SwapBuffers(this->hdc);
        }
    }
}

#endif  // WIN_PLATFORM

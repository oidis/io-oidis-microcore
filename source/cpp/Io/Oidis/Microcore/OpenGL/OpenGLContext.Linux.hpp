/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014 The Chromium Embedded Framework Authors
 * Copyright (c) 2018 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_MICROCORE_OPENGL_OPENGLCONTEXT_LINUX_HPP_
#define IO_OIDIS_MICROCORE_OPENGL_OPENGLCONTEXT_LINUX_HPP_

#ifdef LINUX_PLATFORM

#include <gtk/gtk.h>
#include <gtk/gtkgl.h>

#include "../OpenGL/OpenGLContextBase.hpp"

namespace Io::Oidis::Microcore::OpenGL {
    class OpenGLContext : public OpenGLContextBase {
     public:
        OpenGLContext(PaintingAreaHandle $widget, bool $swapBuffers);

        ~OpenGLContext();

        bool isValid() const;

     private:
        GdkGLDrawable *glDrawable = nullptr;
        bool valid = false;
    };
}

#endif  // LINUX_PLATFORM

#endif  // IO_OIDIS_MICROCORE_OPENGL_OPENGLCONTEXT_LINUX_HPP_

/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014 The Chromium Embedded Framework Authors
 * Copyright (c) 2018 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifdef MAC_PLATFORM

#include "../sourceFilesMap.hpp"

namespace Io::Oidis::Microcore::OpenGL {
    OpenGLContext::OpenGLContext(PaintingAreaHandle $view, const bool $swapBuffers)
            : OpenGLContextBase($swapBuffers) {
        this->context = [$view openGLContext];
        [this->context makeCurrentContext];
    }

    OpenGLContext::~OpenGLContext() {
        [NSOpenGLContext clearCurrentContext];

        if (this->swapBuffers) {
            [this->context flushBuffer];
        }
    }
}

#endif  // MAC_PLATFORM

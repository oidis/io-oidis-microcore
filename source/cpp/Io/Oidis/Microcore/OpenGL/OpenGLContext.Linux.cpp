/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014 The Chromium Embedded Framework Authors
 * Copyright (c) 2018 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifdef LINUX_PLATFORM

#include <GL/gl.h>

#include "../sourceFilesMap.hpp"

namespace Io::Oidis::Microcore::OpenGL {
    OpenGLContext::OpenGLContext(PaintingAreaHandle $widget, const bool $swapBuffers)
            : OpenGLContextBase($swapBuffers) {
        GdkGLContext *glContext = gtk_widget_get_gl_context($widget);
        this->glDrawable = gtk_widget_get_gl_drawable($widget);
        this->valid = (gdk_gl_drawable_gl_begin(this->glDrawable, glContext) != 0);
    }

    OpenGLContext::~OpenGLContext() {
        if (this->valid) {
            gdk_gl_drawable_gl_end(this->glDrawable);

            if (swapBuffers) {
                if (gdk_gl_drawable_is_double_buffered(this->glDrawable) != 0) {
                    gdk_gl_drawable_swap_buffers(this->glDrawable);
                }
            } else {
                glFlush();
            }
        }
    }

    bool OpenGLContext::isValid() const {
        return this->valid;
    }
}

#endif  // LINUX_PLATFORM

/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_MICROCORE_OPENGL_OPENGLCONTEXTBASE_HPP_
#define IO_OIDIS_MICROCORE_OPENGL_OPENGLCONTEXTBASE_HPP_

#include "../Types/WindowHandles.hpp"

namespace Io::Oidis::Microcore::OpenGL {
    class OpenGLContextBase {
     protected:
        using PaintingAreaHandle = Io::Oidis::Microcore::Types::PaintingAreaHandle;

        explicit OpenGLContextBase(bool $swapBuffers);

        bool swapBuffers = false;
    };
}

#endif  // IO_OIDIS_MICROCORE_OPENGL_OPENGLCONTEXTBASE_HPP_

/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_MICROCORE_OPENGL_OPENGL_HPP_
#define IO_OIDIS_MICROCORE_OPENGL_OPENGL_HPP_

#ifdef WIN_PLATFORM
#   include <gl/gl.h>
#elif MAC_PLATFORM
#   import <OpenGL/gl.h>
#elif LINUX_PLATFORM
#   include <GL/gl.h>
#else
#   error Not implemented on this platform
#endif

#endif  // IO_OIDIS_MICROCORE_OPENGL_OPENGL_HPP_

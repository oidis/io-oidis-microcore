/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_MICROCORE_TYPES_WINDOWHANDLES_HPP_
#define IO_OIDIS_MICROCORE_TYPES_WINDOWHANDLES_HPP_

#ifdef WIN_PLATFORM
#include "../Types/WindowHandles.Windows.hpp"
#elif LINUX_PLATFORM
#include "../Types/WindowHandles.Linux.hpp"
#elif MAC_PLATFORM
#include "../Types/WindowHandles.OSX.hpp"
#endif

#endif  // IO_OIDIS_MICROCORE_TYPES_WINDOWHANDLES_HPP_

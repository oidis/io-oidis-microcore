/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_MICROCORE_TYPES_WINDOWHANDLES_OSX_HPP_
#define IO_OIDIS_MICROCORE_TYPES_WINDOWHANDLES_OSX_HPP_

#ifdef MAC_PLATFORM

#import <AppKit/NSWindow.h>

#import "../Types/Aliases.OSX.hpp"

namespace Io::Oidis::Microcore::Types {
    using WindowHandle =  NSWindow *;
    using PaintingAreaHandle = NativeOpenGLView *;
}

#endif  // MAC_PLATFORM

#endif  // IO_OIDIS_MICROCORE_TYPES_WINDOWHANDLES_OSX_HPP_

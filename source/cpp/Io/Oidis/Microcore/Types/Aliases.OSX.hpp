/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_MICROCORE_TYPES_ALIASES_OSX_HPP_
#define IO_OIDIS_MICROCORE_TYPES_ALIASES_OSX_HPP_

#ifdef MAC_PLATFORM

#import "../Hardware/Keyboard.OSX.hpp"
#import "../Windows/Native/NativeWindow.OSX.hpp"
#import "../Windows/Native/NativeOpenGLView.OSX.hpp"

@compatibility_alias NativeWindow ComWuiFrameworkMicrocoreWindowsNative_NativeWindow;
@compatibility_alias NativeOpenGLView ComWuiFrameworkMicrocoreWindowsNative_NativeOpenGlView;
@compatibility_alias Keyboard ComWuiFrameworkMicrocoreHardware_Keyboard;

#endif  // MAC_PLATFORM

#endif  // IO_OIDIS_MICROCORE_TYPES_ALIASES_OSX_HPP_

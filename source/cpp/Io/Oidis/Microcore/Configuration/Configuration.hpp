/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_MICROCORE_CONFIGURATION_CONFIGURATION_HPP_
#define IO_OIDIS_MICROCORE_CONFIGURATION_CONFIGURATION_HPP_

#include "../Structures/WindowDimensions.hpp"

namespace Io::Oidis::Microcore::Configuration {
    class Configuration {
        using ProgramArguments = Io::Oidis::Microcore::Configuration::ProgramArguments;
        using WindowDimensions = Io::Oidis::Microcore::Structures::WindowDimensions;

     public:
        static Configuration &GetInstance();

        Configuration() = default;

        ~Configuration() = default;

        Configuration(const Configuration &) = delete;

        Configuration(Configuration &&) = delete;

        Configuration &operator=(const Configuration &) = delete;

        Configuration &operator=(Configuration &&) = delete;

        void Override(ProgramArguments $arguments);

        void ResolveDependencies();

        struct Window {
            WindowDimensions dimensions;
            string id = Io::Oidis::Microcore::Configuration::Defaults::Window::GetId();
            string url = Io::Oidis::Microcore::Configuration::Defaults::Window::GetURL();
            bool visible = Io::Oidis::Microcore::Configuration::Defaults::Window::IsVisible();
            bool decorated = Io::Oidis::Microcore::Configuration::Defaults::Window::IsDecorated();
            bool transparent = Io::Oidis::Microcore::Configuration::Defaults::Window::IsTransparent();
        };

        struct Renderer {
            cef_color_t backgroundColor = Io::Oidis::Microcore::Configuration::Defaults::Renderer::GetBackgroundColor(false);
        };

        struct CEF {
            struct Global {
                bool singleProcess = Io::Oidis::Microcore::Configuration::Defaults::CEF::Global::IsSingleProcess();
                int frameRate = Io::Oidis::Microcore::Configuration::Defaults::CEF::Global::GetFrameRate();
            };

            struct Cache {
                string path = Io::Oidis::XCppCommons::System::IO::FileSystem::getLocalAppDataPath() +
                              "/" + Io::Oidis::Microcore::Configuration::Defaults::CEF::Cache::GetDirectoryName() + "/" +
                              Io::Oidis::XCppCommons::EnvironmentArgs::getInstance().getProjectName();
            };

            struct Log {
                string path = Io::Oidis::Microcore::Configuration::Defaults::CEF::Log::GetPath();
                cef_log_severity_t severity = Io::Oidis::Microcore::Configuration::Defaults::CEF::Log::GetSeverity();
            };

            struct Debugging {
                int remotePort = Io::Oidis::Microcore::Configuration::Defaults::CEF::Debugging::GetPort();
            };

            Global global;
            Cache cache;
            Log log;
            Debugging debugging;
        };

        Window getWindow() const;

        Renderer getRenderer() const;

        CEF getCEF() const;

     private:
        Window window;
        Renderer renderer;
        CEF cef;
    };
}

#endif  // IO_OIDIS_MICROCORE_CONFIGURATION_CONFIGURATION_HPP_

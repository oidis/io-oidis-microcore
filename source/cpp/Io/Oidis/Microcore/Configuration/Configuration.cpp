/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

namespace Io::Oidis::Microcore::Configuration {
    using ProgramArguments = Io::Oidis::Microcore::Configuration::ProgramArguments;

    Configuration &Configuration::GetInstance() {
        static Configuration instance;

        return instance;
    }

    void Configuration::Override(ProgramArguments $arguments) {
        this->window.url = $arguments.getUrl();
        this->window.visible = $arguments.getIsWindowVisible();
        this->window.decorated = $arguments.getIsWindowDecorated();
    }

    void Configuration::ResolveDependencies() {
        if (this->window.transparent) {
            this->renderer.backgroundColor = Defaults::Renderer::GetBackgroundColor(true);
        }
    }

    Configuration::Window Configuration::getWindow() const {
        return this->window;
    }

    Configuration::Renderer Configuration::getRenderer() const {
        return this->renderer;
    }

    Configuration::CEF Configuration::getCEF() const {
        return this->cef;
    }
}

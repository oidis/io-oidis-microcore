/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_MICROCORE_CONFIGURATION_DEFAULTSWINDOW_HPP_
#define IO_OIDIS_MICROCORE_CONFIGURATION_DEFAULTSWINDOW_HPP_

namespace Io::Oidis::Microcore::Configuration::Defaults {
    namespace Window {
        constexpr const char *GetURL() {
            return "https://google.com";
        }

        constexpr const char *GetId() {
            return "com-wui-framework-microcore-window";
        }

        constexpr bool IsDecorated() {
            return false;
        }

        constexpr bool IsVisible() {
            return true;
        }

        constexpr bool IsTransparent() {
#ifdef LINUX_PLATFORM
            return false;
#else
            return true;
#endif
        }

        constexpr int GetWidth() {
            return 1024;
        }

        constexpr int GetHeight() {
            return 768;
        }
    }
}

#endif  // IO_OIDIS_MICROCORE_CONFIGURATION_DEFAULTSWINDOW_HPP_

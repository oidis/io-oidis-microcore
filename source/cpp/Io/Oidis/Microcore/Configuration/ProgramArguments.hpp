/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_MICROCORE_CONFIGURATION_PROGRAMARGUMENTS_HPP_
#define IO_OIDIS_MICROCORE_CONFIGURATION_PROGRAMARGUMENTS_HPP_

#include "../Configuration/Defaults.hpp"

namespace Io::Oidis::Microcore::Configuration {
    class ProgramArguments
            : public Io::Oidis::XCppCommons::Structures::ProgramArgs {
     public:
        ProgramArguments();

        bool getIsSingleProcess() const;

        bool getIsWindowVisible() const;

        bool getIsWindowDecorated() const;

        string getUrl() const;

     private:
        bool singleProcess = Io::Oidis::Microcore::Configuration::Defaults::CEF::Global::IsSingleProcess();
        bool windowVisible = Io::Oidis::Microcore::Configuration::Defaults::Window::IsVisible();
        bool windowDecorated = Io::Oidis::Microcore::Configuration::Defaults::Window::IsDecorated();
        string url = Io::Oidis::Microcore::Configuration::Defaults::Window::GetURL();
    };
}

#endif  // IO_OIDIS_MICROCORE_CONFIGURATION_PROGRAMARGUMENTS_HPP_

/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_MICROCORE_CONFIGURATION_DEFAULTSCEF_HPP_
#define IO_OIDIS_MICROCORE_CONFIGURATION_DEFAULTSCEF_HPP_

namespace Io::Oidis::Microcore::Configuration::Defaults {
    namespace CEF {
        namespace Global {
            constexpr bool IsSingleProcess() {
#ifdef MAC_PLATFORM
                return true;
#else
                return false;
#endif
            }

            constexpr int GetFrameRate() {
                return 40;
            }
        }

        namespace Cache {
            constexpr const char *GetDirectoryName() {
                return "WUIFramework";
            }
        }

        namespace Log {
            constexpr const char *GetPath() {
                return "Chromium.log";
            }

            constexpr cef_log_severity_t GetSeverity() {
                return cef_log_severity_t::LOGSEVERITY_VERBOSE;
            }
        }

        namespace Debugging {
            constexpr int GetPort() {
                return 50023;
            }
        }
    }
}

#endif  // IO_OIDIS_MICROCORE_CONFIGURATION_DEFAULTSCEF_HPP_

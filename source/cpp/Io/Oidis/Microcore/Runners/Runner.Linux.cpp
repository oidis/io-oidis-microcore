/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifdef LINUX_PLATFORM

#include <gtk/gtk.h>
#include <gtk/gtkgl.h>

#include "../sourceFilesMap.hpp"

namespace Io::Oidis::Microcore::Runners {
    RunnerBase::ProcessExitCode Runner::initialize() {
        gtk_init(&this->argc, const_cast<char ***>(&this->argv));
        gtk_gl_init(&this->argc, const_cast<char ***>(&this->argv));

        return RunnerBase::initialize();
    }
}

#endif  // LINUX_PLATFORM

/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_MICROCORE_RUNNERS_RUNNER_WINDOWS_HPP_
#define IO_OIDIS_MICROCORE_RUNNERS_RUNNER_WINDOWS_HPP_

#ifdef WIN_PLATFORM

#include "../Runners/RunnerBase.hpp"

namespace Io::Oidis::Microcore::Runners {
    class Runner : public RunnerBase {
        using RunnerBase::RunnerBase;

     private:
        void initializeCefMainArgs() override;
    };
}

#endif  // WIN_PLATFORM

#endif  // IO_OIDIS_MICROCORE_RUNNERS_RUNNER_WINDOWS_HPP_

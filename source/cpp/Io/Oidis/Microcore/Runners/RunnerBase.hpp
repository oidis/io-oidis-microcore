/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_MICROCORE_RUNNERS_RUNNERBASE_HPP_
#define IO_OIDIS_MICROCORE_RUNNERS_RUNNERBASE_HPP_

namespace Io::Oidis::Microcore::Runners {
    class RunnerBase {
     public:
        using ProcessExitCode = int;
        using ArgumentParsingResult = int;

        RunnerBase(int $argc, const char **$argv);

        ProcessExitCode Run();

     protected:
        virtual ProcessExitCode initialize();

        void runMessageLoop();

        void shutdown();

        ProcessExitCode launchSecondaryProcess();

        ArgumentParsingResult parseArguments();

        virtual void initializeCefMainArgs() = 0;

        CefSettings createCefSettings() const;

        int argc = 0;
        const char **argv = nullptr;
        Io::Oidis::Microcore::Configuration::ProgramArguments programArguments;
        CefMainArgs cefMainArgs;
    };
}

#endif  // IO_OIDIS_MICROCORE_RUNNERS_RUNNERBASE_HPP_

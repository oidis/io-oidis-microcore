/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include <include/cef_version.h>

#include "../sourceFilesMap.hpp"

namespace Io::Oidis::Microcore::Runners {
    using Io::Oidis::XCppCommons::Enums::LogLevel;
    using Io::Oidis::XCppCommons::EnvironmentArgs;
    using Io::Oidis::XCppCommons::System::IO::FileSystem;
    using Io::Oidis::XCppCommons::Utils::ArgsParser;
    using Io::Oidis::XCppCommons::Utils::LogIt;

    using Io::Oidis::Microcore::App::Application;
    using Io::Oidis::Microcore::Configuration::Configuration;
    using Io::Oidis::Microcore::Enums::ProcessExitCodes;
    using Io::Oidis::Microcore::Configuration::ProgramArguments;

    namespace fs = boost::filesystem;
    namespace Logging = Io::Oidis::Microcore::Logging;

    RunnerBase::RunnerBase(const int $argc, const char **$argv)
            : argc{$argc},
              argv{$argv} {
    }

    RunnerBase::ProcessExitCode RunnerBase::Run() {
        const auto processExitCode = this->initialize();

        if (processExitCode == ProcessExitCodes::SUCCESS) {
            this->runMessageLoop();
            this->shutdown();
        }

        LogIt::Info("Going to exit with code {0}", processExitCode);

        return processExitCode;
    }

    RunnerBase::ProcessExitCode RunnerBase::initialize() {
        LogIt::Info("Initializing");

        RunnerBase::ProcessExitCode exitCodeToReturn = ProcessExitCodes::DEFAULT;
        this->initializeCefMainArgs();
        const auto secondaryProcessExitCode = this->launchSecondaryProcess();

        if (secondaryProcessExitCode == ProcessExitCodes::BROWSER_PROCESS_SPAWN) {
            LogIt::Info("Running on the CEF {0}", string{CEF_VERSION});

            if (this->parseArguments() == 0) {
                Logging::RemoveLogFromCEF();

                CefRefPtr<Application> wrappedApp{new Application{std::move(programArguments)}};

                exitCodeToReturn = CefInitialize(std::move(this->cefMainArgs), createCefSettings(), wrappedApp.get(), nullptr) ?
                                   ProcessExitCodes::SUCCESS : ProcessExitCodes::CEF_INITIALIZATION_FAILED;
            } else {
                exitCodeToReturn = ProcessExitCodes::INCORRECT_ARGUMENTS;
            }
        } else {
            exitCodeToReturn = secondaryProcessExitCode;
        }

        return exitCodeToReturn;
    }

    void RunnerBase::runMessageLoop() {
        LogIt::Info("Running message loop");

        CefRunMessageLoop();
    }

    void RunnerBase::shutdown() {
        LogIt::Info("Shutting down");

        CefShutdown();
    }

    RunnerBase::ProcessExitCode RunnerBase::launchSecondaryProcess() {
        LogIt::Info("Launching secondary process");

        CefEnableHighDPISupport();

        return CefExecuteProcess(this->cefMainArgs, nullptr, nullptr);
    }

    RunnerBase::ArgumentParsingResult RunnerBase::parseArguments() {
        LogIt::Info("Going to parse arguments");

        const auto parseResult = ArgsParser::Parse(this->programArguments, this->argc, this->argv);

        if (parseResult == -1) {
            LogIt::Error("Incorrect arguments");

            this->programArguments.PrintHelp();
        } else if (parseResult == 1) {
            LogIt::Debug("Print help");
        }

        return parseResult;
    }

    CefSettings RunnerBase::createCefSettings() const {
        CefSettings cefSettings;

        cefSettings.windowless_rendering_enabled = true;
        cefSettings.no_sandbox = true;
        cefSettings.log_severity = Configuration::GetInstance().getCEF().log.severity;
        CefString(&cefSettings.log_file).FromString(Configuration::GetInstance().getCEF().log.path);
        cefSettings.remote_debugging_port = Configuration::GetInstance().getCEF().debugging.remotePort;
        if (this->programArguments.getIsSingleProcess()) {
            cefSettings.single_process = 1;
        }

        const auto cachePath = fs::path(Configuration::GetInstance().getCEF().cache.path);

        try {
            if (!FileSystem::Exists(cachePath.string())) {
                FileSystem::CreateDirectory(cachePath.string());
            }

            if (fs::is_directory(cachePath)) {
                LogIt::Info("Setting cache and user data path to {0}", cachePath.string());

                CefString(&cefSettings.cache_path) = string(cachePath.string());
                CefString(&cefSettings.user_data_path) = string(cachePath.string());
                cefSettings.persist_session_cookies = 1;
                cefSettings.persist_user_preferences = 1;
            } else {
                LogIt::Error("Can not create cache path at " + cachePath.string());
            }
        }
        catch (const fs::filesystem_error &exception) {
            LogIt::Error("Failed to create a cache path because of: {0}", exception.what());
        }

        return cefSettings;
    }
}

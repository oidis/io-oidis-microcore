/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifdef WIN_PLATFORM

#include <MSVC/MicrocoreMSVC.hpp> // NOLINT

#include "../sourceFilesMap.hpp"

namespace Io::Oidis::Microcore::DragAndDrop {
    using Io::Oidis::XCppCommons::Enums::LogLevel;
    using Io::Oidis::XCppCommons::Utils::LogIt;

    using Io::Oidis::Microcore::Windows::Window;
    using Io::Oidis::Microcore::Windows::WindowManager;

    namespace Exceptions = Io::Oidis::Microcore::Exceptions::DragAndDropManager;

    WindowManager *DragAndDropManager::windowManager = nullptr;
    std::mutex DragAndDropManager::concurrencyProtection = {};

    void DragAndDropManager::SetWindowManager(WindowManager *$windowManager) {
        std::lock_guard<decltype(DragAndDropManager::concurrencyProtection)> criticalSection{DragAndDropManager::concurrencyProtection};

        DragAndDropManager::windowManager = $windowManager;
    }

    CefBrowserHost::DragOperationsMask DragAndDropManager::OnDragEnter(const char *$id,
                                                                       const MSVC::Structures::DragData $dragData,
                                                                       const int $x,
                                                                       const int $y,
                                                                       const uint32 $modifiers,
                                                                       const CefBrowserHost::DragOperationsMask $effect) {
        std::lock_guard<decltype(DragAndDropManager::concurrencyProtection)> criticalSection{concurrencyProtection};

        LogIt::Debug("OnDragEnter with id {0}", $id);

        DragAndDropManager::assertWindowManager();

        const CefMouseEvent ev = MSVC::Structures::CreateCefMouseEvent($x, $y, $modifiers);
        const CefRefPtr<CefDragData> cefDragData = MSVC::Structures::CreateDragData($dragData);

        return DragAndDropManager::windowManager->GetWindow($id)->DragBeginEvent(ev, cefDragData, $effect);
    }

    CefBrowserHost::DragOperationsMask DragAndDropManager::OnDragOver(const char *$id,
                                                                      const int $x,
                                                                      const int $y,
                                                                      const int $modifiers,
                                                                      const CefBrowserHost::DragOperationsMask $effect) {
        std::lock_guard<decltype(DragAndDropManager::concurrencyProtection)> criticalSection{concurrencyProtection};

        LogIt::Debug("OnDragOver with id {0}", $id);

        DragAndDropManager::assertWindowManager();

        const CefMouseEvent event = MSVC::Structures::CreateCefMouseEvent($x, $y, $modifiers);

        return DragAndDropManager::windowManager->GetWindow($id)->DragMotionEvent(event, $effect);
    }

    void DragAndDropManager::OnDragLeave(const char *$id) {
        std::lock_guard<decltype(DragAndDropManager::concurrencyProtection)> criticalSection{concurrencyProtection};

        LogIt::Debug("OnDragLeave with id {0}", $id);

        DragAndDropManager::assertWindowManager();

        DragAndDropManager::windowManager->GetWindow($id)->DragLeaveEvent();
    }

    CefBrowserHost::DragOperationsMask DragAndDropManager::OnDrop(const char *$id,
                                                                  const int $x,
                                                                  const int $y,
                                                                  const int $modifiers,
                                                                  const CefBrowserHost::DragOperationsMask $effect) {
        std::lock_guard<decltype(DragAndDropManager::concurrencyProtection)> criticalSection{concurrencyProtection};

        LogIt::Debug("OnDrop with id {0}", $id);

        DragAndDropManager::assertWindowManager();

        const CefMouseEvent event = MSVC::Structures::CreateCefMouseEvent($x, $y, $modifiers);

        return DragAndDropManager::windowManager->GetWindow($id)->DragDropEvent(event, $effect);
    }

    void DragAndDropManager::assertWindowManager() {
        if (DragAndDropManager::windowManager == nullptr) {
            throw Exceptions::WindowManagerIsNotSet{};
        }
    }
}

#endif  // WIN_PLATFORM

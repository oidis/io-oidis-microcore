/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_MICROCORE_DRAGANDDROP_DRAGANDDROP_OSX_HPP_
#define IO_OIDIS_MICROCORE_DRAGANDDROP_DRAGANDDROP_OSX_HPP_

#ifdef MAC_PLATFORM

#import <AppKit/NSDragging.h>

#include "../DragAndDrop/DragAndDrop.Unix.hpp"

namespace Io::Oidis::Microcore::DragAndDrop {
    struct DragAndDrop : DragAndDropUnix {
        void Reset() override;

        void FillPasteboard(id $owner);

        NSArray *GetDraggedTypes() const;

        NSDragOperation operation = NSDragOperationNone;
        NSDragOperation allowedOperations = NSDragOperationNone;
        NSPasteboard *pasteboard = nil;
        CFStringRef fileUTI = nil;
    };
}

#endif  // MAC_PLATFORM

#endif  // IO_OIDIS_MICROCORE_DRAGANDDROP_DRAGANDDROP_OSX_HPP_

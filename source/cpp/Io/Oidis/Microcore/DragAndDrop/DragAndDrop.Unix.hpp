/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_MICROCORE_DRAGANDDROP_DRAGANDDROP_UNIX_HPP_
#define IO_OIDIS_MICROCORE_DRAGANDDROP_DRAGANDDROP_UNIX_HPP_

#if defined(LINUX_PLATFORM) || defined(MAC_PLATFORM)

#include "../DragAndDrop/DragAndDropBase.hpp"

namespace Io::Oidis::Microcore::DragAndDrop {
    struct DragAndDropUnix : DragAndDropBase {
        virtual void Reset() = 0;

        CefRefPtr<CefDragData> data = nullptr;
    };
}

#endif  // defined(LINUX_PLATFORM) || defined(MAC_PLATFORM)

#endif  // IO_OIDIS_MICROCORE_DRAGANDDROP_DRAGANDDROP_UNIX_HPP_

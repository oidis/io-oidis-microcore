/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#if defined(LINUX_PLATFORM) || defined(MAC_PLATFORM)

#include "../sourceFilesMap.hpp"

namespace Io::Oidis::Microcore::DragAndDrop {
    void DragAndDropUnix::Reset() {
        this->data = nullptr;
    }
}

#endif  // defined(LINUX_PLATFORM) || defined(MAC_PLATFORM)

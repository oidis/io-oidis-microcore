/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifdef LINUX_PLATFORM

#include <include/wrapper/cef_helpers.h>

#include "../sourceFilesMap.hpp"

namespace Io::Oidis::Microcore::DragAndDrop {
    void DragAndDrop::Reset() {
        DragAndDropUnix::Reset();

        CEF_REQUIRE_UI_THREAD();

        if (this->triggerEvent != nullptr) {
            gdk_event_free(this->triggerEvent);
            this->triggerEvent = nullptr;
        }

        this->operation = CefRenderHandler::DragOperation::DRAG_OPERATION_NONE;

        if (this->context != nullptr) {
            g_object_unref(this->context);
            this->context = nullptr;
        }

        this->isLeave = false;
        this->isDrop = false;
    }
}

#endif  // LINUX_PLATFORM

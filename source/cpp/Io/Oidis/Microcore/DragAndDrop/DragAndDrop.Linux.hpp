/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_MICROCORE_DRAGANDDROP_DRAGANDDROP_LINUX_HPP_
#define IO_OIDIS_MICROCORE_DRAGANDDROP_DRAGANDDROP_LINUX_HPP_

#ifdef LINUX_PLATFORM

#include <gdk/gdk.h>
#include <gtk/gtk.h>

#include "../DragAndDrop/DragAndDrop.Unix.hpp"

namespace Io::Oidis::Microcore::DragAndDrop {
    struct DragAndDrop : DragAndDropUnix {
        void Reset() override;

        GdkEvent *triggerEvent = nullptr;
        GdkDragContext *context = nullptr;
        GtkTargetList *targets = gtk_target_list_new(nullptr, 0);
        bool isLeave = false;
        bool isDrop = false;
    };
}

#endif  // LINUX_PLATFORM

#endif  // IO_OIDIS_MICROCORE_DRAGANDDROP_DRAGANDDROP_LINUX_HPP_

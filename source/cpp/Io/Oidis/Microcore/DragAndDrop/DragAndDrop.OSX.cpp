/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifdef MAC_PLATFORM

#include <include/cef_parser.h>

#include "../sourceFilesMap.hpp"

namespace Io::Oidis::Microcore::DragAndDrop {
    using Io::Oidis::Microcore::Enums::PasteboardDataTypes;

    void DragAndDrop::Reset() {
        DragAndDropUnix::Reset();

        this->operation = NSDragOperationNone;
        this->allowedOperations = NSDragOperationNone;

        if (this->fileUTI != nil) {
            CFRelease(this->fileUTI);
            this->fileUTI = nil;
        }
        if (this->pasteboard) {
            [this->pasteboard release];
            this->pasteboard = nil;
        }
    }

    void DragAndDrop::FillPasteboard(id $owner) {
        this->pasteboard = [[NSPasteboard pasteboardWithName:NSDragPboard] retain];

        [this->pasteboard declareTypes:@[PasteboardDataTypes::GetString(PasteboardDataTypes::DUMMY)] owner:$owner];

        if (this->data->IsLink()) {
            [this->pasteboard addTypes:@[NSURLPboardType, PasteboardDataTypes::GetString(PasteboardDataTypes::URL_TITLE)]
                              owner:$owner];
        }

        const size_t contentSize = this->data->GetFileContents(nullptr);
        const CefString downloadMetadata = this->data->GetLinkMetadata();
        const CefString fileName = this->data->GetFileName();

        if (contentSize > 0) {
            const auto fileName = this->data->GetFileName().ToString();
            const auto separator = fileName.rfind('.');

            if (separator != std::string::npos) {
                const CefString extension = fileName.substr(separator + 1);

                CefString mimeType = CefGetMimeType(extension);

                if (!mimeType.empty()) {
                    CFStringRef mimeTypeCF;
                    mimeTypeCF = CFStringCreateWithCString(kCFAllocatorDefault,
                                                           mimeType.ToString().c_str(),
                                                           kCFStringEncodingUTF8);
                    this->fileUTI = UTTypeCreatePreferredIdentifierForTag(kUTTagClassMIMEType, mimeTypeCF, nil);
                    CFRelease(mimeTypeCF);

                    NSArray *fileUTIList = @[static_cast<NSString *>(this->fileUTI)];

                    [this->pasteboard addTypes:@[NSFilesPromisePboardType] owner:$owner];
                    [this->pasteboard setPropertyList:fileUTIList forType:NSFilesPromisePboardType];
                    [this->pasteboard addTypes: fileUTIList owner:$owner];
                }
            }
        }

        if (!this->data->GetFragmentText().empty()) {
            [this->pasteboard addTypes:@[NSStringPboardType] owner:$owner];
        }
    }

    NSArray *DragAndDrop::GetDraggedTypes() const {
        return [NSArray arrayWithObjects:PasteboardDataTypes::GetString(PasteboardDataTypes::DUMMY),
                                         NSStringPboardType,
                                         NSFilenamesPboardType,
                                         NSPasteboardTypeString,
                                         nil];
    }
}

#endif  // MAC_PLATFORM

/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

namespace Io::Oidis::Microcore::Exceptions {
    SharedLibraryManager::ProcedureNotExists::ProcedureNotExists(const string &$name)
            : std::runtime_error{"Given procedure doesn't exist: " + $name} {
    }

    SharedLibraryManager::LibraryFileNotExists::LibraryFileNotExists(const string &$pattern)
            : std::runtime_error{"Library file with this pattern doesn't exist: " + $pattern} {
    }

    SharedLibraryManager::LibraryLoadFailed::LibraryLoadFailed(const string &$libraryPath)
            : std::runtime_error{"Could not load library or its dependencies: " + $libraryPath} {
    }
}

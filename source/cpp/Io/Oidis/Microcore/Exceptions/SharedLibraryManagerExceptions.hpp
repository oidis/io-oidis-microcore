/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_MICROCORE_EXCEPTIONS_SHAREDLIBRARYMANAGEREXCEPTIONS_HPP_
#define IO_OIDIS_MICROCORE_EXCEPTIONS_SHAREDLIBRARYMANAGEREXCEPTIONS_HPP_

namespace Io::Oidis::Microcore::Exceptions::SharedLibraryManager {
    struct ProcedureNotExists : public std::runtime_error {
        explicit ProcedureNotExists(const string &$name);
    };

    struct LibraryFileNotExists : public std::runtime_error {
        explicit LibraryFileNotExists(const string &$pattern);
    };

    struct LibraryLoadFailed : public std::runtime_error {
        explicit LibraryLoadFailed(const string &$libraryPath);
    };
}

#endif  // IO_OIDIS_MICROCORE_EXCEPTIONS_SHAREDLIBRARYMANAGEREXCEPTIONS_HPP_

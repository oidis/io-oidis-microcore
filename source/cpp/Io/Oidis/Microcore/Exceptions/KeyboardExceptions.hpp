/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_MICROCORE_EXCEPTIONS_KEYBOARDEXCEPTIONS_HPP_
#define IO_OIDIS_MICROCORE_EXCEPTIONS_KEYBOARDEXCEPTIONS_HPP_

namespace Io::Oidis::Microcore::Exceptions::Keyboard {
    struct KeyboardError : public std::runtime_error {
        explicit KeyboardError(const std::string & reason);
    };
}

#endif  // IO_OIDIS_MICROCORE_EXCEPTIONS_KEYBOARDEXCEPTIONS_HPP_

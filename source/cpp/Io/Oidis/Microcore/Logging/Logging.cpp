/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include <algorithm>

#include "../sourceFilesMap.hpp"

namespace Io::Oidis::Microcore::Logging {
    using Io::Oidis::XCppCommons::Utils::LogIt;

    using Io::Oidis::Microcore::Configuration::Configuration;

    void RemoveLogFromCEF() {
        if (std::remove(Configuration::GetInstance().getCEF().log.path.c_str()) != 0) {
            LogIt::Error("Failed to delete {0}", Configuration::GetInstance().getCEF().log.path);
        }
    }
}

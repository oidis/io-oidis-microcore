/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014 The Chromium Embedded Framework Authors
 * Copyright (c) 2018 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_MICROCORE_APP_APP_HPP_
#define IO_OIDIS_MICROCORE_APP_APP_HPP_

#include <include/cef_app.h>

#ifdef ERROR
#undef ERROR  // un-define ERROR macro (is defined in cef_logging.h, and we need to use LogLevel::ERROR)
#endif

#include "../Configuration/ProgramArguments.hpp"

namespace Io::Oidis::Microcore::App {
    class Application : public CefApp {
        using ProgramArguments = Io::Oidis::Microcore::Configuration::ProgramArguments;

     public:
        explicit Application(ProgramArguments $arguments);

        CefRefPtr <CefBrowserProcessHandler> GetBrowserProcessHandler() override;

        void CreateApplicationWindow();

     private:
        unique_ptr <Io::Oidis::Microcore::Windows::WindowManager> windowManager = nullptr;
        CefRefPtr <Io::Oidis::Microcore::Handlers::BrowserProcessHandler> browserProcessHandler = nullptr;

        IMPLEMENT_REFCOUNTING(Application);
    };
}

#endif  // IO_OIDIS_MICROCORE_APP_APP_HPP_

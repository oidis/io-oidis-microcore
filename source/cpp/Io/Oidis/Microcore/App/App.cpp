/* ********************************************************************************************************* *
 *
 * Copyright (c) 2015 The Chromium Embedded Framework Authors
 * Copyright (c) 2018 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

namespace Io::Oidis::Microcore::App {
    using Io::Oidis::Microcore::Configuration::Configuration;
    using Io::Oidis::Microcore::Handlers::BrowserProcessHandler;
    using Io::Oidis::Microcore::Windows::WindowManager;

    Application::Application(ProgramArguments $arguments)
            : windowManager{std::make_unique<WindowManager>()} {
        Configuration::GetInstance().Override(std::move($arguments));
        Configuration::GetInstance().ResolveDependencies();

        this->browserProcessHandler = new BrowserProcessHandler{[this]() {
            this->CreateApplicationWindow();
        }};
    }

    CefRefPtr <CefBrowserProcessHandler> Application::GetBrowserProcessHandler() {
        return this->browserProcessHandler;
    }

    void Application::CreateApplicationWindow() {
        auto windowConfiguration = Configuration::GetInstance().getWindow();
        // here, we can change e.g. id, etc.

        this->windowManager->RegisterNewWindow(std::move(windowConfiguration));
    }
}

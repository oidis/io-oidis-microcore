/* ********************************************************************************************************* *
 *
 * Copyright (c) 2013 The Chromium Embedded Framework Authors
 * Copyright (c) 2018 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_MICROCORE_APP_CLIENT_HPP_
#define IO_OIDIS_MICROCORE_APP_CLIENT_HPP_

#include <include/cef_client.h>

#include "../Handlers/ContextMenuHandler.hpp"
#include "../Handlers/DisplayHandler.hpp"
#include "../Handlers/DragHandler.hpp"
#include "../Handlers/LifeSpanHandler.hpp"
#include "../Handlers/LoadHandler.hpp"
#include "../Handlers/RenderHandler.hpp"

namespace Io::Oidis::Microcore::App {
    class Client : public CefClient {
        using ContextMenuHandler = Io::Oidis::Microcore::Handlers::ContextMenuHandler;
        using DisplayHandler = Io::Oidis::Microcore::Handlers::DisplayHandler;
        using LifeSpanHandler = Io::Oidis::Microcore::Handlers::LifeSpanHandler;
        using LoadHandler = Io::Oidis::Microcore::Handlers::LoadHandler;
        using RenderHandler = Io::Oidis::Microcore::Handlers::RenderHandler;
        using DragHandler = Io::Oidis::Microcore::Handlers::DragHandler;
        using IOsrDelegate = Io::Oidis::Microcore::Interfaces::IOsrDelegate;

     public:
        explicit Client(IOsrDelegate *$delegate);

        CefRefPtr <CefContextMenuHandler> GetContextMenuHandler() override;

        CefRefPtr <CefDisplayHandler> GetDisplayHandler() override;

        CefRefPtr <CefLifeSpanHandler> GetLifeSpanHandler() override;

        CefRefPtr <CefLoadHandler> GetLoadHandler() override;

        CefRefPtr <CefRenderHandler> GetRenderHandler() override;

        CefRefPtr <CefDragHandler> GetDragHandler() override;

        void DetachDelegate();

     private:
        IOsrDelegate *osrDelegate = nullptr;
        CefRefPtr <CefContextMenuHandler> contextMenuHandler = new ContextMenuHandler;
        CefRefPtr <DisplayHandler> displayHandler = new DisplayHandler;
        CefRefPtr <LifeSpanHandler> lifeSpanHandler = new LifeSpanHandler{osrDelegate};
        CefRefPtr <LoadHandler> loadHandler = new LoadHandler;
        CefRefPtr <RenderHandler> renderHandler = new RenderHandler{osrDelegate};
        CefRefPtr <DragHandler> dragHandler = new DragHandler;

        IMPLEMENT_REFCOUNTING(Client);
    };
}

#endif  // IO_OIDIS_MICROCORE_APP_CLIENT_HPP_

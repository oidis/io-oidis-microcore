/* ********************************************************************************************************* *
 *
 * Copyright (c) 2013 The Chromium Embedded Framework Authors
 * Copyright (c) 2018 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

namespace Io::Oidis::Microcore::App {
    Client::Client(IOsrDelegate *$delegate)
            : osrDelegate{$delegate} {
    }

    CefRefPtr <CefContextMenuHandler> Client::GetContextMenuHandler() {
        return this->contextMenuHandler;
    }

    CefRefPtr <CefDisplayHandler> Client::GetDisplayHandler() {
        return this->displayHandler;
    }

    CefRefPtr <CefLifeSpanHandler> Client::GetLifeSpanHandler() {
        return this->lifeSpanHandler;
    }

    CefRefPtr <CefLoadHandler> Client::GetLoadHandler() {
        return this->loadHandler;
    }

    CefRefPtr <CefRenderHandler> Client::GetRenderHandler() {
        return this->renderHandler;
    }

    CefRefPtr <CefDragHandler> Client::GetDragHandler() {
        return this->dragHandler;
    }

    void Client::DetachDelegate() {
        this->osrDelegate = nullptr;
    }
}

/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_MICROCORE_APPLICATION_HPP_
#define IO_OIDIS_MICROCORE_APPLICATION_HPP_

namespace Io::Oidis::Microcore {
    class Application : public Io::Oidis::XCppCommons::Application {
     public:
        int Run(int $argc, const char **$argv) override;
    };
}

#endif  // IO_OIDIS_MICROCORE_APPLICATION_HPP_

/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014 The Chromium Embedded Framework Authors
 * Copyright (c) 2018 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

namespace Io::Oidis::Microcore::Helpers::Utils {
    int LogicalToDevice(const int $value, const float $deviceScaleFactor) {
        return static_cast<int>(std::floor(static_cast<float>($value) * $deviceScaleFactor));
    }

    int DeviceToLogical(const int $value, const float $deviceScaleFactor) {
        return static_cast<int>(std::floor(static_cast<float>($value) / $deviceScaleFactor));
    }

    void DeviceToLogical(CefMouseEvent &$value, const float $deviceScaleFactor) {
        $value.x = DeviceToLogical($value.x, $deviceScaleFactor);
        $value.y = DeviceToLogical($value.y, $deviceScaleFactor);
    }
}

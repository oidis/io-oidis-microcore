/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014 The Chromium Embedded Framework Authors
 * Copyright (c) 2018 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_MICROCORE_HELPERS_UTILS_HPP_
#define IO_OIDIS_MICROCORE_HELPERS_UTILS_HPP_

namespace Io::Oidis::Microcore::Helpers::Utils {
    int LogicalToDevice(int $value, float $deviceScaleFactor);

    int DeviceToLogical(int $value, float $deviceScaleFactor);

    void DeviceToLogical(CefMouseEvent &$value, float $deviceScaleFactor);
}

#endif  // IO_OIDIS_MICROCORE_HELPERS_UTILS_HPP_

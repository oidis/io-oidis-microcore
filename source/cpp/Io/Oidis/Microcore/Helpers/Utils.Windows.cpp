/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014 The Chromium Embedded Framework Authors
 * Copyright (c) 2018 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifdef WIN_PLATFORM

#include "../sourceFilesMap.hpp"

namespace Io::Oidis::Microcore::Helpers::Utils {
    void SetUserDataPtr(HWND $hwnd, void *$ptr) {
        SetLastError(ERROR_SUCCESS);
        SetWindowLongPtr($hwnd, GWLP_USERDATA, reinterpret_cast<LONG_PTR>($ptr));
    }

    std::wstring GetWideString(const string &$string) {
        return std::wstring(std::cbegin($string), std::cend($string));
    }
}

#endif  // WIN_PLATFORM

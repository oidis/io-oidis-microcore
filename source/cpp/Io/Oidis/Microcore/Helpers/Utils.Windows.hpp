/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014 The Chromium Embedded Framework Authors
 * Copyright (c) 2018 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_MICROCORE_HELPERS_UTILS_WINDOWS_HPP_
#define IO_OIDIS_MICROCORE_HELPERS_UTILS_WINDOWS_HPP_

#ifdef WIN_PLATFORM

namespace Io::Oidis::Microcore::Helpers::Utils {
    void SetUserDataPtr(const HWND $hwnd, void *$ptr);

    template<typename T>
    T GetUserDataPtr(const HWND $hwnd) {
        return reinterpret_cast<T>(GetWindowLongPtr($hwnd, GWLP_USERDATA));
    }

    std::wstring GetWideString(const string &$string);
}

#endif  // WIN_PLATFORM

#endif  // IO_OIDIS_MICROCORE_HELPERS_UTILS_WINDOWS_HPP_

/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014 The Chromium Embedded Framework Authors
 * Copyright (c) 2018 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifdef LINUX_PLATFORM

#include "../sourceFilesMap.hpp"

namespace Io::Oidis::Microcore::Helpers::Utils {
    uint32 GetCefStateModifiers(guint $state) {
        int modifiers = 0;

        if (($state & GDK_SHIFT_MASK) != 0) {
            modifiers |= EVENTFLAG_SHIFT_DOWN;
        }
        if (($state & GDK_LOCK_MASK) != 0) {
            modifiers |= EVENTFLAG_CAPS_LOCK_ON;
        }
        if (($state & GDK_CONTROL_MASK) != 0) {
            modifiers |= EVENTFLAG_CONTROL_DOWN;
        }
        if (($state & GDK_MOD1_MASK) != 0) {
            modifiers |= EVENTFLAG_ALT_DOWN;
        }
        if (($state & GDK_BUTTON1_MASK) != 0) {
            modifiers |= EVENTFLAG_LEFT_MOUSE_BUTTON;
        }
        if (($state & GDK_BUTTON2_MASK) != 0) {
            modifiers |= EVENTFLAG_MIDDLE_MOUSE_BUTTON;
        }
        if (($state & GDK_BUTTON3_MASK) != 0) {
            modifiers |= EVENTFLAG_RIGHT_MOUSE_BUTTON;
        }

        return modifiers;
    }

    CefBrowserHost::DragOperationsMask GetDragOperationsMask(GdkDragContext *$dragContext) {
        int allowedOperations = DRAG_OPERATION_NONE;

        const GdkDragAction dragAction = gdk_drag_context_get_actions($dragContext);

        if ((dragAction & GDK_ACTION_COPY) != 0) {
            allowedOperations |= DRAG_OPERATION_COPY;
        }
        if ((dragAction & GDK_ACTION_MOVE) != 0) {
            allowedOperations |= DRAG_OPERATION_MOVE;
        }
        if ((dragAction & GDK_ACTION_LINK) != 0) {
            allowedOperations |= DRAG_OPERATION_LINK;
        }
        if ((dragAction & GDK_ACTION_PRIVATE) != 0) {
            allowedOperations |= DRAG_OPERATION_PRIVATE;
        }

        return static_cast<CefBrowserHost::DragOperationsMask>(allowedOperations);
    }
}

#endif  // LINUX_PLATFORM

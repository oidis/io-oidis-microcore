/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_MICROCORE_WINDOWS_WINDOWMANAGERBASE_HPP_
#define IO_OIDIS_MICROCORE_WINDOWS_WINDOWMANAGERBASE_HPP_

namespace Io::Oidis::Microcore::Windows {
    class WindowManagerBase {
        using Window = Io::Oidis::Microcore::Windows::Window;
        using WindowConfiguration = Io::Oidis::Microcore::Configuration::Configuration::Window;

     public:
        WindowManagerBase();

        ~WindowManagerBase();

        void RegisterNewWindow(WindowConfiguration $configuration);

        Window *GetWindow(const string &$id) const;

     private:
        unique_ptr <Window> createWindowInstance(WindowConfiguration $configuration) const;

        std::vector <unique_ptr<Window>> windows;
        mutable std::mutex concurrencyProtection;
    };
}

#endif  // IO_OIDIS_MICROCORE_WINDOWS_WINDOWMANAGERBASE_HPP_

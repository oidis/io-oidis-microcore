/* ********************************************************************************************************* *
 *
 * Copyright (c) 2015 The Chromium Embedded Framework Authors
 * Copyright (c) 2018 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include <include/wrapper/cef_helpers.h>

#include "../sourceFilesMap.hpp"

namespace Io::Oidis::Microcore::Windows {
    using Io::Oidis::XCppCommons::Enums::LogLevel;
    using Io::Oidis::XCppCommons::Utils::LogIt;

    using Io::Oidis::Microcore::App::Client;
    using Io::Oidis::Microcore::Configuration::Configuration;
    using Io::Oidis::Microcore::Interfaces::IOsrDelegate;

    namespace Utils = Io::Oidis::Microcore::Helpers::Utils;

    WindowBase::WindowBase(Configuration::Window $configuration)
            : configuration{std::move($configuration)},
              renderer(Configuration::GetInstance().getRenderer()),
              client(new Client{this}) {
        LogIt::Info("Window {0} is being constructed", this->configuration.id);
    }

    void WindowBase::Open() {
        this->createNativeWindow();
        this->registerEvents();
        this->registerDragAndDrop();
        this->setDeviceScaleFactorAccordingToDisplay();

        if (this->configuration.transparent) {
            this->makeTransparent();
        }

        if (this->configuration.visible) {
            this->show();
            this->windowInfo.SetAsWindowless(this->getCefWindowHandle());
        }

        this->createBrowser();
    }

    CefRefPtr<CefBrowser> WindowBase::getBrowser() const {
        return this->browser;
    }

    string WindowBase::getId() const {
        return this->configuration.id;
    }

    void WindowBase::createBrowser() {
        CefBrowserSettings browserSettings;
        browserSettings.windowless_frame_rate = Configuration::GetInstance().getCEF().global.frameRate;
        browserSettings.background_color = Configuration::GetInstance().getRenderer().backgroundColor;

        CefBrowserHost::CreateBrowser(this->windowInfo, this->client, this->configuration.url, browserSettings, nullptr);
    }

    void WindowBase::applyPopupOffset(int &$x, int &$y) const {
        if (this->isOverPopupWidget($x, $y)) {
            $x += this->getPopupXOffset();
            $y += this->getPopupYOffset();
        }
    }

    bool WindowBase::isOverPopupWidget(const int $x, const int $y) const {
        const CefRect &rc = this->renderer.getPopupRect();
        const int popupRight = rc.x + rc.width;
        const int popupBottom = rc.y + rc.height;

        return ($x >= rc.x) && ($x < popupRight) && ($y >= rc.y) && ($y < popupBottom);
    }

    int WindowBase::getPopupXOffset() const {
        return this->renderer.getOriginalPopupRect().x - this->renderer.getPopupRect().x;
    }

    int WindowBase::getPopupYOffset() const {
        return this->renderer.getOriginalPopupRect().y - this->renderer.getPopupRect().y;
    }

    bool WindowBase::isPaintingAreaReady() const {
        return this->paintingArea != nullptr;
    }

    void WindowBase::onPaintBegin() {
        this->enableOpenGl();
    }

    void WindowBase::onPaintEnd(const CefRenderHandler::PaintElementType $type,
                                const CefRenderHandler::RectList &$dirtyRects,
                                const void *$buffer,
                                const int $width,
                                const int $height) {
        this->renderer.OnPaint($type, $dirtyRects, $buffer, $width, $height);

        if ($type == PET_VIEW && !this->renderer.getPopupRect().IsEmpty()) {
            this->browser->GetHost()->Invalidate(PET_POPUP);
        }

        this->renderer.Render();
    }

    bool WindowBase::shouldPaint(const int $width, const int $height) const {
        return $width > 2 && $height > 2;
    }

    bool WindowBase::getViewRect(CefRect &$rect, WindowDimensions $dimensions) const {
        if (this->isPaintingAreaReady()) {
            $rect.x = 0;
            $rect.y = 0;

            $rect.width = Utils::DeviceToLogical($dimensions.width, this->deviceScaleFactor);
            $rect.height = Utils::DeviceToLogical($dimensions.height, this->deviceScaleFactor);

            return true;
        }

        return false;
    }

    void WindowBase::close() {
        this->browser->GetHost()->CloseBrowser(true);
    }

    void WindowBase::OnAfterCreated(const CefRefPtr <CefBrowser> $browser) {
        this->browser = $browser;
        this->browser->GetHost()->SendFocusEvent(true);
    }

    void WindowBase::OnBeforeClose() {
        dynamic_cast<Client *>(this->browser->GetHost()->GetClient().get())->DetachDelegate();
        this->browser = nullptr;

        this->hide();
        this->destroy();
    }

    bool WindowBase::OnBeforePopup(const CefString &$targetUrl) {
        this->browser->GetMainFrame()->LoadURL($targetUrl);

        return true;
    }

    void WindowBase::UpdateDragCursor(const CefRenderHandler::DragOperation $operation) {
        this->dragAndDrop.operation = $operation;
    }

    bool WindowBase::GetScreenInfo(CefScreenInfo &$screenInfo) {
        if (this->isPaintingAreaReady()) {
            CefRect viewRect;
            GetViewRect(viewRect);

            $screenInfo.device_scale_factor = this->deviceScaleFactor;

            $screenInfo.rect = viewRect;
            $screenInfo.available_rect = viewRect;

            return true;
        }

        return false;
    }

    void WindowBase::destroy() {
        this->disableOpenGl();
        this->unregisterDragAndDrop();
        this->unregisterEvents();
        this->destroyNativeWindow();

        this->handle = nullptr;
        this->paintingArea = nullptr;
    }

    void WindowBase::enableOpenGl() {
        if (!this->openGlStarted) {
            CEF_REQUIRE_UI_THREAD();

            this->startOpenGl();

            this->openGlStarted = true;
        }
    }

    void WindowBase::disableOpenGl() {
        if (this->openGlStarted) {
            CEF_REQUIRE_UI_THREAD();

            this->stopOpenGl();

            this->openGlStarted = false;
        }
    }
}

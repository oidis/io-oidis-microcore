/* ********************************************************************************************************* *
 *
 * Copyright (c) 2015 The Chromium Embedded Framework Authors
 * Copyright (c) 2018 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifdef WIN_PLATFORM

#include <BaseTsd.h>
#include <dwmapi.h>

#include <include/internal/cef_win.h>
#include <include/wrapper/cef_helpers.h>

#include <MSVC/MicrocoreMSVC.hpp>

#include "../sourceFilesMap.hpp"

namespace Io::Oidis::Microcore::Windows {
    using Io::Oidis::XCppCommons::Enums::LogLevel;
    using Io::Oidis::XCppCommons::Utils::LogIt;

    using Io::Oidis::Microcore::Bridges::DragAndDropBridge;
    using Io::Oidis::Microcore::DragAndDrop::DragAndDropManager;
    using Io::Oidis::Microcore::OpenGL::OpenGLContext;

    namespace Exceptions = Io::Oidis::Microcore::Exceptions;
    namespace Utils = Io::Oidis::Microcore::Helpers::Utils;

    bool Window::isPaintingAreaReady() const {
        return WindowBase::isPaintingAreaReady() && IsWindow(this->handle) != 0;
    }

    void Window::OnPaint(const CefRenderHandler::PaintElementType $type,
                         const CefRenderHandler::RectList &$dirtyRects,
                         const void *$buffer,
                         const int $width,
                         const int $height) {
        WindowBase::onPaintBegin();

        if (WindowBase::shouldPaint($width, $height)) {
            const OpenGLContext openGLContext(this->hdc, this->hrc, true);

            WindowBase::onPaintEnd($type, $dirtyRects, $buffer, $width, $height);
        }
    }

    bool Window::GetViewRect(CefRect &$rect) {
        return WindowBase::getViewRect($rect, { this->clientRect.right, this->clientRect.bottom });
    }

    bool Window::GetScreenPoint(const int $viewX, const int $viewY, int &$screenX, int &$screenY) {
        if (this->isPaintingAreaReady()) {
            POINT screenPoint = {Utils::LogicalToDevice($viewX, this->deviceScaleFactor),
                                 Utils::LogicalToDevice($viewY, this->deviceScaleFactor)};
            ClientToScreen(this->handle, &screenPoint);
            $screenX = screenPoint.x;
            $screenY = screenPoint.y;

            return true;
        }

        return false;
    }

    void Window::OnCursorChange(const CefCursorHandle $cursor) {
        if (this->isPaintingAreaReady()) {
            SetClassLongPtr(this->handle, GCLP_HCURSOR, static_cast<LONG>(reinterpret_cast<LONG_PTR>($cursor)));
            SetCursor($cursor);
        }
    }

    bool Window::StartDragging(const CefRefPtr<CefBrowser> $browser,
                               const CefRefPtr<CefDragData> $cefDragData,
                               const CefRenderHandler::DragOperationsMask $allowedOps,
                               const int $x,
                               const int $y) {
        if (DragAndDropBridge::GetInstance().HasTarget()) {
            this->dragAndDrop.operation = DRAG_OPERATION_NONE;
            MSVC::Structures::DragData dragData = MSVC::Structures::CreateDragData($cefDragData);
            const CefBrowserHost::DragOperationsMask result = DragAndDropBridge::GetInstance().StartDragging(dragData,
                                                                                                             $allowedOps,
                                                                                                             $x,
                                                                                                             $y);
            MSVC::Structures::DestroyDragData(&dragData);

            this->dragAndDrop.operation = DRAG_OPERATION_NONE;
            POINT pt = {0, 0};
            GetCursorPos(&pt);
            ScreenToClient(this->paintingArea, &pt);

            $browser->GetHost()->DragSourceEndedAt(Utils::DeviceToLogical(pt.x, this->deviceScaleFactor),
                                                   Utils::DeviceToLogical(pt.y, this->deviceScaleFactor),
                                                   result);
            $browser->GetHost()->DragSourceSystemDragEnded();

            return true;
        }

        return false;
    }

    void Window::startOpenGl() {
        PIXELFORMATDESCRIPTOR pfd;
        hdc = GetDC(this->handle);
        ZeroMemory(&pfd, sizeof(pfd));

        pfd.nSize = sizeof(pfd);
        pfd.nVersion = 1;
        pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER | PFD_SUPPORT_COMPOSITION;
        pfd.iPixelType = PFD_TYPE_RGBA;
        pfd.cColorBits = 24;
        pfd.cAlphaBits = 8;
        pfd.iLayerType = PFD_MAIN_PLANE;
        const int format = ChoosePixelFormat(this->hdc, &pfd);
        SetPixelFormat(this->hdc, format, &pfd);

        this->hrc = wglCreateContext(this->hdc);

        const OpenGLContext openGLContext(this->hdc, this->hrc, false);

        this->renderer.Initialize();
    }

    void Window::stopOpenGl() {
        {
            const OpenGLContext openGLContext(this->hdc, this->hrc, false);

            this->renderer.Cleanup();
        }

        if (this->isPaintingAreaReady()) {
            wglDeleteContext(this->hrc);
            ReleaseDC(this->handle, this->hdc);
        }

        this->hdc = nullptr;
        this->hrc = nullptr;
    }

    void Window::createNativeWindow() {
        CEF_REQUIRE_UI_THREAD();

        this->createWin32Window();

        this->paintingArea = this->handle;
    }

    void Window::destroyNativeWindow() {
        DestroyWindow(this->handle);
    }

    CefWindowHandle Window::getCefWindowHandle() const {
        return this->handle;
    }

    void Window::show() {
        ShowWindow(this->handle, SW_SHOW);
    }

    void Window::hide() {
        ShowWindow(this->handle, SW_HIDE);
    }

    void Window::registerEvents() {
        if (SetWindowLongPtr(this->handle, GWLP_WNDPROC, reinterpret_cast<LONG_PTR>(&Window::windowHandleEvent)) == 0) {
            throw Exceptions::WindowBase::FailedToInitialize{this->configuration.id, "Failed to set-up the procedure handle callback"};
        }

        UpdateWindow(this->handle);
        GetWindowRect(this->handle, &clientRect);
    }

    void Window::unregisterEvents() {
        if (UnregisterClass(this->systemIdentification.c_str(), GetModuleHandle(nullptr))== 0) {
            throw Exceptions::WindowBase::FailedToDeinitialize{this->configuration.id, "Failed to un-register the window class"};
        }
    }

    void Window::registerDragAndDrop() {
        LogIt::Info("Using this version of microcore-msvc: {0}", DragAndDropBridge::GetInstance().GetVersion());

        if (!DragAndDropBridge::GetInstance().Initialize(this->configuration.id.c_str(),
                                                         reinterpret_cast<MSVC::Callbacks::OnDrop>(&DragAndDropManager::OnDrop),
                                                         reinterpret_cast<MSVC::Callbacks::OnDragLeave>(&DragAndDropManager::OnDragLeave),
                                                         reinterpret_cast<MSVC::Callbacks::OnDragOver>(&DragAndDropManager::OnDragOver),
                                                         reinterpret_cast<MSVC::Callbacks::OnDragEnter>(&DragAndDropManager::OnDragEnter),
                                                         this->handle)) {
            throw Exceptions::DragAndDrop::DragAndDropError{"Drag and drop functionality failed to initialize"};
        }
    }

    void Window::unregisterDragAndDrop() {
        if (!DragAndDropBridge::GetInstance().Destroy(this->handle)) {
            throw Exceptions::DragAndDrop::DragAndDropError{"Failed to unregister drag and drop"};
        }
    }

        void Window::createWin32Window() {
        this->systemIdentification = Utils::GetWideString(this->configuration.id);

        WNDCLASSEXW windowClass;
        windowClass.cbSize = sizeof(WNDCLASSEX);
        windowClass.style = CS_OWNDC;
        windowClass.lpfnWndProc = DefWindowProc;
        windowClass.cbClsExtra = 0;
        windowClass.cbWndExtra = 0;
        windowClass.hInstance = GetModuleHandle(nullptr);
        windowClass.hIcon = nullptr;
        windowClass.hCursor = LoadCursor(nullptr, IDC_ARROW);
        windowClass.hbrBackground = nullptr;
        windowClass.lpszMenuName = nullptr;
        windowClass.lpszClassName = this->systemIdentification.c_str();
        windowClass.hIconSm = nullptr;

        RegisterClassEx(&windowClass);

        this->handle = CreateWindow(this->systemIdentification.c_str(),
                                    this->systemIdentification.c_str(),
                                    this->configuration.decorated ? WS_OVERLAPPEDWINDOW : WS_POPUP,
                                    0,
                                    0,
                                    this->configuration.dimensions.width,
                                    this->configuration.dimensions.height,
                                    nullptr,
                                    nullptr,
                                    GetModuleHandle(nullptr),
                                    nullptr);

        if (this->handle == nullptr) {
            throw Exceptions::WindowBase::FailedToInitialize{this->configuration.id, "Could not create win32 window"};
        }

        Utils::SetUserDataPtr(this->handle, this);
    }

    void Window::setDeviceScaleFactorAccordingToDisplay() {
        // TODO(nxf45876): implement
    }

    void Window::makeTransparent() {
        DWM_BLURBEHIND blur = { 0 };
        blur.dwFlags = DWM_BB_ENABLE | DWM_BB_BLURREGION;
        blur.hRgnBlur = CreateRectRgn(-1, -1, 0, 0);
        blur.fEnable = TRUE;
        blur.fTransitionOnMaximized = FALSE;

        if (DwmEnableBlurBehindWindow(this->handle, &blur) != S_OK) {
            DeleteObject(blur.hRgnBlur);

            throw Exceptions::WindowBase::FailedToInitialize{this->configuration.id, "Could not make apply blur/transparency"};
        }

        DeleteObject(blur.hRgnBlur);

        if (this->configuration.decorated) {
            if (SetWindowLong(this->handle, GWL_EXSTYLE, GetWindowLong(this->handle, GWL_EXSTYLE) | WS_EX_LAYERED) == 0) {
                throw Exceptions::WindowBase::FailedToInitialize{this->configuration.id,
                                                                 "Could not set layered style needed for decorated windows"};
            }

            if (SetLayeredWindowAttributes(handle, 0, 254, LWA_ALPHA) == 0) {
                throw Exceptions::WindowBase::FailedToInitialize{this->configuration.id, "Could not perform alpha hack"};
            }
        }
    }
}

#endif  // WIN_PLATFORM

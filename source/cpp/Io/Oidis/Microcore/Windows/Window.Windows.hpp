/* ********************************************************************************************************* *
 *
 * Copyright (c) 2015 The Chromium Embedded Framework Authors
 * Copyright (c) 2018 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_MICROCORE_WINDOWS_WINDOW_WINDOWS_HPP_
#define IO_OIDIS_MICROCORE_WINDOWS_WINDOW_WINDOWS_HPP_

#ifdef WIN_PLATFORM

#include "../Hardware/Mouse.Windows.hpp"
#include "../Windows/WindowBase.hpp"

namespace Io::Oidis::Microcore::Windows {
    class Window : public WindowBase {
     private:
        static LRESULT __stdcall windowHandleEvent(const HWND $hwnd, UINT $message, WPARAM $wParam, LPARAM $lParam);

     public:
        using WindowBase::WindowBase;

        CefBrowserHost::DragOperationsMask DragBeginEvent(CefMouseEvent $event,
                                                          CefRefPtr<CefDragData> $dragData,
                                                          CefBrowserHost::DragOperationsMask $effect);

        CefBrowserHost::DragOperationsMask DragMotionEvent(CefMouseEvent $event, CefBrowserHost::DragOperationsMask $effect);

        void DragLeaveEvent();

        CefBrowserHost::DragOperationsMask DragDropEvent(CefMouseEvent $event, CefBrowserHost::DragOperationsMask $effect);

     private:
        bool isPaintingAreaReady() const override;

        void OnPaint(CefRenderHandler::PaintElementType $type,
                     const CefRenderHandler::RectList &$dirtyRects,
                     const void *$buffer,
                     int $width,
                     int $height) override;

        bool GetViewRect(CefRect &$rect) override;

        bool GetScreenPoint(int $viewX, int $viewY, int &$screenX, int &$screenY) override;

        void OnCursorChange(CefCursorHandle $cursor) override;

        bool StartDragging(const CefRefPtr<CefBrowser> $browser,
                           const CefRefPtr<CefDragData> $cefDragData,
                           CefRenderHandler::DragOperationsMask $allowedOps,
                           int $x,
                           int $y) override;

        void startOpenGl() override;

        void stopOpenGl() override;

        void sizeAllocationEvent();

        void paintEvent();

        void keyEvent(UINT $message, WPARAM $wParam, LPARAM $lParam);

        void mouseEvent(UINT $message, WPARAM $wParam, LPARAM $lParam);

        void createNativeWindow() override;

        void destroyNativeWindow() override;

        CefWindowHandle getCefWindowHandle() const override;

        void show() override;

        void hide() override;

        void registerEvents() override;

        void unregisterEvents() override;

        void registerDragAndDrop() override;

        void unregisterDragAndDrop() override;

        void setDeviceScaleFactorAccordingToDisplay() override;

        void makeTransparent() override;

        void createWin32Window();

        HDC hdc = nullptr;
        HGLRC hrc = nullptr;
        RECT clientRect = {0, 0, 0, 0};
        Io::Oidis::Microcore::Hardware::Mouse::MouseData mouse;
        std::wstring systemIdentification;
    };
}

#endif  // WIN_PLATFORM

#endif  // IO_OIDIS_MICROCORE_WINDOWS_WINDOW_WINDOWS_HPP_

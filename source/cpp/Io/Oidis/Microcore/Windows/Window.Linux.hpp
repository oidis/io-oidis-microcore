/* ********************************************************************************************************* *
 *
 * Copyright (c) 2015 The Chromium Embedded Framework Authors
 * Copyright (c) 2018 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_MICROCORE_WINDOWS_WINDOW_LINUX_HPP_
#define IO_OIDIS_MICROCORE_WINDOWS_WINDOW_LINUX_HPP_

#ifdef LINUX_PLATFORM

#include <gtk/gtk.h>

#include "../Windows/WindowBase.hpp"

namespace Io::Oidis::Microcore::Windows {
    class Window : public WindowBase {
     private:
        static void closeWindowEvent(GtkWidget *$window, Window *$self);

        static gint clickEvent(GtkWidget *$widget, GdkEventButton *$event, Window *$self);

        static gint keyEvent(GtkWidget *$widget, GdkEventKey *$event, Window *$self);

        static gint scrollEvent(GtkWidget *$widget, GdkEventScroll *$event, Window *$self);

        static gint moveEvent(GtkWidget *$widget, GdkEventMotion *$event, Window *$self);

        static gint focusEvent(GtkWidget *$widget, GdkEventFocus *$event, Window *$self);

        static gint sizeAllocationEvent(GtkWidget *$widget, GtkAllocation *$allocation, Window *$self);

        static void dragBeginEvent(GtkWidget *$widget, GdkDragContext *$dragContext, Window *$self);

        static void dragEndEvent(GtkWidget *$widget, GdkDragContext *$dragContext, Window *$self);

        static gboolean dragMotionEvent(GtkWidget *$widget,
                                        GdkDragContext *$dragContext,
                                        gint $x,
                                        gint $y,
                                        guint $time,
                                        Window *$self);

        static void dragLeaveEvent(GtkWidget *$widget,
                                   GdkDragContext *$dragContext,
                                   guint $time,
                                   Window *$self);

        static gboolean dragFailedEvent(GtkWidget *$widget,
                                        GdkDragContext *$dragContext,
                                        GtkDragResult $result,
                                        Window *$self);

        static gboolean dragDropEvent(GtkWidget *$widget,
                                      GdkDragContext *$dragContext,
                                      gint $x,
                                      gint $y,
                                      guint $time,
                                      Window *$self);

     public:
        using WindowBase::WindowBase;

     private:
        void OnPaint(CefRenderHandler::PaintElementType $type,
                     const CefRenderHandler::RectList &$dirtyRects,
                     const void *$buffer,
                     int $width,
                     int $height) override;

        bool GetViewRect(CefRect &$rect) override;

        bool GetScreenPoint(int $viewX, int $viewY, int &$screenX, int &$screenY) override;

        void OnCursorChange(CefCursorHandle $cursor) override;

        bool StartDragging(const CefRefPtr<CefBrowser> $browser,
                           const CefRefPtr<CefDragData> $cefDragData,
                           CefRenderHandler::DragOperationsMask $allowedOps,
                           int $x,
                           int $y) override;


        void startOpenGl() override;

        void stopOpenGl() override;

        void createNativeWindow() override;

        void destroyNativeWindow() override;

        CefWindowHandle getCefWindowHandle() const override;

        void show() override;

        void hide() override;

        void registerEvents() override;

        void unregisterEvents() override;

        void registerDragAndDrop() override;

        void unregisterDragAndDrop() override;

        void setDeviceScaleFactorAccordingToDisplay() override;

        void makeTransparent() override;

        void createGtkWindow();

        void createGlArea();

        void getWidgetRectangleInScreen(GtkWidget *$widget, GdkRectangle *$rectangle) const;

        bool cancelDragAndReturn();
    };
}

#endif  // LINUX_PLATFORM

#endif  // IO_OIDIS_MICROCORE_WINDOWS_WINDOW_LINUX_HPP_

/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

namespace Io::Oidis::Microcore::Windows {
    using Io::Oidis::XCppCommons::Enums::LogLevel;
    using Io::Oidis::XCppCommons::Utils::LogIt;

    using Io::Oidis::Microcore::Windows::Window;

    namespace Exceptions = Io::Oidis::Microcore::Exceptions::WindowManagerBase;

    WindowManagerBase::WindowManagerBase() {
        LogIt::Info("Window manager is being constructed");
    }

    WindowManagerBase::~WindowManagerBase() {
        LogIt::Info("Window manager is being destructed");

        std::lock_guard<decltype(this->concurrencyProtection)> criticalSection{this->concurrencyProtection};

        this->windows.clear();
        this->windows.shrink_to_fit();
    }

    void WindowManagerBase::RegisterNewWindow(WindowConfiguration $configuration) {
        std::lock_guard<decltype(this->concurrencyProtection)> criticalSection{this->concurrencyProtection};

        LogIt::Info("Window manager is registering new window");

        auto window = this->createWindowInstance(std::move($configuration));
        window->Open();

        this->windows.emplace_back(std::move(window));
    }

    Window *WindowManagerBase::GetWindow(const string &$id) const {
        std::lock_guard<decltype(this->concurrencyProtection)> criticalSection{this->concurrencyProtection};

        LogIt::Debug("Getting window for id {0}", $id);

        const auto iterator = std::find_if(std::cbegin(this->windows),
                                           std::cend(this->windows),
                                           [&$id](const unique_ptr<Window> &window) {
                                               return window->getId() == $id;
                                           });

        if (iterator != std::cend(this->windows)) {
            return (*iterator).get();
        } else {
            throw Exceptions::WindowNotExists{$id};
        }
    }

    unique_ptr<Window> WindowManagerBase::createWindowInstance(WindowConfiguration $configuration) const {
        return std::make_unique<Window>(std::move($configuration));
    }
}

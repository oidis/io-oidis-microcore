/* ********************************************************************************************************* *
 *
 * Copyright (c) 2015 The Chromium Embedded Framework Authors
 * Copyright (c) 2018 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_MICROCORE_WINDOWS_WINDOWBASE_HPP_
#define IO_OIDIS_MICROCORE_WINDOWS_WINDOWBASE_HPP_

#include "../Configuration/Configuration.hpp"
#include "../DragAndDrop/DragAndDrop.hpp"
#include "../Interfaces/IOsrDelegate.hpp"
#include "../Renderers/OsrRenderer.hpp"
#include "../Types/WindowHandles.hpp"

namespace Io::Oidis::Microcore::Windows {
    class WindowBase : public Io::Oidis::Microcore::Interfaces::IOsrDelegate {
        using DragAndDrop = Io::Oidis::Microcore::DragAndDrop::DragAndDrop;
        using PaintingAreaHandle = Io::Oidis::Microcore::Types::PaintingAreaHandle;
        using WindowConfiguration = Io::Oidis::Microcore::Configuration::Configuration::Window;
        using WindowDimensions = Io::Oidis::Microcore::Structures::WindowDimensions;
        using WindowHandle = Io::Oidis::Microcore::Types::WindowHandle;

     public:
        explicit WindowBase(WindowConfiguration $configuration);

        void Open();

        CefRefPtr<CefBrowser> getBrowser() const;

        string getId() const;

     protected:
        void createBrowser();

        void applyPopupOffset(int &$x, int &$y) const;

        bool isOverPopupWidget(int $x, int $y) const;

        int getPopupXOffset() const;

        int getPopupYOffset() const;

        virtual bool isPaintingAreaReady() const;

        void onPaintBegin();

        void onPaintEnd(CefRenderHandler::PaintElementType $type,
                        const CefRenderHandler::RectList &$dirtyRects,
                        const void *$buffer,
                        int $width,
                        int $height);

        bool shouldPaint(int $widrh, int $height) const;

        bool getViewRect(CefRect &$rect, WindowDimensions $dimensions) const;

        void close();

        WindowConfiguration configuration;
        CefWindowInfo windowInfo;
        CefRefPtr<CefBrowser> browser = nullptr;
        float deviceScaleFactor = 1.0;
        Io::Oidis::Microcore::Renderers::OsrRenderer renderer;
        WindowHandle handle = nullptr;
        PaintingAreaHandle paintingArea = nullptr;
        bool openGlStarted = false;
        DragAndDrop dragAndDrop;

     private:
        void OnAfterCreated(CefRefPtr<CefBrowser> $browser) override;

        void OnBeforeClose() override;

        bool OnBeforePopup(const CefString &$targetUrl) override;

        void UpdateDragCursor(CefRenderHandler::DragOperation $operation) override;

        bool GetScreenInfo(CefScreenInfo &$screenInfo) override;

        virtual void destroy();

        virtual void enableOpenGl();

        virtual void disableOpenGl();

        virtual void startOpenGl() = 0;

        virtual void stopOpenGl() = 0;

        virtual void createNativeWindow() = 0;

        virtual void destroyNativeWindow() = 0;

        virtual CefWindowHandle getCefWindowHandle() const = 0;

        virtual void show() = 0;

        virtual void hide() = 0;

        virtual void registerEvents() = 0;

        virtual void unregisterEvents() = 0;

        virtual void registerDragAndDrop() = 0;

        virtual void unregisterDragAndDrop() = 0;

        virtual void setDeviceScaleFactorAccordingToDisplay() = 0;

        virtual void makeTransparent() = 0;

        CefRefPtr<Io::Oidis::Microcore::App::Client> client = nullptr;
    };
}

#endif  // IO_OIDIS_MICROCORE_WINDOWS_WINDOWBASE_HPP_

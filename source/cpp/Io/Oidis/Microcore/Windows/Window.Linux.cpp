/* ********************************************************************************************************* *
 *
 * Copyright (c) 2015 The Chromium Embedded Framework Authors
 * Copyright (c) 2018 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifdef LINUX_PLATFORM

#include <GL/gl.h>

#include <include/internal/cef_linux.h>
#include <include/wrapper/cef_helpers.h>

#include "../sourceFilesMap.hpp"

// has to be here, because of the same symbols defined in the CEF
#include <gdk/gdkx.h> // NOLINT

namespace Io::Oidis::Microcore::Windows {
    using Io::Oidis::XCppCommons::Enums::LogLevel;
    using Io::Oidis::XCppCommons::Utils::LogIt;

    using Io::Oidis::Microcore::OpenGL::OpenGLContext;

    namespace Exceptions = Io::Oidis::Microcore::Exceptions::WindowBase;
    namespace Mouse = Io::Oidis::Microcore::Hardware::Mouse;
    namespace Utils = Io::Oidis::Microcore::Helpers::Utils;

    void Window::OnPaint(const CefRenderHandler::PaintElementType $type,
                         const CefRenderHandler::RectList &$dirtyRects,
                         const void *$buffer,
                         const int $width,
                         const int $height) {
        WindowBase::onPaintBegin();

        if (WindowBase::shouldPaint($width, $height)) {
            const OpenGLContext openGLContext(this->paintingArea, true);

            if (openGLContext.isValid()) {
                WindowBase::onPaintEnd($type, $dirtyRects, $buffer, $width, $height);
            }
        }
    }

    bool Window::GetViewRect(CefRect &$rect) {
        return WindowBase::getViewRect($rect, { this->paintingArea->allocation.width, this->paintingArea->allocation.height });
    }

    bool Window::GetScreenPoint(const int $viewX, const int $viewY, int &$screenX, int &$screenY) {
        if (this->isPaintingAreaReady()) {
            GdkRectangle screenRectangle = {0, 0, 0, 0};
            this->getWidgetRectangleInScreen(this->paintingArea, &screenRectangle);
            $screenX = screenRectangle.x + Utils::LogicalToDevice($viewX, this->deviceScaleFactor);
            $screenY = screenRectangle.y + Utils::LogicalToDevice($viewY, this->deviceScaleFactor);

            return true;
        }

        return false;
    }

    void Window::OnCursorChange(const CefCursorHandle $cursor) {
        if (this->isPaintingAreaReady()) {
            const auto xDisplay = cef_get_xdisplay();
            const auto xWindow = GDK_WINDOW_XID(gtk_widget_get_window(this->paintingArea));

            if (xDisplay != nullptr && xWindow != 0) {
                XDefineCursor(xDisplay, xWindow, $cursor);
            }
        }
    }

    bool Window::StartDragging(const CefRefPtr<CefBrowser> /*$browser*/,
                               const CefRefPtr<CefDragData> $cefDragData,
                               const CefRenderHandler::DragOperationsMask $allowedOps,
                               const int $x,
                               const int $y) {
        if ($cefDragData->HasImage()) {
            this->dragAndDrop.Reset();
            this->dragAndDrop.data = $cefDragData;

            if (this->dragAndDrop.triggerEvent != nullptr) {
                LogIt::Error("Dragging started, but last mouse event is missing");

                return this->cancelDragAndReturn();
            }

            this->dragAndDrop.context = gtk_drag_begin(
                    this->paintingArea, this->dragAndDrop.targets, GDK_ACTION_COPY, 1, this->dragAndDrop.triggerEvent);

            if (this->dragAndDrop.context == nullptr) {
                LogIt::Error("GTK drag begin failed");

                return this->cancelDragAndReturn();
            }

            g_object_ref(this->dragAndDrop.context);

            CefMouseEvent mouseEvent = Mouse::GetCefMouseEvent($x, $y, EVENTFLAG_LEFT_MOUSE_BUTTON);

            browser->GetHost()->DragTargetDragEnter(this->dragAndDrop.data, mouseEvent, $allowedOps);

            return true;
        }

        LogIt::Error("Drag image representation not available");

        return false;
    }

    void Window::startOpenGl() {
        const OpenGLContext openGLContext(this->paintingArea, false);

        if (openGLContext.isValid()) {
            this->renderer.Initialize();
        }
    }

    void Window::stopOpenGl() {
        const OpenGLContext openGLContext(this->paintingArea, false);

        if (openGLContext.isValid()) {
            this->renderer.Cleanup();
        }
    }

    void Window::createNativeWindow() {
        CEF_REQUIRE_UI_THREAD();

        this->createGtkWindow();
    }

    void Window::destroyNativeWindow() {
        gtk_widget_destroy(this->handle);
    }

    CefWindowHandle Window::getCefWindowHandle() const {
        return GDK_WINDOW_XID(gtk_widget_get_window(this->handle));
    }

    void Window::show() {
        gtk_widget_show_all(this->handle);

        GdkWindow *gdkWindow = gtk_widget_get_window(this->handle);
        GdkDisplay *display = gdk_window_get_display(gdkWindow);
        gdk_display_flush(display);
    }

    void Window::hide() {
        gtk_widget_hide_all(this->handle);
    }

    void Window::registerEvents() {
        g_signal_connect(this->handle, "destroy", G_CALLBACK(&Window::closeWindowEvent), this);

        gtk_widget_set_events(
                this->paintingArea, GDK_BUTTON_PRESS_MASK | GDK_BUTTON_RELEASE_MASK |
                                    GDK_KEY_PRESS_MASK | GDK_KEY_RELEASE_MASK |
                                    GDK_ENTER_NOTIFY_MASK | GDK_LEAVE_NOTIFY_MASK |
                                    GDK_POINTER_MOTION_MASK | GDK_POINTER_MOTION_HINT_MASK |
                                    GDK_SCROLL_MASK | GDK_FOCUS_CHANGE_MASK);
        g_signal_connect(this->paintingArea, "button_press_event", G_CALLBACK(&Window::clickEvent), this);
        g_signal_connect(this->paintingArea, "button_release_event", G_CALLBACK(&Window::clickEvent), this);
        g_signal_connect(this->paintingArea, "key_press_event", G_CALLBACK(&Window::keyEvent), this);
        g_signal_connect(this->paintingArea, "key_release_event", G_CALLBACK(&Window::keyEvent), this);
        g_signal_connect(this->paintingArea, "scroll_event", G_CALLBACK(&Window::scrollEvent), this);
        g_signal_connect(this->paintingArea, "size_allocate", G_CALLBACK(&Window::sizeAllocationEvent), this);
        g_signal_connect(this->paintingArea, "enter_notify_event", G_CALLBACK(&Window::moveEvent), this);
        g_signal_connect(this->paintingArea, "leave_notify_event", G_CALLBACK(&Window::moveEvent), this);
        g_signal_connect(this->paintingArea, "motion_notify_event", G_CALLBACK(&Window::moveEvent), this);
        g_signal_connect(this->paintingArea, "focus_in_event", G_CALLBACK(&Window::focusEvent), this);
        g_signal_connect(this->paintingArea, "focus_out_event", G_CALLBACK(&Window::focusEvent), this);

        gtk_container_add(GTK_CONTAINER(this->handle), this->paintingArea);
    }

    void Window::unregisterEvents() {
        g_signal_handlers_disconnect_matched(this->paintingArea, G_SIGNAL_MATCH_DATA, 0, 0, nullptr, nullptr, this);
        g_signal_handlers_disconnect_matched(this->handle, G_SIGNAL_MATCH_DATA, 0, 0, nullptr, nullptr, this);
    }

    void Window::registerDragAndDrop() {
        g_signal_connect(this->paintingArea, "drag_begin", G_CALLBACK(&Window::dragBeginEvent), this);
        g_signal_connect(this->paintingArea, "drag_end", G_CALLBACK(&Window::dragEndEvent), this);

        g_signal_connect(this->paintingArea, "drag_motion", G_CALLBACK(&Window::dragMotionEvent), this);
        g_signal_connect(this->paintingArea, "drag_leave", G_CALLBACK(&Window::dragLeaveEvent), this);
        g_signal_connect(this->paintingArea, "drag_failed", G_CALLBACK(&Window::dragFailedEvent), this);
        g_signal_connect(this->paintingArea, "drag_drop", G_CALLBACK(&Window::dragDropEvent), this);

        gtk_drag_dest_set(this->paintingArea, static_cast<GtkDestDefaults>(0), nullptr, 0, GDK_ACTION_COPY);
    }

    void Window::unregisterDragAndDrop() {
        gtk_drag_dest_unset(this->paintingArea);

        if (this->dragAndDrop.triggerEvent != nullptr) {
            gdk_event_free(this->dragAndDrop.triggerEvent);
        }
        if (this->dragAndDrop.context != nullptr) {
            g_object_unref(this->dragAndDrop.context);
        }
        gtk_target_list_unref(this->dragAndDrop.targets);
    }

    void Window::setDeviceScaleFactorAccordingToDisplay() {
    }

    void Window::makeTransparent() {
#warning Not implemented on this platform
    }

    void Window::createGtkWindow() {
        this->handle = gtk_window_new(GTK_WINDOW_TOPLEVEL);

        if (this->handle == nullptr) {
            throw Exceptions::FailedToInitialize{this->configuration.id, "Could not create gtk window"};
        }

        gtk_window_set_default_size(GTK_WINDOW(this->handle),
                                    this->configuration.dimensions.width,
                                    this->configuration.dimensions.height);
        gtk_window_set_decorated(GTK_WINDOW(this->handle), this->configuration.decorated ? TRUE : FALSE);

        this->createGlArea();
    }

    void Window::createGlArea() {
        this->paintingArea = gtk_drawing_area_new();

        if (this->paintingArea == nullptr) {
            throw Exceptions::FailedToInitialize{this->configuration.id, "Could not create painting area"};
        }

        GdkGLConfig *glConfig = gdk_gl_config_new_by_mode(static_cast<GdkGLConfigMode>(
                                                                  GDK_GL_MODE_RGB | GDK_GL_MODE_DEPTH |
                                                                  GDK_GL_MODE_DOUBLE));

        if (glConfig == nullptr) {
            throw Exceptions::FailedToInitialize{this->configuration.id, "Could not create GL configuration"};
        }

        gtk_widget_set_gl_capability(this->paintingArea, glConfig, nullptr, TRUE, GDK_GL_RGBA_TYPE);
        gtk_widget_set_can_focus(this->paintingArea, TRUE);
    }

    void Window::getWidgetRectangleInScreen(GtkWidget *$widget, GdkRectangle *$rectangle) const {
        gint x = 0;
        gint y = 0;
        gint width = 0;
        gint height = 0;
        GdkRectangle extents = {0, 0, 0, 0};

        GdkWindow *window = gtk_widget_get_parent_window($widget);

        gdk_window_get_root_origin(window, &x, &y);
        gdk_drawable_get_size(window, &width, &height);
        gdk_window_get_frame_extents(window, &extents);

        const gint border = (extents.width - width) / 2;
        $rectangle->x = x + border + $widget->allocation.x;
        $rectangle->y = y + (extents.height - height) - border + $widget->allocation.y;
        $rectangle->width = $widget->allocation.width;
        $rectangle->height = $widget->allocation.height;
    }

    bool Window::cancelDragAndReturn() {
        this->dragAndDrop.Reset();

        return false;
    }
}

#endif  // LINUX_PLATFORM

/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifdef MAC_PLATFORM

#include "../../sourceFilesMap.hpp"

namespace Exceptions = Io::Oidis::Microcore::Exceptions::WindowBase;

@implementation ComWuiFrameworkMicrocoreWindowsNative_NativeOpenGlView

- (id)Initialize:(const CGRect)$frame
        window:(Io::Oidis::Microcore::Windows::Window *)$window
        textInputContext:(NSTextInputContext *)$textInputContext { // NOLINT
    const NSOpenGLPixelFormatAttribute attributes[] = {
            NSOpenGLPFAOpenGLProfile, NSOpenGLProfileVersionLegacy,
            NSOpenGLPFADoubleBuffer, true,
            NSOpenGLPFADepthSize, 32,
            0, 0};
    NSOpenGLPixelFormat *pixelFormat = [[NSOpenGLPixelFormat alloc] initWithAttributes: attributes];
    [pixelFormat autorelease];

    if (self = [super initWithFrame:$frame pixelFormat:pixelFormat]) {
        window = $window;

        trackingArea = [[NSTrackingArea alloc] initWithRect:$frame
                                               options:NSTrackingMouseMoved | NSTrackingActiveInActiveApp | NSTrackingInVisibleRect
                                               owner:self
                                               userInfo:nil];
        if (trackingArea == nil) {
            throw Exceptions::FailedToInitialize{window->getId(), "Failed to create NSTrackingArea instance"};
        }

        [self addTrackingArea:trackingArea];

        self.autoresizingMask = NSViewWidthSizable | NSViewHeightSizable;
        self.autoresizesSubviews = YES;
        self.wantsBestResolutionOpenGLSurface = YES;

        textInputContext = $textInputContext;
    }

    return self;
}

- (void)MakeTransparent {
    if (self.openGLContext == nil) {
        throw Exceptions::FailedToInitialize{window->getId(), "Failed to get current OpenGL context"};
    }

    GLint opacity = 0;
    [self.openGLContext setValues:&opacity forParameter:NSOpenGLCPSurfaceOpacity];
}

- (void)Detach {
    window = nil;

    [trackingArea release];
    trackingArea = nil;

    [textInputContext release];
    textInputContext = nil;
}

- (BOOL)acceptsFirstResponder {
    return YES;
}

- (void)dealloc {
    [self Detach];

    [super dealloc];
}

- (void)draggedImage:(NSImage*)$image ended:(NSPoint)$ended operation:(NSDragOperation)$operation { // NOLINT
    window->dragImageEvent([self flipWindowPointToView:[self.window convertScreenToBase:$ended]], $operation);
}

- (NSDragOperation)draggingEntered:(id<NSDraggingInfo>)$info { // NOLINT
    NSPoint point = [self flipWindowPointToView:$info.draggingLocation];
    point = [self convertPointToBacking:point];

    return window->dragBeginEvent(point, $info.draggingPasteboard, $info.draggingSourceOperationMask);
}

- (void)draggingExited:(id<NSDraggingInfo>)$sender { // NOLINT
    window->dragEndEvent();
}

- (NSDragOperation)draggingSession:(NSDraggingSession*)$session sourceOperationMaskForDraggingContext:(NSDraggingContext)$context { // NOLINT
    return window->getDragAndDrop().allowedOperations;
}

- (NSDragOperation)draggingUpdated:(id<NSDraggingInfo>)$info { // NOLINT
    NSPoint point = [self flipWindowPointToView:$info.draggingLocation];
    point = [self convertPointToBacking:point];

    return window->dragMotionEvent(point, $info.draggingSourceOperationMask);
}

- (void)drawRect:(const NSRect)$dirtyRect { // NOLINT
    window->drawRectangleEvent($dirtyRect, [self inLiveResize]);
}

- (void)flagsChanged:(NSEvent *)$event { // NOLINT
    window->inputFlagsChanged($event);
}

- (NSPoint)flipWindowPointToView:(NSPoint)$windowPoint {
    NSPoint viewPoint = [self convertPoint:$windowPoint fromView:nil];
    viewPoint.y = self.frame.size.height - viewPoint.y;

    return viewPoint;
}

- (NSPoint)getClickPointForEvent:(NSEvent *)$event { // NOLINT
    const NSPoint windowLocal = [$event locationInWindow];
    const NSPoint contentLocal = [self convertPoint:windowLocal fromView:nil];

    return {contentLocal.x, [self frame].size.height - contentLocal.y};
}

- (NSTextInputContext*)inputContext { // NOLINT
    return textInputContext;
}

- (void)keyDown:(NSEvent *)$event { // NOLINT
    window->keyEvent($event, false);
}

- (void)keyUp:(NSEvent *)$event { // NOLINT
    window->keyEvent($event, true);
}

- (void)mouseDown:(NSEvent*)$event { // NOLINT
    window->clickEvent($event, MBT_LEFT, [self convertPointToBacking:[self getClickPointForEvent:$event]], false);
}

- (void)mouseDragged:(NSEvent*)$event { // NOLINT
    [self mouseMoved:$event];
}

- (void)mouseEntered:(NSEvent *)$event { // NOLINT
    [self mouseMoved:$event];
}

- (void)mouseExited:(NSEvent *)$event { // NOLINT
    window->moveEvent($event, [self convertPointToBacking:[self getClickPointForEvent:$event]]);
}

- (void)mouseMoved:(NSEvent *)$event { // NOLINT
    window->moveEvent($event, [self convertPointToBacking:[self getClickPointForEvent:$event]]);
}

- (void)mouseUp:(NSEvent*)$event { // NOLINT
    window->clickEvent($event, MBT_LEFT, [self convertPointToBacking:[self getClickPointForEvent:$event]], true);
}

- (NSArray*)namesOfPromisedFilesDroppedAtDestination:(NSURL*)$dropDestination { // NOLINT
    if (window->getDragAndDrop().data != nullptr && [$dropDestination isFileURL]) {
        const size_t expectedSize = window->getDragAndDrop().data->GetFileContents(nullptr);

        if (expectedSize > 0) {
            const string path = string([$dropDestination.path UTF8String]) +
                                string("/") +
                                window->getDragAndDrop().data->GetFileName().ToString();

            CefRefPtr<CefStreamWriter> writer = CefStreamWriter::CreateForFile(path);

            if (writer == nullptr || (window->getDragAndDrop().data->GetFileContents(writer) != expectedSize)) {
                return nil;
            }

            return @[[NSString stringWithUTF8String:path.c_str()]];
        }
    }

    return nil;
}

- (void)pasteboard:(NSPasteboard *)$pasteboard provideDataForType:(NSString *)$type { // NOLINT
    window->pasteboardProvideEvent($pasteboard, $type);
}

- (BOOL)performDragOperation:(id<NSDraggingInfo>)$info { // NOLINT
    NSPoint point = [self flipWindowPointToView:$info.draggingLocation];
    point = [self convertPointToBacking:point];

    return window->dragDropEvent(point);
}

- (BOOL)prepareForDragOperation:(id<NSDraggingInfo>)$info { // NOLINT
    return YES;
}

- (void)scrollWheel:(NSEvent*)$event { // NOLINT
    window->scrollEvent($event, [self convertPointToBacking:[self getClickPointForEvent:$event]]);
}

- (void)viewDidChangeBackingProperties { // NOLINT
    window->paintingAreaChangeBackingPropertiesEvent();
}

@end

#endif  // MAC_PLATFORM

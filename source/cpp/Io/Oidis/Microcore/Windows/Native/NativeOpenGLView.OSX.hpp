/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_MICROCORE_WINDOWS_NATIVE_NATIVEOPENGLVIEW_OSX_HPP_
#define IO_OIDIS_MICROCORE_WINDOWS_NATIVE_NATIVEOPENGLVIEW_OSX_HPP_

#ifdef MAC_PLATFORM

#import <Appkit/NSDragging.h>
#import <Appkit/NSOpenGLView.h>
#import <Appkit/NSTextInputContext.h>
#import <Appkit/NSTrackingArea.h>

@interface ComWuiFrameworkMicrocoreWindowsNative_NativeOpenGlView : NSOpenGLView<NSDraggingSource, NSDraggingDestination> {
 @private // NOLINT
    Io::Oidis::Microcore::Windows::Window *window;
    NSTrackingArea *trackingArea;
    NSTextInputContext *textInputContext;
}

- (id)Initialize:(CGRect)$frame
        window:(Io::Oidis::Microcore::Windows::Window *)$window // NOLINT
        textInputContext:(NSTextInputContext *)$textInputContext; // NOLINT

- (void)MakeTransparent;

- (void)Detach;

@end

#endif  // MAC_PLATFORM

#endif  // IO_OIDIS_MICROCORE_WINDOWS_NATIVE_NATIVEOPENGLVIEW_OSX_HPP_

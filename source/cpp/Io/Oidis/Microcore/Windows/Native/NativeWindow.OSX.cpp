/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifdef MAC_PLATFORM

#include "../../sourceFilesMap.hpp"

@implementation ComWuiFrameworkMicrocoreWindowsNative_NativeWindow

- (id)Initialize:(NSRect)$rectangle decorated:(BOOL)$decorated {
    return [super initWithContentRect:$rectangle
                  styleMask:$decorated ?
                    NSWindowStyleMaskResizable | NSWindowStyleMaskTitled | NSWindowStyleMaskClosable | NSWindowStyleMaskMiniaturizable :
                    NSWindowStyleMaskBorderless
                  backing:NSBackingStoreBuffered
                  defer:NO];
}

- (void)MakeTransparent {
    self.alphaValue = 1.0;
    self.backgroundColor = NSColor.clearColor;
    self.opaque = NO;
}

- (BOOL)canBecomeKeyWindow {
    return YES;
}

@end;

#endif  // MAC_PLATFORM

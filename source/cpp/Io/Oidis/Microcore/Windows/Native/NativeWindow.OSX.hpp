/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_MICROCORE_WINDOWS_NATIVE_NATIVEWINDOW_OSX_HPP_
#define IO_OIDIS_MICROCORE_WINDOWS_NATIVE_NATIVEWINDOW_OSX_HPP_

#ifdef MAC_PLATFORM

#include <AppKit/NSWindow.h>

@interface ComWuiFrameworkMicrocoreWindowsNative_NativeWindow : NSWindow

- (id)Initialize:(NSRect)$rectangle decorated:(BOOL)$decorated;

- (void)MakeTransparent;

@end;

#endif  // MAC_PLATFORM

#endif  // IO_OIDIS_MICROCORE_WINDOWS_NATIVE_NATIVEWINDOW_OSX_HPP_

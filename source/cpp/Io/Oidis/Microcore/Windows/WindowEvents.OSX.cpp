/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifdef MAC_PLATFORM

#include "../sourceFilesMap.hpp"

namespace Io::Oidis::Microcore::Windows {
    using Io::Oidis::Microcore::Commons::BytesWriteHandler;
    using Io::Oidis::Microcore::Enums::PasteboardDataTypes;

    namespace Mouse = Io::Oidis::Microcore::Hardware::Mouse;
    namespace Utils = Io::Oidis::Microcore::Helpers::Utils;

    void Window::clickEvent(NSEvent *$event, const CefBrowserHost::MouseButtonType $buttonType, const NSPoint $point, const bool $isUp) {
        if (this->browser != nullptr) {
            const CefMouseEvent mouseEvent = Mouse::GetCefMouseEvent(Utils::DeviceToLogical($point.x, this->deviceScaleFactor),
                                                                     Utils::DeviceToLogical($point.y, this->deviceScaleFactor),
                                                                     this->getModifiersForEvent($event));
            this->browser->GetHost()->SendMouseClickEvent(mouseEvent, $buttonType, $isUp, [$event clickCount]);
        }
    }

    void Window::scrollEvent(NSEvent *$event, const NSPoint $point) {
        const CefMouseEvent mouseEvent = Mouse::GetCefMouseEvent(Utils::DeviceToLogical($point.x, this->deviceScaleFactor),
                                                                 Utils::DeviceToLogical($point.y, this->deviceScaleFactor),
                                                                 this->getModifiersForEvent($event));

        browser->GetHost()->SendMouseWheelEvent(mouseEvent,
                                                CGEventGetIntegerValueField($event.CGEvent, kCGScrollWheelEventPointDeltaAxis2),
                                                CGEventGetIntegerValueField($event.CGEvent, kCGScrollWheelEventPointDeltaAxis1));
    }

    void Window::moveEvent(NSEvent *$event, const NSPoint $point) {
        if (this->browser != nullptr) {
            // TODO(nxf45876): handle rotation

            CefMouseEvent mouseEvent = Mouse::GetCefMouseEvent(Utils::DeviceToLogical($point.x, this->deviceScaleFactor),
                                                               Utils::DeviceToLogical($point.y, this->deviceScaleFactor),
                                                               this->getModifiersForEvent($event));

            this->browser->GetHost()->SendMouseMoveEvent(mouseEvent, false);
        }
    }

    void Window::keyEvent(NSEvent *$event, const bool $isUp) {
        if ($event.type != NSFlagsChanged) {
            if ([Keyboard IsCommand:$event]) {
                this->commandKeyEvent($event);
            } else {
                this->plainKeyEvent($event, $isUp);
            }
        }
    }

    void Window::inputFlagsChanged(NSEvent *$event) {
        this->keyEvent($event, [Keyboard IsKeyUpEvent:$event]);
    }

    void Window::paintingAreaChangeBackingPropertiesEvent() {
        if (this->browser != nullptr) {
            this->browser->GetHost()->NotifyScreenInfoChanged();
            this->browser->GetHost()->WasResized();
        }
    }

    void Window::drawRectangleEvent(const NSRect $dirtyRectangle, const bool $isInResizeProcess) {
        [NSColor.clearColor set];
        NSRectFill($dirtyRectangle);

        if (this->browser != nullptr && $isInResizeProcess) {
            this->browser->GetHost()->Invalidate(PET_VIEW);
        }
    }

    NSDragOperation Window::dragBeginEvent(NSPoint $point, NSPasteboard *$pastebard, NSDragOperation $operationMask) {
        CefRefPtr<CefDragData> dragData;

        if (dragAndDrop.data == nullptr) {
            dragData = CefDragData::Create();

            this->populateDropData(dragData, $pastebard);
        } else {
            dragData = dragAndDrop.data->Clone();
            dragData->ResetFileContents();
        }


        const CefMouseEvent mouseEvent = Mouse::GetCefMouseEvent(Utils::DeviceToLogical($point.x, this->deviceScaleFactor),
                                                                 Utils::DeviceToLogical($point.y, this->deviceScaleFactor),
                                                                 [NSEvent modifierFlags]);

        const CefBrowserHost::DragOperationsMask allowedOperations = static_cast<CefBrowserHost::DragOperationsMask>($operationMask);

        this->browser->GetHost()->DragTargetDragEnter(dragData, mouseEvent, allowedOperations);
        this->browser->GetHost()->DragTargetDragOver(mouseEvent, allowedOperations);

        this->dragAndDrop.operation = NSDragOperationCopy;

        return this->dragAndDrop.operation;
    }

    void Window::dragEndEvent() {
        this->browser->GetHost()->DragTargetDragLeave();
    }

    NSDragOperation Window::dragMotionEvent(NSPoint $point, NSDragOperation $operationMask) {
        const CefMouseEvent mouseEvent = Mouse::GetCefMouseEvent(Utils::DeviceToLogical($point.x, this->deviceScaleFactor),
                                                                 Utils::DeviceToLogical($point.y, this->deviceScaleFactor),
                                                                 [NSEvent modifierFlags]);

        this->browser->GetHost()->DragTargetDragOver(mouseEvent, static_cast<CefBrowserHost::DragOperationsMask>($operationMask));

        return this->dragAndDrop.operation;
    }

    bool Window::dragDropEvent(const NSPoint $point) {
        const CefMouseEvent mouseEvent = Mouse::GetCefMouseEvent(Utils::DeviceToLogical($point.x, this->deviceScaleFactor),
                                                                 Utils::DeviceToLogical($point.y, this->deviceScaleFactor),
                                                                 [NSEvent modifierFlags]);

        this->browser->GetHost()->DragTargetDrop(mouseEvent);
        this->browser->GetHost()->DragSourceSystemDragEnded();
        this->browser->GetHost()->SendMouseMoveEvent(mouseEvent, false);

        return true;
    }

    void Window::dragImageEvent(const NSPoint $point, NSDragOperation $operation) {
        if ($operation == (NSDragOperationMove | NSDragOperationCopy)) {
            $operation &= ~NSDragOperationMove;
        }

        const CefRenderHandler::DragOperation operation = static_cast<CefRenderHandler::DragOperation>($operation);

        this->browser->GetHost()->DragSourceEndedAt($point.x, $point.y, operation);
        this->browser->GetHost()->DragSourceSystemDragEnded();

        this->dragAndDrop.Reset();
    }

    void Window::pasteboardProvideEvent(NSPasteboard *$pasteboard, NSString *$type) {
        if ([$type isEqualToString:NSURLPboardType]) {
            NSURL *url = [NSURL URLWithString:[NSString stringWithUTF8String:this->dragAndDrop.data->GetLinkURL().ToString().c_str()]];

            [url writeToPasteboard:$pasteboard];
        } else if ([$type isEqualToString:PasteboardDataTypes::GetString(PasteboardDataTypes::URL_TITLE)]) {
            [$pasteboard setString:[NSString stringWithUTF8String:this->dragAndDrop.data->GetLinkTitle().ToString().c_str()]
                         forType:PasteboardDataTypes::GetString(PasteboardDataTypes::URL_TITLE)];
        } else if ([$type isEqualToString:static_cast<NSString *>(this->dragAndDrop.fileUTI)]) {
            CefRefPtr<BytesWriteHandler> handler = new BytesWriteHandler(this->dragAndDrop.data->GetFileContents(nullptr));
            CefRefPtr<CefStreamWriter> writer = CefStreamWriter::CreateForHandler(handler.get());

            this->dragAndDrop.data->GetFileContents(writer);

            [$pasteboard setData:[NSData dataWithBytes:handler->GetData()
                                         length:handler->GetDataSize()]
                                         forType:static_cast<NSString *>(this->dragAndDrop.fileUTI)];
        } else if ([$type isEqualToString:NSStringPboardType]) {
            [$pasteboard setString:[NSString stringWithUTF8String:this->dragAndDrop.data->GetFragmentText().ToString().c_str()]
                         forType:NSStringPboardType];

        } else if ([$type isEqualToString:PasteboardDataTypes::GetString(PasteboardDataTypes::DUMMY)]) {
            [$pasteboard setData:[NSData data] forType:PasteboardDataTypes::GetString(PasteboardDataTypes::DUMMY)];
        }
    }

    void Window::closeWindowEvent() {
        this->close();
    }

    void Window::changeBackingPropertiesEvent() {
        this->setDeviceScaleFactorAccordingToDisplay();
    }

    int Window::getModifiersForEvent(NSEvent * $event) const {
        int modifiers = 0;

        if ($event.modifierFlags & NSControlKeyMask) {
            modifiers |= EVENTFLAG_CONTROL_DOWN;
        }
        if ($event.modifierFlags & NSShiftKeyMask) {
            modifiers |= EVENTFLAG_SHIFT_DOWN;
        }
        if ($event.modifierFlags & NSAlternateKeyMask) {
            modifiers |= EVENTFLAG_ALT_DOWN;
        }
        if ($event.modifierFlags & NSCommandKeyMask) {
            modifiers |= EVENTFLAG_COMMAND_DOWN;
        }
        if ($event.modifierFlags & NSAlphaShiftKeyMask) {
            modifiers |= EVENTFLAG_CAPS_LOCK_ON;
        }

        if ($event.type == NSKeyUp || $event.type == NSKeyDown || $event.type == NSFlagsChanged) {
            if ([Keyboard IsNumericEvent:$event]) {
                modifiers |= EVENTFLAG_IS_KEY_PAD;
            }
        }

        switch ($event.type) {
            case NSLeftMouseDragged:
            case NSLeftMouseDown:
            case NSLeftMouseUp:
                modifiers |= EVENTFLAG_LEFT_MOUSE_BUTTON;
            break;
            case NSRightMouseDragged:
            case NSRightMouseDown:
            case NSRightMouseUp:
                modifiers |= EVENTFLAG_RIGHT_MOUSE_BUTTON;
            break;
            case NSOtherMouseDragged:
            case NSOtherMouseDown:
            case NSOtherMouseUp:
                modifiers |= EVENTFLAG_MIDDLE_MOUSE_BUTTON;
            break;
            default:
                break;
        }

        return modifiers;
    }

    void Window::commandKeyEvent(NSEvent *$event) {
        if ([Keyboard IsCommandCopy:$event]) {
            this->browser->GetFocusedFrame()->Copy();
        } else if ([Keyboard IsCommandCut:$event]) {
            this->browser->GetFocusedFrame()->Cut();
        } else if ([Keyboard IsCommandPaste:$event]) {
            this->browser->GetFocusedFrame()->Paste();
        } else if ([Keyboard IsCommandQuit:$event]) {
            this->closeWindowEvent();
        }
    }

    void Window::plainKeyEvent(NSEvent *$event, const bool $isUp) {
        CefKeyEvent keyEvent = [Keyboard CreateKeyEvent:$event];
        keyEvent.type = $isUp ? KEYEVENT_KEYUP : KEYEVENT_KEYDOWN;

        if (keyEvent.type == KEYEVENT_KEYUP) {
            this->browser->GetHost()->SendKeyEvent(keyEvent);
        } else {
            [this->keyboard HandleKeyEventBeforeTextInputClient:$event];

            [[this->keyboard getTextInputContext] handleEvent:$event];

            keyEvent.modifiers = this->getModifiersForEvent($event);

            [this->keyboard HandleKeyEventAfterTextInputClient:keyEvent];
        }
    }

    void Window::sizeAllocationEvent() {
        if (this->browser != nullptr) {
            this->browser->GetHost()->WasResized();
        }
    }
}

#endif  // MAC_PLATFORM

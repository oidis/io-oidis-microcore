/* ********************************************************************************************************* *
 *
 * Copyright (c) 2015 The Chromium Embedded Framework Authors
 * Copyright (c) 2018 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifdef LINUX_PLATFORM

#include <gdk/gdkkeysyms.h>

#include <include/wrapper/cef_helpers.h>

#include "../sourceFilesMap.hpp"

namespace Io::Oidis::Microcore::Windows {
    using Io::Oidis::XCppCommons::Enums::LogLevel;
    using Io::Oidis::XCppCommons::Utils::LogIt;

    using Io::Oidis::Microcore::Enums::KeyboardCodes;

    namespace Keyboard = Io::Oidis::Microcore::Hardware::Keyboard;
    namespace Mouse = Io::Oidis::Microcore::Hardware::Mouse;
    namespace Utils = Io::Oidis::Microcore::Helpers::Utils;

    void Window::closeWindowEvent(GtkWidget */*$window*/, Window *$self) {
        $self->close();
    }

    gint Window::clickEvent(GtkWidget *$widget, GdkEventButton *$event, Window *$self) {
        CEF_REQUIRE_UI_THREAD();

        if ($self->browser != nullptr) {
            const CefRefPtr<CefBrowserHost> host = $self->browser->GetHost();

            CefBrowserHost::MouseButtonType buttonType = CefBrowserHost::MouseButtonType::MBT_LEFT;
            switch ($event->button) {
                case 1:
                    break;
                case 2:
                    buttonType = CefBrowserHost::MouseButtonType::MBT_MIDDLE;
                    break;
                case 3:
                    buttonType = CefBrowserHost::MouseButtonType::MBT_RIGHT;
                    break;
                default:
                    return FALSE;
            }

            CefMouseEvent mouseEvent = Mouse::GetCefMouseEvent($event->x, $event->y, $event->state);
            $self->applyPopupOffset(mouseEvent.x, mouseEvent.y);
            Utils::DeviceToLogical(mouseEvent, $self->deviceScaleFactor);

            const auto isMouseUp = ($event->type == GDK_BUTTON_RELEASE);
            if (!isMouseUp) {
                gtk_widget_grab_focus($widget);
            }

            int clickCount = 1;
            switch ($event->type) {
                case GDK_2BUTTON_PRESS:
                    clickCount = 2;
                    break;
                case GDK_3BUTTON_PRESS:
                    clickCount = 3;
                    break;
                default:
                    break;
            }

            host->SendMouseClickEvent(mouseEvent, buttonType, isMouseUp, clickCount);

            if ($self->dragAndDrop.context == nullptr && buttonType == MBT_LEFT) {
                if ($self->dragAndDrop.triggerEvent != nullptr) {
                    gdk_event_free($self->dragAndDrop.triggerEvent);
                }

                $self->dragAndDrop.triggerEvent = gdk_event_copy(reinterpret_cast<GdkEvent *>($event));
            }
        }

        return TRUE;
    }

    gint Window::keyEvent(GtkWidget */*$widget*/, GdkEventKey *$event, Window *$self) {
        CEF_REQUIRE_UI_THREAD();

        if ($self->browser != nullptr) {
            const CefRefPtr<CefBrowserHost> host = $self->browser->GetHost();

            CefKeyEvent keyEvent;
            const KeyboardCodes windowsKeyCode = Keyboard::GdkEventToWindowsKeyCode($event);
            keyEvent.windows_key_code = Keyboard::GetWindowsKeyCodeWithoutLocation(windowsKeyCode);
            keyEvent.native_key_code = $event->hardware_keycode;

            keyEvent.modifiers = Utils::GetCefStateModifiers($event->state);
            if ($event->keyval >= GDK_KP_Space && $event->keyval <= GDK_KP_9) {
                keyEvent.modifiers |= EVENTFLAG_IS_KEY_PAD;
            }
            if ((keyEvent.modifiers & EVENTFLAG_ALT_DOWN) != 0) {
                keyEvent.is_system_key = 1;
            }

            if (windowsKeyCode == KeyboardCodes::VKEY_RETURN) {
                keyEvent.unmodified_character = '\r';
            } else {
                keyEvent.unmodified_character = static_cast<int>(gdk_keyval_to_unicode($event->keyval));
            }

            if ((keyEvent.modifiers & EVENTFLAG_CONTROL_DOWN) != 0) {
                keyEvent.character = Keyboard::GetControlCharacter(windowsKeyCode, (keyEvent.modifiers & EVENTFLAG_SHIFT_DOWN) != 0);
            } else {
                keyEvent.character = keyEvent.unmodified_character;
            }

            if ($event->type == GDK_KEY_PRESS) {
                keyEvent.type = KEYEVENT_RAWKEYDOWN;
                host->SendKeyEvent(keyEvent);
                keyEvent.type = KEYEVENT_CHAR;
                host->SendKeyEvent(keyEvent);
            } else {
                keyEvent.type = KEYEVENT_KEYUP;
                host->SendKeyEvent(keyEvent);
            }
        }

        return TRUE;
    }

    gint Window::scrollEvent(GtkWidget */*$widget*/, GdkEventScroll *$event, Window *$self) {
        CEF_REQUIRE_UI_THREAD();

        if ($self->browser != nullptr) {
            const CefRefPtr<CefBrowserHost> host = $self->browser->GetHost();

            CefMouseEvent mouseEvent = Mouse::GetCefMouseEvent($event->x, $event->y, $event->state);
            $self->applyPopupOffset(mouseEvent.x, mouseEvent.y);
            Utils::DeviceToLogical(mouseEvent, $self->deviceScaleFactor);

            static const int scrollbarPixelsPerGtkTick = 40;
            int deltaX = 0;
            int deltaY = 0;
            switch ($event->direction) {
                case GDK_SCROLL_UP:
                    deltaY = scrollbarPixelsPerGtkTick;
                    break;
                case GDK_SCROLL_DOWN:
                    deltaY = -scrollbarPixelsPerGtkTick;
                    break;
                case GDK_SCROLL_LEFT:
                    deltaX = scrollbarPixelsPerGtkTick;
                    break;
                case GDK_SCROLL_RIGHT:
                    deltaX = -scrollbarPixelsPerGtkTick;
                    break;
            }

            host->SendMouseWheelEvent(mouseEvent, deltaX, deltaY);
        }

        return TRUE;
    }

    gint Window::moveEvent(GtkWidget */*$widget*/, GdkEventMotion *$event, Window *$self) {
        CEF_REQUIRE_UI_THREAD();

        if ($self->browser != nullptr) {
            const CefRefPtr<CefBrowserHost> host = $self->browser->GetHost();

            gint x = 0;
            gint y = 0;
            GdkModifierType state = GDK_BUTTON1_MASK;

            if ($event->is_hint != 0) {
                gdk_window_get_pointer($event->window, &x, &y, &state);
            } else {
                x = static_cast<gint>($event->x);
                y = static_cast<gint>($event->y);
                state = static_cast<GdkModifierType>($event->state);

                if (x == 0 && y == 0) {
                    return TRUE;
                }
            }

            CefMouseEvent mouseEvent = Mouse::GetCefMouseEvent(x, y, state);
            $self->applyPopupOffset(mouseEvent.x, mouseEvent.y);
            Utils::DeviceToLogical(mouseEvent, $self->deviceScaleFactor);

            host->SendMouseMoveEvent(mouseEvent, $event->type == GDK_LEAVE_NOTIFY);

            if ($self->dragAndDrop.context == nullptr && (mouseEvent.modifiers & EVENTFLAG_LEFT_MOUSE_BUTTON) != 0) {
                if ($self->dragAndDrop.triggerEvent != nullptr) {
                    gdk_event_free($self->dragAndDrop.triggerEvent);
                }

                $self->dragAndDrop.triggerEvent = gdk_event_copy(reinterpret_cast<GdkEvent*>($event));
            }
        }

        return TRUE;
    }

    gint Window::focusEvent(GtkWidget */*$widget*/, GdkEventFocus *$event, Window *$self) {
        CEF_REQUIRE_UI_THREAD();

        if ($self->browser != nullptr) {
            $self->browser->GetHost()->SendFocusEvent($event->in == TRUE);
        }

        return TRUE;
    }

    gint Window::sizeAllocationEvent(GtkWidget */*$widget*/, GtkAllocation */*$allocation*/, Window *$self) {
        CEF_REQUIRE_UI_THREAD();

        if ($self->browser != nullptr) {
            $self->browser->GetHost()->WasResized();
        }

        return TRUE;
    }

    void Window::dragBeginEvent(GtkWidget */*$widget*/, GdkDragContext */*$dragContext*/, Window *$self) {
        CEF_REQUIRE_UI_THREAD();

        if (!$self->dragAndDrop.data->HasImage()) {
            LogIt::Error("Failed to set drag icon, drag image not available");

            return;
        }

        int pixelWidth = 0;
        int pixelHeight = 0;
        const CefRefPtr<CefBinaryValue> imageInBinaryForm = $self->dragAndDrop.data->GetImage()->GetAsPNG(
                $self->deviceScaleFactor,
                true,
                pixelWidth,
                pixelHeight);

        if (imageInBinaryForm == nullptr) {
            LogIt::Error("Failed to set drag icon, drag image error");

            return;
        }

        GError *error = nullptr;
        GdkPixbufLoader *loader = gdk_pixbuf_loader_new_with_type("png", &error);

        if (loader != nullptr && error == nullptr) {
            const size_t imageSize = imageInBinaryForm->GetSize();
            auto imageBuffer = new guint8[imageSize];
            imageInBinaryForm->GetData(static_cast<void *>(imageBuffer), imageSize, 0);

            gboolean success = gdk_pixbuf_loader_write(loader, imageBuffer, imageSize, nullptr);
            if (success != 0) {
                success = gdk_pixbuf_loader_close(loader, nullptr);
                if (success != 0) {
                    GdkPixbuf *pixelBuffer = gdk_pixbuf_loader_get_pixbuf(loader);
                    if (pixelBuffer != nullptr) {
                        const CefPoint imageHotspot = $self->dragAndDrop.data->GetImageHotspot();

                        gtk_drag_set_icon_pixbuf($self->dragAndDrop.context, pixelBuffer, imageHotspot.x, imageHotspot.y);
                    } else {
                        LogIt::Error("Failed to set drag icon, pixel buffer error");
                    }
                } else {
                    LogIt::Error("Failed to set drag icon, loader close error");
                }
            } else {
                LogIt::Error("Failed to set drag icon, loader write error");
            }

            delete[] imageBuffer;
            imageBuffer = nullptr;
        } else {
            LogIt::Error("Failed to set drag icon, loader creation error");
        }

        if (loader != nullptr) {
            g_object_unref(loader);
        }

        if (error != nullptr) {
            g_error_free(error);
        }
    }

    void Window::dragEndEvent(GtkWidget */*$widget*/, GdkDragContext */*$dragContext*/, Window *$self) {
        CEF_REQUIRE_UI_THREAD();

        if ($self->browser != nullptr) {
            if (!$self->dragAndDrop.isDrop) {
                $self->browser->GetHost()->DragSourceEndedAt(-1, -1, $self->dragAndDrop.operation);
            }

            $self->browser->GetHost()->DragSourceSystemDragEnded();
        }

        $self->dragAndDrop.Reset();
    }

    gboolean Window::dragMotionEvent(GtkWidget *$widget,
                                     GdkDragContext *$dragContext,
                                     gint $x,
                                     gint $y,
                                     guint $time,
                                     Window *$self) {
        CEF_REQUIRE_UI_THREAD();

        CefMouseEvent mouseEventForMove = Mouse::GetCefMouseEvent($x, $y, EVENTFLAG_LEFT_MOUSE_BUTTON);
        $self->applyPopupOffset(mouseEventForMove.x, mouseEventForMove.y);
        Utils::DeviceToLogical(mouseEventForMove, $self->deviceScaleFactor);

        if ($self->browser != nullptr) {
            $self->browser->GetHost()->SendMouseMoveEvent(mouseEventForMove, $self->dragAndDrop.isLeave);
        }

        CefMouseEvent mouseEvent = Mouse::GetCefMouseEvent($x, $y, EVENTFLAG_LEFT_MOUSE_BUTTON);

        const CefBrowserHost::DragOperationsMask allowedOperations = Utils::GetDragOperationsMask($self->dragAndDrop.context);

        if ($self->dragAndDrop.isLeave && $self->browser != nullptr) {
            $self->browser->GetHost()->DragTargetDragEnter($self->dragAndDrop.data, mouseEvent, allowedOperations);
        }

        if ($self->browser != nullptr) {
            $self->browser->GetHost()->DragTargetDragOver(mouseEvent, allowedOperations);
        }

        if ($widget == $self->paintingArea) {
            gdk_drag_status($dragContext, GDK_ACTION_COPY, $time);

            if ($self->dragAndDrop.isLeave) {
                $self->dragAndDrop.isLeave = false;
            }

            return TRUE;
        }

        LogIt::Warning("Invalid drag destination widget");

        gdk_drag_status($dragContext, static_cast<GdkDragAction>(0), $time);

        return FALSE;
    }

    void Window::dragLeaveEvent(GtkWidget */*$widget*/, GdkDragContext */*$dragContext*/, guint /*$time*/, Window *$self) {
        CEF_REQUIRE_UI_THREAD();

        if ($self->browser != nullptr) {
            $self->browser->GetHost()->DragTargetDragLeave();
        }

        $self->dragAndDrop.isLeave = true;
    }

    gboolean Window::dragFailedEvent(GtkWidget */*$widget*/, GdkDragContext */*$dragContext*/, GtkDragResult /*$result*/, Window *$self) {
        CEF_REQUIRE_UI_THREAD();

        if ($self->browser != nullptr) {
            $self->browser->GetHost()->DragSourceEndedAt(-1, -1, $self->dragAndDrop.operation);
            $self->browser->GetHost()->DragSourceSystemDragEnded();
        }

        $self->dragAndDrop.Reset();

        return TRUE;
    }

    gboolean Window::dragDropEvent(GtkWidget */*$widget*/,
                                   GdkDragContext */*$dragContext*/,
                                   gint $x,
                                   gint $y,
                                   guint $time,
                                   Window *$self) {
        CEF_REQUIRE_UI_THREAD();

        gtk_drag_finish($self->dragAndDrop.context, TRUE, FALSE, $time);

        CefMouseEvent mouseEvent = Mouse::GetCefMouseEvent($x, $y, EVENTFLAG_LEFT_MOUSE_BUTTON);

        const CefBrowserHost::DragOperationsMask allowedOperations = Utils::GetDragOperationsMask($self->dragAndDrop.context);

        if ($self->dragAndDrop.isLeave && $self->browser != nullptr) {
            $self->browser->GetHost()->DragTargetDragEnter($self->dragAndDrop.data, mouseEvent, allowedOperations);
            $self->browser->GetHost()->DragTargetDragOver(mouseEvent, allowedOperations);
        }

        if ($self->browser != nullptr) {
            $self->browser->GetHost()->DragTargetDrop(mouseEvent);
        }

        if ($self->browser != nullptr) {
            $self->browser->GetHost()->DragSourceEndedAt($x, $y, $self->dragAndDrop.operation);
        }

        $self->dragAndDrop.isDrop = true;

        return TRUE;
    }
}

#endif  // LINUX_PLATFORM

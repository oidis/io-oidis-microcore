/* ********************************************************************************************************* *
 *
 * Copyright (c) 2015 The Chromium Embedded Framework Authors
 * Copyright (c) 2018 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifdef WIN_PLATFORM

#include <include/wrapper/cef_helpers.h>

#include "../sourceFilesMap.hpp"

// has to be here, because of the same symbols defined in the CEF
#include <windowsx.h> // NOLINT

namespace Io::Oidis::Microcore::Windows {
    using Io::Oidis::XCppCommons::Enums::LogLevel;
    using Io::Oidis::XCppCommons::Utils::LogIt;

    namespace Exceptions = Io::Oidis::Microcore::Exceptions::WindowEvents;
    namespace Keyboard = Io::Oidis::Microcore::Hardware::Keyboard;
    namespace Mouse = Io::Oidis::Microcore::Hardware::Mouse;
    namespace Utils = Io::Oidis::Microcore::Helpers::Utils;

    LRESULT __stdcall Window::windowHandleEvent(HWND $hwnd, const UINT $message, const WPARAM $wParam, const LPARAM $lParam) {
        const auto self = Utils::GetUserDataPtr<Window *>($hwnd);
        if (self == nullptr) {
            throw Exceptions::IncorrectUserDataInformation{};
        }

        CEF_REQUIRE_UI_THREAD();

        switch ($message) {
            case WM_LBUTTONDOWN:
            case WM_RBUTTONDOWN:
            case WM_MBUTTONDOWN:
            case WM_LBUTTONUP:
            case WM_RBUTTONUP:
            case WM_MBUTTONUP:
            case WM_MOUSEMOVE:
            case WM_MOUSELEAVE:
            case WM_MOUSEWHEEL:
                self->mouseEvent($message, $wParam, $lParam);
                break;

            case WM_SIZE:
                self->sizeAllocationEvent();
                break;

            case WM_SYSCHAR:
            case WM_SYSKEYDOWN:
            case WM_SYSKEYUP:
            case WM_KEYDOWN:
            case WM_KEYUP:
            case WM_CHAR:
                self->keyEvent($message, $wParam, $lParam);
                break;

            case WM_PAINT:
                self->paintEvent();
                break;

            case WM_ERASEBKGND:
                return 0;

            case WM_NCDESTROY:
                Utils::SetUserDataPtr($hwnd, nullptr);
                break;

            case WM_CLOSE:
                self->close();
                break;

            default:
                break;
        }

        return DefWindowProc($hwnd, $message, $wParam, $lParam);
    }

    CefBrowserHost::DragOperationsMask Window::DragBeginEvent(CefMouseEvent $event,
                                                              const CefRefPtr<CefDragData> $dragData,
                                                              const CefBrowserHost::DragOperationsMask $effect) {
        if (this->browser != nullptr) {
            Utils::DeviceToLogical($event, this->deviceScaleFactor);

            this->browser->GetHost()->DragTargetDragEnter($dragData, $event, $effect);
            this->browser->GetHost()->DragTargetDragOver($event, $effect);
        }

        return this->dragAndDrop.operation;
    }

    CefBrowserHost::DragOperationsMask Window::DragMotionEvent(CefMouseEvent $event, const CefBrowserHost::DragOperationsMask $effect) {
        if (this->browser != nullptr) {
            Utils::DeviceToLogical($event, this->deviceScaleFactor);

            this->browser->GetHost()->DragTargetDragOver($event, $effect);
        }

        return this->dragAndDrop.operation;
    }

    void Window::DragLeaveEvent() {
        if (this->browser != nullptr) {
            this->browser->GetHost()->DragTargetDragLeave();
        }
    }

    CefBrowserHost::DragOperationsMask Window::DragDropEvent(CefMouseEvent $event, const CefBrowserHost::DragOperationsMask $effect) {
        if (this->browser != nullptr) {
            Utils::DeviceToLogical($event, this->deviceScaleFactor);

            this->browser->GetHost()->DragTargetDragOver($event, $effect);
            this->browser->GetHost()->DragTargetDrop($event);
        }

        return this->dragAndDrop.operation;
    }

    void Window::sizeAllocationEvent() {
        GetClientRect(this->handle, &this->clientRect);

        if (this->browser != nullptr) {
            this->browser->GetHost()->WasResized();
        }
    }

    void Window::paintEvent() {
        PAINTSTRUCT ps;
        BeginPaint(this->handle, &ps);
        EndPaint(this->handle, &ps);

        if (this->browser != nullptr) {
            this->browser->GetHost()->Invalidate(PET_VIEW);
        }
    }

    void Window::keyEvent(UINT $message, WPARAM $wParam, LPARAM $lParam) {
        if (this->browser != nullptr) {
            CefKeyEvent event;
            event.windows_key_code = $wParam;
            event.native_key_code = $lParam;
            event.is_system_key = $message == WM_SYSCHAR || $message == WM_SYSKEYDOWN || $message == WM_SYSKEYUP;

            if ($message == WM_KEYDOWN || $message == WM_SYSKEYDOWN) {
                event.type = KEYEVENT_RAWKEYDOWN;
            } else if ($message == WM_KEYUP || $message == WM_SYSKEYUP) {
                event.type = KEYEVENT_KEYUP;
            } else {
                event.type = KEYEVENT_CHAR;
            }
            event.modifiers = Keyboard::GetCefKeyboardModifiers($wParam, $lParam);

            this->browser->GetHost()->SendKeyEvent(event);
        }
    }

    void Window::mouseEvent(UINT $message, WPARAM $wParam, LPARAM $lParam) {
        CefRefPtr<CefBrowserHost> browserHost;

        if (this->browser != nullptr) {
            browserHost = browser->GetHost();
        }

        LONG currentTime = 0;
        bool cancelPreviousClick = false;
        const POINT mousePoint = POINT{GET_X_LPARAM($lParam), GET_Y_LPARAM($lParam)};

        const CefBrowserHost::MouseButtonType btnType =
                ($message == WM_LBUTTONDOWN ? MBT_LEFT : ($message == WM_RBUTTONDOWN ? MBT_RIGHT : MBT_MIDDLE));

        if ($message == WM_LBUTTONDOWN || $message == WM_RBUTTONDOWN ||
            $message == WM_MBUTTONDOWN || $message == WM_MOUSEMOVE ||
            $message == WM_MOUSELEAVE) {
            currentTime = GetMessageTime();
            cancelPreviousClick =
                    (abs(this->mouse.lastClickX - mousePoint.x) > (GetSystemMetrics(SM_CXDOUBLECLK) / 2)) ||
                    (abs(this->mouse.lastClickY - mousePoint.y) > (GetSystemMetrics(SM_CYDOUBLECLK) / 2)) ||
                    ((currentTime - this->mouse.lastClickTime) > GetDoubleClickTime());
            if (cancelPreviousClick && ($message == WM_MOUSEMOVE || $message == WM_MOUSELEAVE)) {
                this->mouse.lastClickCount = 0;
                this->mouse.lastClickX = 0;
                this->mouse.lastClickY = 0;
                this->mouse.lastClickTime = 0;
            }
        }

        switch ($message) {
            case WM_LBUTTONDOWN:
            case WM_RBUTTONDOWN:
            case WM_MBUTTONDOWN: {
                SetCapture(this->handle);
                SetFocus(this->handle);

                if (!cancelPreviousClick && (btnType == this->mouse.lastClickButton)) {
                    ++this->mouse.lastClickCount;
                } else {
                    this->mouse.lastClickCount = 1;
                    this->mouse.lastClickX = mousePoint.x;
                    this->mouse.lastClickY = mousePoint.y;
                }
                this->mouse.lastClickTime = currentTime;
                this->mouse.lastClickButton = btnType;

                if (browserHost != nullptr) {
                    CefMouseEvent mouseEvent = Mouse::GetCefMouseEvent(mousePoint, $wParam);
                    this->mouse.lastMouseDownOnView = !isOverPopupWidget(mousePoint.x, mousePoint.y);
                    this->applyPopupOffset(mouseEvent.x, mouseEvent.y);
                    Utils::DeviceToLogical(mouseEvent, this->deviceScaleFactor);
                    browserHost->SendMouseClickEvent(mouseEvent, btnType, false, this->mouse.lastClickCount);
                }
            }
                break;

            case WM_LBUTTONUP:
            case WM_RBUTTONUP:
            case WM_MBUTTONUP:
                if (GetCapture() == this->handle) {
                    ReleaseCapture();
                }
                if (browserHost != nullptr) {
                    CefMouseEvent mouseEvent = Mouse::GetCefMouseEvent(mousePoint, $wParam);
                    if (this->mouse.lastMouseDownOnView && isOverPopupWidget(mousePoint.x, mousePoint.y) &&
                        (this->getPopupXOffset() != 0 || this->getPopupYOffset() != 0)) {
                        break;
                    }
                    this->applyPopupOffset(mouseEvent.x, mouseEvent.y);
                    Utils::DeviceToLogical(mouseEvent, deviceScaleFactor);
                    browserHost->SendMouseClickEvent(mouseEvent, btnType, true, this->mouse.lastClickCount);
                }
                break;

            case WM_MOUSEMOVE: {
                if (!this->mouse.mouseTracking) {
                    TRACKMOUSEEVENT tme;
                    tme.cbSize = sizeof(TRACKMOUSEEVENT);
                    tme.dwFlags = TME_LEAVE;
                    tme.hwndTrack = this->handle;
                    TrackMouseEvent(&tme);
                    this->mouse.mouseTracking = true;
                }

                if (browserHost != nullptr) {
                    CefMouseEvent mouseEvent = Mouse::GetCefMouseEvent(mousePoint, $wParam);
                    this->applyPopupOffset(mouseEvent.x, mouseEvent.y);
                    Utils::DeviceToLogical(mouseEvent, this->deviceScaleFactor);
                    browserHost->SendMouseMoveEvent(mouseEvent, false);
                }
                break;
            }

            case WM_MOUSELEAVE: {
                if (this->mouse.mouseTracking) {
                    TRACKMOUSEEVENT tme;
                    tme.cbSize = sizeof(TRACKMOUSEEVENT);
                    tme.dwFlags = TME_LEAVE & TME_CANCEL;
                    tme.hwndTrack = this->handle;
                    TrackMouseEvent(&tme);
                    this->mouse.mouseTracking = false;
                }

                if (browserHost != nullptr) {
                    POINT cursorPosition = {0, 0};
                    GetCursorPos(&cursorPosition);
                    ScreenToClient(this->handle, &cursorPosition);

                    CefMouseEvent mouseEvent = Mouse::GetCefMouseEvent(cursorPosition, $wParam);
                    Utils::DeviceToLogical(mouseEvent, this->deviceScaleFactor);
                    browserHost->SendMouseMoveEvent(mouseEvent, true);
                }
            }
                break;

            case WM_MOUSEWHEEL:
                if (browserHost != nullptr) {
                    POINT cursorPosition = {GET_X_LPARAM($lParam), GET_Y_LPARAM($lParam)};
                    HWND scrolledWindow = WindowFromPoint(cursorPosition);
                    if (scrolledWindow != this->handle) {
                        break;
                    }

                    ScreenToClient(this->handle, &cursorPosition);
                    const int delta = GET_WHEEL_DELTA_WPARAM($wParam);

                    CefMouseEvent mouseEvent = Mouse::GetCefMouseEvent(cursorPosition, $wParam);
                    this->applyPopupOffset(mouseEvent.x, mouseEvent.y);
                    Utils::DeviceToLogical(mouseEvent, this->deviceScaleFactor);
                    browserHost->SendMouseWheelEvent(mouseEvent,
                                                     Keyboard::IsKeyDown(VK_SHIFT) ? delta : 0,
                                                     !Keyboard::IsKeyDown(VK_SHIFT) ? delta : 0);
                }
                break;
            default:
                break;
        }
    }
}

#endif  // WIN_PLATFORM

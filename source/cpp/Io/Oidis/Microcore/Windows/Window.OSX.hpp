/* ********************************************************************************************************* *
 *
 * Copyright (c) 2015 The Chromium Embedded Framework Authors
 * Copyright (c) 2018 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_MICROCORE_WINDOWS_WINDOW_OSX_HPP_
#define IO_OIDIS_MICROCORE_WINDOWS_WINDOW_OSX_HPP_

#ifdef MAC_PLATFORM

#include "../Hardware/Keyboard.OSX.hpp"
#include "../Windows/WindowBase.hpp"

namespace Io::Oidis::Microcore::Windows {
    class Window : public WindowBase {
        using DragAndDrop = Io::Oidis::Microcore::DragAndDrop::DragAndDrop;

     public:
        using WindowBase::WindowBase;

        void clickEvent(NSEvent *$event, CefBrowserHost::MouseButtonType $buttonType, NSPoint $point, bool $isUp);

        void scrollEvent(NSEvent *$event, const NSPoint $point);

        void moveEvent(NSEvent *$event, NSPoint $point);

        void keyEvent(NSEvent *$event, bool $isUp);

        void inputFlagsChanged(NSEvent *$event);

        void paintingAreaChangeBackingPropertiesEvent();

        void drawRectangleEvent(NSRect $dirtyRectangle, bool $isInResizeProcess);

        NSDragOperation dragBeginEvent(NSPoint $point, NSPasteboard *$pastebard, NSDragOperation $operation);

        void dragEndEvent();

        NSDragOperation dragMotionEvent(NSPoint $point, NSDragOperation $operationMask);

        bool dragDropEvent(NSPoint $point);

        void dragImageEvent(NSPoint $point, NSDragOperation $operation);

        void pasteboardProvideEvent(NSPasteboard *$pasteboard, NSString *$type);

        DragAndDrop getDragAndDrop() const;

     private:
        void OnPaint(CefRenderHandler::PaintElementType $type,
                     const CefRenderHandler::RectList &$dirtyRects,
                     const void *$buffer,
                     int $width,
                     int $height) override;

        bool GetViewRect(CefRect &$rect) override;

        bool GetScreenPoint(int $viewX, int $viewY, int &$screenX, int &$screenY) override;

        void OnCursorChange(CefCursorHandle $cursor) override;

        bool StartDragging(const CefRefPtr<CefBrowser> $browser,
                           const CefRefPtr<CefDragData> $cefDragData,
                           CefRenderHandler::DragOperationsMask $allowedOps,
                           int $x,
                           int $y) override;

        void startOpenGl() override;

        void stopOpenGl() override;

        void createNativeWindow() override;

        void destroyNativeWindow() override;

        CefWindowHandle getCefWindowHandle() const override;

        void show() override;

        void hide() override;

        void registerEvents() override;

        void unregisterEvents() override;

        void registerDragAndDrop() override;

        void unregisterDragAndDrop() override;

        void setDeviceScaleFactorAccordingToDisplay() override;

        void makeTransparent() override;

        void createNSWindow();

        void createGlArea();

        void closeWindowEvent();

        void changeBackingPropertiesEvent();

        int getModifiersForEvent(NSEvent *$event) const;

        void commandKeyEvent(NSEvent *$event);

        void plainKeyEvent(NSEvent *$event, bool $isUp);

        void sizeAllocationEvent();

        void createKeyboardHandler();

        void destroyKeyboardHandler();

        void populateDropData(CefRefPtr<CefDragData> $data, NSPasteboard *$pasteboard);

        std::vector<id> eventObservers;

        Keyboard *keyboard = nullptr;
    };
}

#endif  // MAC_PLATFORM

#endif  // IO_OIDIS_MICROCORE_WINDOWS_WINDOW_OSX_HPP_

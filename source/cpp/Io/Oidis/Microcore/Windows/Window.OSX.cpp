/* ********************************************************************************************************* *
 *
 * Copyright (c) 2015 The Chromium Embedded Framework Authors
 * Copyright (c) 2018 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifdef MAC_PLATFORM

#include <include/internal/cef_mac.h>
#include <include/wrapper/cef_helpers.h>

#include "../sourceFilesMap.hpp"

namespace Io::Oidis::Microcore::Windows {
    using Io::Oidis::Microcore::DragAndDrop::DragAndDrop;
    using Io::Oidis::Microcore::OpenGL::OpenGLContext;

    namespace Exceptions = Io::Oidis::Microcore::Exceptions;
    namespace Utils = Io::Oidis::Microcore::Helpers::Utils;

    DragAndDrop Window::getDragAndDrop() const {
        return this->dragAndDrop;
    }

    void Window::OnPaint(const CefRenderHandler::PaintElementType $type,
                         const CefRenderHandler::RectList &$dirtyRects,
                         const void *$buffer,
                         const int $width,
                         const int $height) {
        WindowBase::onPaintBegin();

        if (WindowBase::shouldPaint($width, $height)) {
            const OpenGLContext openGLContext(this->paintingArea, true);

            WindowBase::onPaintEnd($type, $dirtyRects, $buffer, $width, $height);
        }
    }

    bool Window::GetViewRect(CefRect &$rect) {
        const auto bounds = [this->paintingArea convertRectToBacking: this->paintingArea.bounds];

        return WindowBase::getViewRect($rect, { static_cast<int>(bounds.size.width), static_cast<int>(bounds.size.height) });
    }

    bool Window::GetScreenPoint(const int $viewX, const int $viewY, int &$screenX, int &$screenY) {
        if (this->isPaintingAreaReady()) {
            NSPoint viewPoint = NSMakePoint(Utils::LogicalToDevice($viewX, this->deviceScaleFactor),
                                            Utils::LogicalToDevice($viewY, this->deviceScaleFactor));
            viewPoint = [this->paintingArea convertPointFromBacking:viewPoint];
            viewPoint.y = [this->paintingArea bounds].size.height - viewPoint.y;

            const NSPoint windowPoint = [this->paintingArea convertPoint:viewPoint toView:nil];
            const NSRect rectanglePoint = NSMakeRect(windowPoint.x, windowPoint.y, 0, 0);
            const NSPoint screenPoint = [this->paintingArea.window convertRectToScreen:rectanglePoint].origin;

            $screenX = screenPoint.x;
            $screenY = screenPoint.y;

            return true;
        }

        return false;
    }

    void Window::OnCursorChange(CefCursorHandle $cursor) {
        if (this->isPaintingAreaReady()) {
            [$cursor set];
        }
    }

    bool Window::StartDragging(const CefRefPtr<CefBrowser> /*$browser*/,
                               const CefRefPtr<CefDragData> $cefDragData,
                               const CefRenderHandler::DragOperationsMask $allowedOps,
                               const int $x,
                               const int $y) {
        if (this->isPaintingAreaReady()) {
            NSPoint point = NSMakePoint(Utils::LogicalToDevice($x, this->deviceScaleFactor),
                                        Utils::LogicalToDevice($y, this->deviceScaleFactor));

            point = [this->paintingArea convertPointFromBacking:point];

            this->dragAndDrop.Reset();

            this->dragAndDrop.allowedOperations = static_cast<NSDragOperation>($allowedOps);
            this->dragAndDrop.data = $cefDragData;

            this->dragAndDrop.FillPasteboard(this->paintingArea);

            const NSEvent *currentEvent = [[NSApplication sharedApplication] currentEvent];

            NSEvent *dragEvent = [NSEvent mouseEventWithType:NSLeftMouseDragged
                                          location:point
                                          modifierFlags:NSLeftMouseDraggedMask
                                          timestamp:currentEvent.timestamp
                                          windowNumber:this->paintingArea.window.windowNumber
                                          context:nil
                                          eventNumber:0
                                          clickCount:1
                                          pressure:1.0];

            [this->paintingArea.window dragImage:nil
                                       at:point
                                       offset:NSZeroSize
                                       event:dragEvent
                                       pasteboard:this->dragAndDrop.pasteboard
                                       source:this->paintingArea
                                       slideBack:YES];

            return true;
        }

        return false;
    }

    void Window::startOpenGl() {
        const OpenGLContext openGLContext(this->paintingArea, false);

        this->renderer.Initialize();
    }

    void Window::stopOpenGl() {
        const OpenGLContext openGLContext(this->paintingArea, false);

        this->renderer.Cleanup();
    }

    void Window::createNativeWindow() {
        CEF_REQUIRE_UI_THREAD();

        this->createKeyboardHandler();
        this->createNSWindow();
    }

    void Window::destroyNativeWindow() {
        [this->paintingArea Detach];
        [this->paintingArea removeFromSuperview];
        [this->paintingArea release];

        [this->handle release];

        this->destroyKeyboardHandler();
    }

    CefWindowHandle Window::getCefWindowHandle() const {
        return this->paintingArea;
    }

    void Window::show() {
        [this->handle makeKeyAndOrderFront:nil];
    }

    void Window::hide() {
        [this->handle orderOut:nil];
    }

    void Window::registerEvents() {
        this->eventObservers.emplace_back(
             [[NSNotificationCenter defaultCenter] addObserverForName:NSWindowWillCloseNotification
                                                   object:this->handle
                                                   queue:nil
                                                   usingBlock:^(NSNotification */*note*/) {
                                                       this->closeWindowEvent();
                                                   }]);

        this->eventObservers.emplace_back(
            [[NSNotificationCenter defaultCenter] addObserverForName:NSWindowDidChangeBackingPropertiesNotification
                                                  object:this->handle
                                                  queue:nil
                                                  usingBlock:^(NSNotification */*note*/) {
                                                        this->changeBackingPropertiesEvent();
                                                  }]);
        this->eventObservers.emplace_back(
            [[NSNotificationCenter defaultCenter] addObserverForName:NSWindowDidResizeNotification
                                                  object:this->handle
                                                  queue:nil
                                                  usingBlock:^(NSNotification */*note*/) {
                                                        this->sizeAllocationEvent();
                                                  }]);
    }

    void Window::unregisterEvents() {
        std::for_each(std::cbegin(this->eventObservers), std::cend(this->eventObservers), [](const id &observerId) {
            [[NSNotificationCenter defaultCenter] removeObserver:observerId];
        });

        this->eventObservers.clear();
        this->eventObservers.shrink_to_fit();
    }

    void Window::registerDragAndDrop() {
        this->dragAndDrop.Reset();

        [this->paintingArea registerForDraggedTypes:this->dragAndDrop.GetDraggedTypes()];
    }

    void Window::unregisterDragAndDrop() {
        [this->paintingArea unregisterDraggedTypes];

        this->dragAndDrop.Reset();
    }

    void Window::setDeviceScaleFactorAccordingToDisplay() {
        this->deviceScaleFactor = [this->paintingArea.window backingScaleFactor];
    }

    void Window::makeTransparent() {
        [this->paintingArea MakeTransparent];
        [this->handle MakeTransparent];
    }

    void Window::createNSWindow() {
        this->handle = [[NativeWindow alloc]
                        Initialize:NSMakeRect(0, 0, this->configuration.dimensions.width, this->configuration.dimensions.height)
                        decorated:this->configuration.decorated];

        if (this->handle == nullptr) {
            throw Exceptions::WindowBase::FailedToInitialize{this->configuration.id, "Could not create NSWindow instance"};
        }

        this->createGlArea();

        [this->handle.contentView addSubview:this->paintingArea];
    }

    void Window::createGlArea() {
        this->paintingArea = [[NativeOpenGLView alloc] Initialize:this->handle.frame
                                                       window:this
                                                       textInputContext:[this->keyboard getTextInputContext]];

        if (this->paintingArea == nullptr) {
            throw Exceptions::WindowBase::FailedToInitialize{this->configuration.id, "Could not create painting area"};
        }
    }

    void Window::createKeyboardHandler() {
        this->keyboard = [[Keyboard alloc] Initialize:this];

        if (this->keyboard == nullptr) {
            throw Exceptions::Keyboard::KeyboardError{"Failed to create Keyboard instance"};
        }
    }

    void Window::destroyKeyboardHandler() {
        [this->keyboard release];
        this->keyboard = nullptr;
    }

    void Window::populateDropData(CefRefPtr<CefDragData> $data, NSPasteboard *$pasteboard) {
        if ([$pasteboard.types containsObject:NSStringPboardType]) {
            $data->SetFragmentText([[$pasteboard stringForType:NSStringPboardType] UTF8String]);
        }

        if ([$pasteboard.types containsObject:NSFilenamesPboardType]) {
            const NSArray *files = [$pasteboard propertyListForType:NSFilenamesPboardType];

            if ([files isKindOfClass:[NSArray class]] && [files count] > 0) {
                for (NSString *filename in files) {
                    if ([[NSFileManager defaultManager] fileExistsAtPath:filename]) {
                        $data->AddFile([filename UTF8String], CefString());
                    }
                }
            }
        }
    }
}

#endif  // MAC_PLATFORM

/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_MICROCORE_HANDLERS_RENDERHANDLER_HPP_
#define IO_OIDIS_MICROCORE_HANDLERS_RENDERHANDLER_HPP_

#include <include/cef_render_handler.h>

namespace Io::Oidis::Microcore::Handlers {
    class RenderHandler : public CefRenderHandler {
        using IOsrDelegate = Io::Oidis::Microcore::Interfaces::IOsrDelegate;

     public:
        explicit RenderHandler(IOsrDelegate *$osrDelegate);

     private:
        bool GetViewRect(CefRefPtr <CefBrowser> $browser, CefRect &$rect) override;

        void OnPaint(CefRefPtr <CefBrowser> $browser,
                     PaintElementType $type,
                     const RectList &$dirtyRects,
                     const void *$buffer,
                     int $width,
                     int $height) override;

        bool GetScreenInfo(CefRefPtr<CefBrowser> $browser, CefScreenInfo &$screenInfo) override;

        bool GetScreenPoint(CefRefPtr <CefBrowser> $browser, int $viewX, int $viewY, int &$screenX, int &$screenY) override;

        void OnCursorChange(CefRefPtr <CefBrowser> $browser,
                            CefCursorHandle $cursor,
                            CursorType $type,
                            const CefCursorInfo &$customCursorInfo) override;

        bool StartDragging(CefRefPtr <CefBrowser> $browser,
                           CefRefPtr <CefDragData> $dragData,
                           CefRenderHandler::DragOperationsMask $allowedOps,
                           int $x,
                           int $y) override;

        void UpdateDragCursor(CefRefPtr <CefBrowser> $browser, CefRenderHandler::DragOperation $operation) override;

        IOsrDelegate *osrDelegate = nullptr;

        IMPLEMENT_REFCOUNTING(RenderHandler);
    };
}

#endif  // IO_OIDIS_MICROCORE_HANDLERS_RENDERHANDLER_HPP_

/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_MICROCORE_HANDLERS_LIFESPANHANDLER_HPP_
#define IO_OIDIS_MICROCORE_HANDLERS_LIFESPANHANDLER_HPP_

#include <include/cef_life_span_handler.h>

namespace Io::Oidis::Microcore::Handlers {
    class LifeSpanHandler : public CefLifeSpanHandler {
        using IOsrDelegate = Io::Oidis::Microcore::Interfaces::IOsrDelegate;

     public:
        explicit LifeSpanHandler(IOsrDelegate *$osrDelegate);

     private:
        void OnAfterCreated(CefRefPtr <CefBrowser> $browser) override;

        void OnBeforeClose(CefRefPtr <CefBrowser> $browser) override;

        bool OnBeforePopup(CefRefPtr<CefBrowser> $browser,
                           CefRefPtr<CefFrame> $frame,
                           const CefString &$targetUrl,
                           const CefString &$targetFrameName,
                           WindowOpenDisposition $targetDisposition,
                           bool $userGesture,
                           const CefPopupFeatures &$popupFeatures,
                           CefWindowInfo &$windowInfo,
                           CefRefPtr<CefClient> &$client,
                           CefBrowserSettings &$settings,
                           bool *$noJavascriptAccess) override;

        IOsrDelegate *osrDelegate = nullptr;

        IMPLEMENT_REFCOUNTING(LifeSpanHandler);
    };
}

#endif  // IO_OIDIS_MICROCORE_HANDLERS_LIFESPANHANDLER_HPP_

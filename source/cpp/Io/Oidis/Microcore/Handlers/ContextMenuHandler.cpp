/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

namespace Io::Oidis::Microcore::Handlers {
    void ContextMenuHandler::OnBeforeContextMenu(CefRefPtr<CefBrowser> /*$browser*/,
                             CefRefPtr<CefFrame> /*$frame*/,
                             CefRefPtr<CefContextMenuParams> /*$params*/,
                             CefRefPtr<CefMenuModel> $model) {
        this->disableContextMenu($model);
    }

    void ContextMenuHandler::disableContextMenu(CefRefPtr<CefMenuModel> $model) const {
        $model->Clear();
    }
}

/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include <include/wrapper/cef_helpers.h>

#include "../sourceFilesMap.hpp"

namespace Io::Oidis::Microcore::Handlers {
    using Io::Oidis::Microcore::Interfaces::IOsrDelegate;

    RenderHandler::RenderHandler(IOsrDelegate *$osrDelegate)
            : osrDelegate{$osrDelegate} {
    }

    bool RenderHandler::GetViewRect(CefRefPtr<CefBrowser> /*$browser*/, CefRect &$rect) {
        CEF_REQUIRE_UI_THREAD();

        return this->osrDelegate != nullptr ? this->osrDelegate->GetViewRect($rect) : false;
    }

    void RenderHandler::OnPaint(CefRefPtr<CefBrowser> /*$browser*/,
                                const PaintElementType $type,
                                const RectList &$dirtyRects,
                                const void *$buffer,
                                const int $width,
                                const int $height) {
        CEF_REQUIRE_UI_THREAD();

        if (this->osrDelegate != nullptr) {
            this->osrDelegate->OnPaint($type, $dirtyRects, $buffer, $width, $height);
        }
    }

    bool RenderHandler::GetScreenInfo(CefRefPtr<CefBrowser> $browser, CefScreenInfo &$screenInfo) {
        CEF_REQUIRE_UI_THREAD();

        return this->osrDelegate != nullptr ? this->osrDelegate->GetScreenInfo($screenInfo) : false;
    }

    bool RenderHandler::GetScreenPoint(CefRefPtr<CefBrowser> /*$browser*/,
                                       const int $viewX,
                                       const int $viewY,
                                       int &$screenX,
                                       int &$screenY) {
        CEF_REQUIRE_UI_THREAD();

        return this->osrDelegate != nullptr ? this->osrDelegate->GetScreenPoint($viewX, $viewY, $screenX, $screenY) : false;
    }

    void RenderHandler::OnCursorChange(CefRefPtr<CefBrowser> /*$browser*/,
                                       CefCursorHandle $cursor,
                                       CursorType /*$type*/,
                                       const CefCursorInfo &/*$customCursorInfo*/) {
        CEF_REQUIRE_UI_THREAD();

        if (this->osrDelegate != nullptr) {
            this->osrDelegate->OnCursorChange($cursor);
        }
    }

    bool RenderHandler::StartDragging(const CefRefPtr <CefBrowser> $browser,
                                      const CefRefPtr <CefDragData> $dragData,
                                      const CefRenderHandler::DragOperationsMask $allowedOps,
                                      int $x,
                                      int $y) {
        CEF_REQUIRE_UI_THREAD();

        return this->osrDelegate != nullptr ? this->osrDelegate->StartDragging($browser, $dragData, $allowedOps, $x, $y) : false;
    }

    void RenderHandler::UpdateDragCursor(CefRefPtr<CefBrowser> /*$browser*/, const CefRenderHandler::DragOperation $operation) {
        CEF_REQUIRE_UI_THREAD();

        if (this->osrDelegate != nullptr) {
            this->osrDelegate->UpdateDragCursor($operation);
        }
    }
}

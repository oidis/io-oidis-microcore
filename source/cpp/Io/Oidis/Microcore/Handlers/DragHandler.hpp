/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_MICROCORE_HANDLERS_DRAGHANDLER_HPP_
#define IO_OIDIS_MICROCORE_HANDLERS_DRAGHANDLER_HPP_

#include <include/cef_drag_handler.h>

namespace Io::Oidis::Microcore::Handlers {
    class DragHandler : public CefDragHandler {
     private:
        bool OnDragEnter(CefRefPtr <CefBrowser> $browser,
                         CefRefPtr <CefDragData> $dragData,
                         CefDragHandler::DragOperationsMask $mask) override;

        IMPLEMENT_REFCOUNTING(DragHandler);
    };
}

#endif  // IO_OIDIS_MICROCORE_HANDLERS_DRAGHANDLER_HPP_

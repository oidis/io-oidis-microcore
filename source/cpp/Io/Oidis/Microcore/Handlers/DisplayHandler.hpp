/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_MICROCORE_HANDLERS_DISPLAYHANDLER_HPP_
#define IO_OIDIS_MICROCORE_HANDLERS_DISPLAYHANDLER_HPP_

#include <include/cef_display_handler.h>

namespace Io::Oidis::Microcore::Handlers {
    class DisplayHandler : public CefDisplayHandler {
     private:
        IMPLEMENT_REFCOUNTING(DisplayHandler);
    };
}

#endif  // IO_OIDIS_MICROCORE_HANDLERS_DISPLAYHANDLER_HPP_

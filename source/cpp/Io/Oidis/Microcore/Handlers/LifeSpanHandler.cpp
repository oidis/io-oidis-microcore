/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include <include/wrapper/cef_helpers.h>

#include "../sourceFilesMap.hpp"

namespace Io::Oidis::Microcore::Handlers {
    LifeSpanHandler::LifeSpanHandler(IOsrDelegate *$osrDelegate)
            : osrDelegate{$osrDelegate} {
    }

    void LifeSpanHandler::OnAfterCreated(const CefRefPtr <CefBrowser> $browser) {
        CEF_REQUIRE_UI_THREAD();

        this->osrDelegate->OnAfterCreated($browser);
    }

    void LifeSpanHandler::OnBeforeClose(CefRefPtr<CefBrowser> /*$browser*/) {
        CEF_REQUIRE_UI_THREAD();

        this->osrDelegate->OnBeforeClose();

        CefQuitMessageLoop();
    }

    bool LifeSpanHandler::OnBeforePopup(CefRefPtr<CefBrowser> /*$browser*/,
                                        CefRefPtr<CefFrame> /*$frame*/,
                                        const CefString &$targetUrl,
                                        const CefString &/*$targetFrameName*/,
                                        WindowOpenDisposition /*$targetDisposition*/,
                                        bool /*$userGesture*/,
                                        const CefPopupFeatures &/*$popupFeatures*/,
                                        CefWindowInfo &/*$windowInfo*/,
                                        CefRefPtr<CefClient> &/*$client*/,
                                        CefBrowserSettings &/*$settings*/,
                                        bool */*$noJavascriptAccess*/) {
        CEF_REQUIRE_UI_THREAD();

        return this->osrDelegate->OnBeforePopup($targetUrl);
    }
}

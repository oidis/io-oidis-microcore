/* ********************************************************************************************************* *
 *
 * Copyright (c) 2012 The Chromium Embedded Framework Authors
 * Copyright (c) 2018 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

#include "../OpenGL/OpenGL.hpp"

namespace {
    const GLenum glBGRA = 0x80E1;
    const GLenum glUnsigned8 = 0x8367;
}

namespace Io::Oidis::Microcore::Renderers {
    OsrRenderer::OsrRenderer(RendererConfiguration $configuration)
            : configuration(std::move($configuration)) {
    }

    OsrRenderer::~OsrRenderer() {
        this->Cleanup();
    }

    void OsrRenderer::Initialize() {
        if (this->initialized) {
            return;
        }

        glHint(GL_POLYGON_SMOOTH_HINT, GL_NICEST);

        if (this->isTransparent()) {
            glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
        } else {
            glClearColor(static_cast<float>(CefColorGetR(this->configuration.backgroundColor)) / 255.0f,
                         static_cast<float>(CefColorGetG(this->configuration.backgroundColor)) / 255.0f,
                         static_cast<float>(CefColorGetB(this->configuration.backgroundColor)) / 255.0f,
                         static_cast<float>(CefColorGetA(this->configuration.backgroundColor)));
        }

        glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

        glGenTextures(1, &this->textureId);

        glBindTexture(GL_TEXTURE_2D, this->textureId);

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);

        this->initialized = true;
    }

    void OsrRenderer::Cleanup() {
        if (this->textureId != 0) {
            glDeleteTextures(1, &this->textureId);
        }
    }

    void OsrRenderer::Render() {
        if (this->viewWidth == 0 || this->viewHeight == 0) {
            return;
        }

        struct {
            float tu, tv;
            float x, y, z;
        } static vertices[] = {{0.0f, 1.0f, -1.0f, -1.0f, 0.0f},
                               {1.0f, 1.0f, 1.0f,  -1.0f, 0.0f},
                               {1.0f, 0.0f, 1.0f,  1.0f,  0.0f},
                               {0.0f, 0.0f, -1.0f, 1.0f,  0.0f}};

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();

        glViewport(0, 0, this->viewWidth, this->viewHeight);
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();

        if (this->spinX != 0) {
            glRotatef(-this->spinX, 1.0f, 0.0f, 0.0f);
        }
        if (this->spinY != 0) {
            glRotatef(-this->spinY, 0.0f, 1.0f, 0.0f);
        }

        if (isTransparent()) {
            glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);

            glEnable(GL_BLEND);
        }

        glEnable(GL_TEXTURE_2D);

        glBindTexture(GL_TEXTURE_2D, this->textureId);
        glInterleavedArrays(GL_T2F_V3F, 0, vertices);
        glDrawArrays(GL_QUADS, 0, 4);

        glDisable(GL_TEXTURE_2D);

        if (this->isTransparent()) {
            glDisable(GL_BLEND);
        }

        if (!this->updateRect.IsEmpty()) {
            int left = this->updateRect.x;
            int right = this->updateRect.x + this->updateRect.width;
            int top = this->updateRect.y;
            int bottom = this->updateRect.y + this->updateRect.height;

#if defined(OS_LINUX)
            top += 1;
            right -= 1;
#else
            left += 1;
            bottom -= 1;
#endif

            glPushAttrib(GL_ALL_ATTRIB_BITS);
            glMatrixMode(GL_PROJECTION);
            glPushMatrix();
            glLoadIdentity();
            glOrtho(0, this->viewWidth, this->viewHeight, 0, 0, 1);

            glLineWidth(1);
            glColor3f(1.0f, 0.0f, 0.0f);

            glBegin(GL_LINE_STRIP);
            glVertex2i(left, top);
            glVertex2i(right, top);
            glVertex2i(right, bottom);
            glVertex2i(left, bottom);
            glVertex2i(left, top);
            glEnd();

            glPopMatrix();
            glPopAttrib();
        }
    }

    void OsrRenderer::OnPaint(CefRenderHandler::PaintElementType $type,
                              const CefRenderHandler::RectList &$dirtyRects,
                              const void *$buffer,
                              int $width,
                              int $height) {
        if (!this->initialized) {
            this->Initialize();
        }

        if (this->isTransparent()) {
            glEnable(GL_BLEND);
        }

        glEnable(GL_TEXTURE_2D);

        glBindTexture(GL_TEXTURE_2D, this->textureId);

        if ($type == PET_VIEW) {
            const int oldWidth = this->viewWidth;
            const int oldHeight = this->viewHeight;

            this->viewWidth = $width;
            this->viewHeight = $height;

            glPixelStorei(GL_UNPACK_ROW_LENGTH, this->viewWidth);

            if (oldWidth != this->viewWidth || oldHeight != this->viewHeight ||
                ($dirtyRects.size() == 1 && $dirtyRects[0] == CefRect(0, 0, this->viewWidth, this->viewHeight))) {
                glPixelStorei(GL_UNPACK_SKIP_PIXELS, 0);
                glPixelStorei(GL_UNPACK_SKIP_ROWS, 0);
                glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, this->viewWidth, this->viewHeight, 0,
                             glBGRA, glUnsigned8, $buffer);
            } else {
                CefRenderHandler::RectList::const_iterator i = $dirtyRects.begin();
                for (; i != $dirtyRects.end(); ++i) {
                    const CefRect &rect = *i;
                    glPixelStorei(GL_UNPACK_SKIP_PIXELS, rect.x);
                    glPixelStorei(GL_UNPACK_SKIP_ROWS, rect.y);
                    glTexSubImage2D(GL_TEXTURE_2D, 0, rect.x, rect.y, rect.width,
                                    rect.height, glBGRA, glUnsigned8,
                                    $buffer);
                }
            }
        } else if ($type == PET_POPUP && this->popupRect.width > 0 && this->popupRect.height > 0) {
            int skipPixels = 0;
            int skipRows = 0;
            int x = popupRect.x;
            int y = popupRect.y;
            int width = $width;
            int height = $height;

            if (x < 0) {
                skipPixels = -x;
                x = 0;
            }
            if (y < 0) {
                skipRows = -y;
                y = 0;
            }
            if (x + width > this->viewWidth) {
                width -= x + width - this->viewWidth;
            }
            if (y + height > this->viewHeight) {
                height -= y + height - this->viewHeight;
            }

            glPixelStorei(GL_UNPACK_ROW_LENGTH, $width);
            glPixelStorei(GL_UNPACK_SKIP_PIXELS, skipPixels);
            glPixelStorei(GL_UNPACK_SKIP_ROWS, skipRows);
            glTexSubImage2D(GL_TEXTURE_2D, 0, x, y, width, height, glBGRA,
                            glUnsigned8, $buffer);
        }

        glDisable(GL_TEXTURE_2D);

        if (this->isTransparent()) {
            glDisable(GL_BLEND);
        }
    }

    const CefRect &OsrRenderer::getPopupRect() const {
        return this->popupRect;
    }

    const CefRect &OsrRenderer::getOriginalPopupRect() const {
        return this->originalPopupRect;
    }

    bool OsrRenderer::isTransparent() const {
        return CefColorGetA(this->configuration.backgroundColor) == 0;
    }
}

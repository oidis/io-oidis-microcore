/* ********************************************************************************************************* *
 *
 * Copyright (c) 2012 The Chromium Embedded Framework Authors
 * Copyright (c) 2018 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_MICROCORE_RENDERERS_OSRRENDERER_HPP_
#define IO_OIDIS_MICROCORE_RENDERERS_OSRRENDERER_HPP_

#include "../Configuration/Configuration.hpp"

namespace Io::Oidis::Microcore::Renderers {
    class OsrRenderer {
        using RendererConfiguration = Io::Oidis::Microcore::Configuration::Configuration::Renderer;

     public:
        explicit OsrRenderer(RendererConfiguration $configuration);

        ~OsrRenderer();

        void Initialize();

        void Cleanup();

        void Render();

        void OnPaint(CefRenderHandler::PaintElementType $type,
                     const CefRenderHandler::RectList &$dirtyRects,
                     const void *$buffer,
                     int $width,
                     int $height);

        const CefRect &getPopupRect() const;

        const CefRect &getOriginalPopupRect() const;

     private:
        bool isTransparent() const;

        RendererConfiguration configuration;
        bool initialized = false;
        unsigned int textureId = 0;
        int viewWidth = 0;
        int viewHeight = 0;
        CefRect popupRect = { 0, 0, 0, 0};
        CefRect originalPopupRect = { 0, 0, 0, 0 };
        float spinX = 0;
        float spinY = 0;
        CefRect updateRect = { 0, 0, 0, 0};

        DISALLOW_COPY_AND_ASSIGN(OsrRenderer);
    };
}

#endif  // IO_OIDIS_MICROCORE_RENDERERS_OSRRENDERER_HPP_

/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifdef WIN_PLATFORM

#include <boost/range/adaptors.hpp>

#include "../sourceFilesMap.hpp"

namespace Io::Oidis::Microcore::Commons {
    using Io::Oidis::XCppCommons::Enums::LogLevel;
    using Io::Oidis::XCppCommons::Utils::LogIt;

    namespace fs = boost::filesystem;
    namespace adaptors = boost::adaptors;

    namespace Exceptions = Io::Oidis::Microcore::Exceptions::SharedLibraryManager;

    SharedLibraryManager::SharedLibraryManager(const string &$libraryNamePattern) {
        LogIt::Info("Loading shared library with this pattern {0}", $libraryNamePattern);

        string libraryPath;
        const boost::regex fileNameFilter{$libraryNamePattern};
        boost::smatch what;

        for (auto &entry : boost::make_iterator_range(fs::directory_iterator("."), {})
                          | adaptors::filtered(static_cast<bool (*)(const fs::path &)>(&fs::is_regular_file))
                          | adaptors::filtered(
                [&](const fs::path &path) { return boost::regex_match(path.filename().string(), what, fileNameFilter); })
                ) {
            libraryPath = entry.path().filename().string();

            LogIt::Info("Found library {0}", libraryPath);

            break;
        }

        if (libraryPath.empty()) {
            throw Exceptions::LibraryFileNotExists{$libraryNamePattern};
        }

        this->libraryHandle = LoadLibraryA(libraryPath.c_str());

        if (!this->libraryHandle) {
            throw Exceptions::LibraryLoadFailed{libraryPath};
        }
    }

    SharedLibraryManager::~SharedLibraryManager() {
        if (this->libraryHandle != nullptr) {
            LogIt::Info("Freeing shared library");

            FreeLibrary(this->libraryHandle);
            this->libraryHandle = nullptr;
        }
    }
}

#endif  // WIN_PLATFORM

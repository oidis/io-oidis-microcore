/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014 The Chromium Embedded Framework Authors
 * Copyright (c) 2018 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_MICROCORE_COMMONS_BYTESWRITEHANDLER_OSX_HPP_
#define IO_OIDIS_MICROCORE_COMMONS_BYTESWRITEHANDLER_OSX_HPP_

#ifdef MAC_PLATFORM

#include <include/cef_stream.h>
#include <include/base/cef_lock.h>

namespace Io::Oidis::Microcore::Commons {
    class BytesWriteHandler : public CefWriteHandler {
     public:
        explicit BytesWriteHandler(size_t $grow);

        ~BytesWriteHandler();

        size_t Write(const void *$ptr, size_t $size, size_t $number) override;

        int Seek(int64 $offset, int $whence) override;

        int64 Tell() override;

        int Flush() override;

        bool MayBlock() override;

        void *GetData();

        int64 GetDataSize();

     private:
        size_t growData(size_t $size);

        size_t grow = 0;
        void *data = nullptr;
        int64 dataSize = 0;
        int64 offset = 0;
        base::Lock lock;

        IMPLEMENT_REFCOUNTING(BytesWriteHandler);
        DISALLOW_COPY_AND_ASSIGN(BytesWriteHandler);
    };
}

#endif  // MAC_PLATFORM

#endif  // IO_OIDIS_MICROCORE_COMMONS_BYTESWRITEHANDLER_OSX_HPP_

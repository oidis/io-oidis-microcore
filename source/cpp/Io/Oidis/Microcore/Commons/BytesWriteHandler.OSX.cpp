/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014 The Chromium Embedded Framework Authors
 * Copyright (c) 2018 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifdef MAC_PLATFORM

#include "../sourceFilesMap.hpp"

namespace Io::Oidis::Microcore::Commons {
    BytesWriteHandler::BytesWriteHandler(const size_t $grow)
            : grow($grow),
              dataSize($grow) {
        this->data = malloc(this->dataSize);
    }

    BytesWriteHandler::~BytesWriteHandler() {
        free(this->data);
        this->data = nullptr;
    }

    size_t BytesWriteHandler::Write(const void *$ptr, const size_t $size, const size_t $number) {
        const base::AutoLock lockScope(this->lock);

        size_t rv = 0;

        if (this->offset + static_cast<int64>($size * $number) >= this->dataSize && this->growData($size * $number) == 0) {
            rv = 0;
        } else {
            memcpy(reinterpret_cast<char*>(this->data) + this->offset, $ptr, $size * $number);
            this->offset += $size * $number;
            rv = $number;
        }

        return rv;
    }

    int BytesWriteHandler::Seek(const int64 $offset, const int $whence) {
        const base::AutoLock lockScope(this->lock);

        int rv = -1L;

        switch ($whence) {
            case SEEK_CUR:
                if (this->offset + $offset > this->dataSize || this->offset + $offset < 0) {
                    break;
                }
                this->offset += $offset;
                rv = 0;
                break;
            case SEEK_END: {
                const int64 offsetAbs = std::abs($offset);
                if (offsetAbs > this->dataSize) {
                    break;
                }
                this->offset = this->dataSize - offsetAbs;
                rv = 0;
                break;
            }
            case SEEK_SET:
                if ($offset > this->dataSize || $offset < 0) {
                    break;
                }
                this->offset = $offset;
                rv = 0;
                break;
        }

        return rv;
    }

    int64 BytesWriteHandler::Tell() {
        const base::AutoLock lockScope(this->lock);

        return this->offset;
    }

    int BytesWriteHandler::Flush() {
        return 0;
    }

    bool BytesWriteHandler::MayBlock() {
        return false;
    }

    void *BytesWriteHandler::GetData() {
        return this->data;
    }

    int64 BytesWriteHandler::GetDataSize() {
        return this->offset;
    }

    size_t BytesWriteHandler::growData(const size_t $size) {
        const base::AutoLock lockScope(this->lock);

        size_t rv = 0;
        size_t s = ($size > this->grow ? $size : this->grow);
        void *tmp = realloc(this->data, this->dataSize + s);

        if (tmp) {
            this->data = tmp;
            this->dataSize += s;
            rv = this->dataSize;
        } else {
            rv = 0;
        }

        return rv;
    }
}

#endif  // MAC_PLATFORM

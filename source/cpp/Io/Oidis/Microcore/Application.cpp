/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "sourceFilesMap.hpp"

namespace Io::Oidis::Microcore {
    using Io::Oidis::XCppCommons::Enums::LogLevel;
    using Io::Oidis::XCppCommons::Utils::LogIt;

    using Io::Oidis::Microcore::Enums::ProcessExitCodes;
    using Io::Oidis::Microcore::Runners::Runner;

    int Application::Run(const int $argc, const char **$argv) {
        Runner::ProcessExitCode exitCode = ProcessExitCodes::DEFAULT;

        const auto applicationRunner = std::make_unique<Runner>($argc, $argv);

        exitCode = applicationRunner->Run();

        LogIt::Info("Going to exit with code {0}", exitCode);

        return exitCode;
    }
}

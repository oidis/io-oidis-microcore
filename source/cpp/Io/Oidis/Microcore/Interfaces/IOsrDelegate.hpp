/* ********************************************************************************************************* *
 *
 * Copyright (c) 2013 The Chromium Embedded Framework Authors
 * Copyright (c) 2018 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_MICROCORE_INTERFACES_IOSRDELEGATE_HPP_
#define IO_OIDIS_MICROCORE_INTERFACES_IOSRDELEGATE_HPP_

namespace Io::Oidis::Microcore::Interfaces {
    class IOsrDelegate {
     public:
        virtual void OnPaint(CefRenderHandler::PaintElementType $type,
                             const CefRenderHandler::RectList &$dirtyRects,
                             const void *$buffer,
                             int $width,
                             int $height) = 0;

        virtual bool GetViewRect(CefRect &$rect) = 0;

        virtual void OnAfterCreated(CefRefPtr<CefBrowser> $browser) = 0;

        virtual void OnBeforeClose() = 0;

        virtual bool OnBeforePopup(const CefString &$targetUrl) = 0;

        virtual bool GetScreenInfo(CefScreenInfo &$screenInfo) = 0;

        virtual bool GetScreenPoint(int $viewX, int $viewY, int &$screenX, int &$screenY) = 0;

        virtual void OnCursorChange(CefCursorHandle $cursor) = 0;

        virtual bool StartDragging(const CefRefPtr<CefBrowser> $browser,
                                   const CefRefPtr<CefDragData> $dragData,
                                   CefRenderHandler::DragOperationsMask $allowedOps,
                                   int $x,
                                   int $y) = 0;

        virtual void UpdateDragCursor(CefRenderHandler::DragOperation $operation) = 0;
    };
}

#endif  // IO_OIDIS_MICROCORE_INTERFACES_IOSRDELEGATE_HPP_

/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_MICROCORE_ENUMS_PROCESSEXITCODES_HPP_
#define IO_OIDIS_MICROCORE_ENUMS_PROCESSEXITCODES_HPP_

namespace Io::Oidis::Microcore::Enums {
    enum ProcessExitCodes : int {
        SUCCESS = EXIT_SUCCESS,

        FAILURE = EXIT_FAILURE,
        INCORRECT_ARGUMENTS = 2,
        CEF_INITIALIZATION_FAILED = 3,
        BROWSER_PROCESS_SPAWN = -1,

        DEFAULT = FAILURE
    };
}

#endif  // IO_OIDIS_MICROCORE_ENUMS_PROCESSEXITCODES_HPP_

/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_MICROCORE_ENUMS_PASTEBOARDDATATYPES_OSX_HPP_
#define IO_OIDIS_MICROCORE_ENUMS_PASTEBOARDDATATYPES_OSX_HPP_

#ifdef MAC_PLATFORM

namespace Io::Oidis::Microcore::Enums {
    struct PasteboardDataTypes
            : Io::Oidis::XCppCommons::Primitives::BaseEnum<Io::Oidis::Microcore::Enums::PasteboardDataTypes> {
        static NSString *GetString(const PasteboardDataTypes &$type);

        WUI_ENUM_DECLARE(PasteboardDataTypes);

        static const PasteboardDataTypes DUMMY;
        static const PasteboardDataTypes URL_TITLE;
    };
}

#endif  // MAC_PLATFORM

#endif  // IO_OIDIS_MICROCORE_ENUMS_PASTEBOARDDATATYPES_OSX_HPP_

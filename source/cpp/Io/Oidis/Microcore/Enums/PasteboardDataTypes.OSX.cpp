/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

#ifdef MAC_PLATFORM

using Io::Oidis::XCppCommons::Primitives::BaseEnum;
using Io::Oidis::Microcore::Enums::PasteboardDataTypes;

namespace Io::Oidis::Microcore::Enums {
    NSString *PasteboardDataTypes::GetString(const PasteboardDataTypes &$type) {
        return [NSString stringWithUTF8String:$type.toString().c_str()];
    }
}

WUI_ENUM_IMPLEMENT(PasteboardDataTypes);

WUI_ENUM_CONST_IMPLEMENT(PasteboardDataTypes, DUMMY, "org.CEF.drag-dummy-type");
WUI_ENUM_CONST_IMPLEMENT(PasteboardDataTypes, URL_TITLE, "public.url-name");

#endif  // MAC_PLATFORM

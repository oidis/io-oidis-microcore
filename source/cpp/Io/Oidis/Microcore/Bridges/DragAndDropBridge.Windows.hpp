/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_MICROCORE_BRIDGES_DRAGANDDROPBRIDGE_WINDOWS_HPP_
#define IO_OIDIS_MICROCORE_BRIDGES_DRAGANDDROPBRIDGE_WINDOWS_HPP_

#ifdef WIN_PLATFORM

#include <MSVC/MicrocoreMSVC.hpp>

#include "../Commons/SharedLibraryManager.Windows.hpp"

namespace Io::Oidis::Microcore::Bridges {
    class DragAndDropBridge {
        using OnDrop = Io::Oidis::Microcore::MSVC::Callbacks::OnDrop;
        using OnDragLeave = Io::Oidis::Microcore::MSVC::Callbacks::OnDragLeave;
        using OnDragOver = Io::Oidis::Microcore::MSVC::Callbacks::OnDragOver;
        using OnDragEnter = Io::Oidis::Microcore::MSVC::Callbacks::OnDragEnter;

     public:
        static DragAndDropBridge &GetInstance();

        DragAndDropBridge();

        ~DragAndDropBridge() = default;

        DragAndDropBridge(const DragAndDropBridge &) = delete;

        DragAndDropBridge(DragAndDropBridge &&) = delete;

        DragAndDropBridge &operator=(const DragAndDropBridge &) = delete;

        DragAndDropBridge &operator=(DragAndDropBridge &&) = delete;

        function<const char *()> GetVersion = nullptr;

        function<bool(const char *,
                      OnDrop,
                      OnDragLeave,
                      OnDragOver,
                      OnDragEnter,
                      HWND)> Initialize = nullptr;

        function<bool(HWND)> Destroy = nullptr;

        function<bool()> HasTarget = nullptr;

        function<CefBrowserHost::DragOperationsMask(Io::Oidis::Microcore::MSVC::Structures::DragData,
                                                    CefRenderHandler::DragOperationsMask,
                                                    int,
                                                    int)> StartDragging = nullptr;

     private:
        void loadDragAndDropFunctions();

        Io::Oidis::Microcore::Commons::SharedLibraryManager dragAndDropSharedLibrary;
    };
}

#endif   // WIN_PLATFORM

#endif  // IO_OIDIS_MICROCORE_BRIDGES_DRAGANDDROPBRIDGE_WINDOWS_HPP_

/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifdef WIN_PLATFORM

#include "../sourceFilesMap.hpp"

namespace Io::Oidis::Microcore::Bridges {
    using Io::Oidis::XCppCommons::Enums::LogLevel;
    using Io::Oidis::XCppCommons::Utils::LogIt;

    using DragData = Io::Oidis::Microcore::MSVC::Structures::DragData;

    DragAndDropBridge &DragAndDropBridge::GetInstance() {
        static DragAndDropBridge instance;

        return instance;
    }

    DragAndDropBridge::DragAndDropBridge()
            : dragAndDropSharedLibrary("com-wui-framework-microcore-msvc-.*\\.dll") {
        this->loadDragAndDropFunctions();
    }

    void DragAndDropBridge::loadDragAndDropFunctions() {
        LogIt::Info("Loading functions from the shared library");

        this->GetVersion = dragAndDropSharedLibrary.GetProcedure<const char *()>("GetVersion");

        this->Initialize = dragAndDropSharedLibrary.GetProcedure<bool(const char *,
                                                                      OnDrop,
                                                                      OnDragLeave,
                                                                      OnDragOver,
                                                                      OnDragEnter,
                                                                      HWND)>("Initialize");

        this->Destroy = dragAndDropSharedLibrary.GetProcedure<bool(HWND)>("Destroy");

        this->HasTarget = dragAndDropSharedLibrary.GetProcedure<bool()>("HasTarget");

        this->StartDragging = dragAndDropSharedLibrary.GetProcedure<CefBrowserHost::DragOperationsMask(DragData,
                                                                                                       CefRenderHandler::DragOperationsMask,
                                                                                                       int,
                                                                                                       int)>("StartDragging");
    }
}

#endif   // WIN_PLATFORM

/* ********************************************************************************************************* *
 *
 * Copyright (c) 2011 The Chromium Authors
 * Copyright (c) 2015 The Chromium Embedded Framework Authors
 * Copyright (c) 2018 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_MICROCORE_HARDWARE_KEYBOARD_LINUX_HPP_
#define IO_OIDIS_MICROCORE_HARDWARE_KEYBOARD_LINUX_HPP_

#ifdef LINUX_PLATFORM

#include <gdk/gdk.h>

#include "../Enums/KeyboardCodes.Unix.hpp"

namespace Io::Oidis::Microcore::Hardware::Keyboard {
    Enums::KeyboardCodes GdkEventToWindowsKeyCode(const GdkEventKey *$event);

    Enums::KeyboardCodes GetWindowsKeyCodeWithoutLocation(Enums::KeyboardCodes $keyCode);

    int GetControlCharacter(Enums::KeyboardCodes $keyCode, bool $shift);
}

#endif  // LINUX_PLATFORM

#endif  // IO_OIDIS_MICROCORE_HARDWARE_KEYBOARD_LINUX_HPP_

/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014 The Chromium Embedded Framework Authors
 * Copyright (c) 2018 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_MICROCORE_HARDWARE_MOUSE_WINDOWS_HPP_
#define IO_OIDIS_MICROCORE_HARDWARE_MOUSE_WINDOWS_HPP_

#ifdef WIN_PLATFORM

namespace Io::Oidis::Microcore::Hardware::Mouse {
    CefMouseEvent GetCefMouseEvent(POINT $point, WPARAM $wparam);

    struct MouseData {
        bool mouseTracking = false;
        int lastClickCount = 0;
        int lastClickX = 0;
        int lastClickY = 0;
        double lastClickTime = 0.0;
        bool lastMouseDownOnView = false;
        CefBrowserHost::MouseButtonType lastClickButton = CefBrowserHost::MouseButtonType::MBT_LEFT;
    };
}

#endif  // WIN_PLATFORM

#endif  // IO_OIDIS_MICROCORE_HARDWARE_MOUSE_WINDOWS_HPP_

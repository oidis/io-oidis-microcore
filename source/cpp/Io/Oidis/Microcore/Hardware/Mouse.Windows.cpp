/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014 The Chromium Embedded Framework Authors
 * Copyright (c) 2018 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifdef WIN_PLATFORM

#include "../sourceFilesMap.hpp"

namespace Io::Oidis::Microcore::Hardware::Mouse {
    int GetCefMouseModifiers(const WPARAM $wparam) {
        int modifiers = 0;

        if (($wparam & MK_CONTROL) != 0) {
            modifiers |= EVENTFLAG_CONTROL_DOWN;
        }
        if (($wparam & MK_SHIFT) != 0) {
            modifiers |= EVENTFLAG_SHIFT_DOWN;
        }
        if (Keyboard::IsKeyDown(VK_MENU)) {
            modifiers |= EVENTFLAG_ALT_DOWN;
        }
        if (($wparam & MK_LBUTTON) != 0) {
            modifiers |= EVENTFLAG_LEFT_MOUSE_BUTTON;
        }
        if (($wparam & MK_MBUTTON) != 0) {
            modifiers |= EVENTFLAG_MIDDLE_MOUSE_BUTTON;
        }
        if (($wparam & MK_RBUTTON) != 0) {
            modifiers |= EVENTFLAG_RIGHT_MOUSE_BUTTON;
        }

        if ((GetKeyState(VK_NUMLOCK) & 1) != 0) {
            modifiers |= EVENTFLAG_NUM_LOCK_ON;
        }
        if ((GetKeyState(VK_CAPITAL) & 1) != 0) {
            modifiers |= EVENTFLAG_CAPS_LOCK_ON;
        }

        return modifiers;
    }

    CefMouseEvent GetCefMouseEvent(const POINT $point, const WPARAM $wparam) {
        return GetCefMouseEvent($point.x, $point.y, GetCefMouseModifiers($wparam));
    }
}

#endif  // WIN_PLATFORM

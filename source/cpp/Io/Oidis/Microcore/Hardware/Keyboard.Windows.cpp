/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014 The Chromium Embedded Framework Authors
 * Copyright (c) 2018 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifdef WIN_PLATFORM

#include "../sourceFilesMap.hpp"

namespace Io::Oidis::Microcore::Hardware::Keyboard {
    bool IsKeyDown(const WPARAM $wparam) {
        return (GetKeyState($wparam) & 0x8000) != 0;
    }

    int GetCefKeyboardModifiers(const WPARAM $wparam, const LPARAM $lparam) {
        int modifiers = 0;

        if (IsKeyDown(VK_SHIFT)) {
            modifiers |= EVENTFLAG_SHIFT_DOWN;
        }
        if (IsKeyDown(VK_CONTROL)) {
            modifiers |= EVENTFLAG_CONTROL_DOWN;
        }
        if (IsKeyDown(VK_MENU)) {
            modifiers |= EVENTFLAG_ALT_DOWN;
        }

        if ((GetKeyState(VK_NUMLOCK) & 1) != 0) {
            modifiers |= EVENTFLAG_NUM_LOCK_ON;
        }
        if ((GetKeyState(VK_CAPITAL) & 1) != 0) {
            modifiers |= EVENTFLAG_CAPS_LOCK_ON;
        }

        switch ($wparam) {
            case VK_RETURN:
                if ((($lparam >> 16) & KF_EXTENDED) != 0) {
                    modifiers |= EVENTFLAG_IS_KEY_PAD;
                }
                break;
            case VK_INSERT:
            case VK_DELETE:
            case VK_HOME:
            case VK_END:
            case VK_PRIOR:
            case VK_NEXT:
            case VK_UP:
            case VK_DOWN:
            case VK_LEFT:
            case VK_RIGHT:
                if ((($lparam >> 16) & KF_EXTENDED) == 0) {
                    modifiers |= EVENTFLAG_IS_KEY_PAD;
                }
                break;
            case VK_NUMLOCK:
            case VK_NUMPAD0:
            case VK_NUMPAD1:
            case VK_NUMPAD2:
            case VK_NUMPAD3:
            case VK_NUMPAD4:
            case VK_NUMPAD5:
            case VK_NUMPAD6:
            case VK_NUMPAD7:
            case VK_NUMPAD8:
            case VK_NUMPAD9:
            case VK_DIVIDE:
            case VK_MULTIPLY:
            case VK_SUBTRACT:
            case VK_ADD:
            case VK_DECIMAL:
            case VK_CLEAR:
                modifiers |= EVENTFLAG_IS_KEY_PAD;
                break;
            case VK_SHIFT:
                if (IsKeyDown(VK_LSHIFT)) {
                    modifiers |= EVENTFLAG_IS_LEFT;
                } else if (IsKeyDown(VK_RSHIFT)) {
                    modifiers |= EVENTFLAG_IS_RIGHT;
                }
                break;
            case VK_CONTROL:
                if (IsKeyDown(VK_LCONTROL)) {
                    modifiers |= EVENTFLAG_IS_LEFT;
                } else if (IsKeyDown(VK_RCONTROL)) {
                    modifiers |= EVENTFLAG_IS_RIGHT;
                }
                break;
            case VK_MENU:
                if (IsKeyDown(VK_LMENU)) {
                    modifiers |= EVENTFLAG_IS_LEFT;
                } else if (IsKeyDown(VK_RMENU)) {
                    modifiers |= EVENTFLAG_IS_RIGHT;
                }
                break;
            case VK_LWIN:
                modifiers |= EVENTFLAG_IS_LEFT;
                break;
            case VK_RWIN:
                modifiers |= EVENTFLAG_IS_RIGHT;
                break;
            default:
                break;
        }
        return modifiers;
    }
}

#endif  // WIN_PLATFORM

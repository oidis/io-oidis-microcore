/* ********************************************************************************************************* *
 *
 * Copyright (c) 2011 The Chromium Authors
 * Copyright (c) 2015 The Chromium Embedded Framework Authors
 * Copyright (c) 2018 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifdef LINUX_PLATFORM

#include <X11/XF86keysym.h>
#define XK_3270
#include <X11/keysym.h>
#undef XK_3270
#include <gdk/gdkkeysyms.h>

#include "../sourceFilesMap.hpp"

namespace Io::Oidis::Microcore::Hardware::Keyboard {
    using Io::Oidis::Microcore::Enums::KeyboardCodes;

    static KeyboardCodes KeyboardCodeFromXKeySymbol(const unsigned int $keySymbol) {
        switch ($keySymbol) {
            case XK_BackSpace:
                return KeyboardCodes::VKEY_BACK;
            case XK_Delete:
            case XK_KP_Delete:
                return KeyboardCodes::VKEY_DELETE;
            case XK_Tab:
            case XK_KP_Tab:
            case XK_ISO_Left_Tab:
            case XK_3270_BackTab:
                return KeyboardCodes::VKEY_TAB;
            case XK_Linefeed:
            case XK_Return:
            case XK_KP_Enter:
            case XK_ISO_Enter:
                return KeyboardCodes::VKEY_RETURN;
            case XK_Clear:
            case XK_KP_Begin:
                return KeyboardCodes::VKEY_CLEAR;
            case XK_KP_Space:
            case XK_space:
                return KeyboardCodes::VKEY_SPACE;
            case XK_Home:
            case XK_KP_Home:
                return KeyboardCodes::VKEY_HOME;
            case XK_End:
            case XK_KP_End:
                return KeyboardCodes::VKEY_END;
            case XK_Page_Up:
            case XK_KP_Page_Up:
                return KeyboardCodes::VKEY_PRIOR;
            case XK_Page_Down:
            case XK_KP_Page_Down:
                return KeyboardCodes::VKEY_NEXT;
            case XK_Left:
            case XK_KP_Left:
                return KeyboardCodes::VKEY_LEFT;
            case XK_Right:
            case XK_KP_Right:
                return KeyboardCodes::VKEY_RIGHT;
            case XK_Down:
            case XK_KP_Down:
                return KeyboardCodes::VKEY_DOWN;
            case XK_Up:
            case XK_KP_Up:
                return KeyboardCodes::VKEY_UP;
            case XK_Escape:
                return KeyboardCodes::VKEY_ESCAPE;
            case XK_Kana_Lock:
            case XK_Kana_Shift:
                return KeyboardCodes::VKEY_KANA;
            case XK_Hangul:
                return KeyboardCodes::VKEY_HANGUL;
            case XK_Hangul_Hanja:
                return KeyboardCodes::VKEY_HANJA;
            case XK_Kanji:
                return KeyboardCodes::VKEY_KANJI;
            case XK_Henkan:
                return KeyboardCodes::VKEY_CONVERT;
            case XK_Muhenkan:
                return KeyboardCodes::VKEY_NONCONVERT;
            case XK_Zenkaku_Hankaku:
                return KeyboardCodes::VKEY_DBE_DBCSCHAR;
            case XK_A:
            case XK_a:
                return KeyboardCodes::VKEY_A;
            case XK_B:
            case XK_b:
                return KeyboardCodes::VKEY_B;
            case XK_C:
            case XK_c:
                return KeyboardCodes::VKEY_C;
            case XK_D:
            case XK_d:
                return KeyboardCodes::VKEY_D;
            case XK_E:
            case XK_e:
                return KeyboardCodes::VKEY_E;
            case XK_F:
            case XK_f:
                return KeyboardCodes::VKEY_F;
            case XK_G:
            case XK_g:
                return KeyboardCodes::VKEY_G;
            case XK_H:
            case XK_h:
                return KeyboardCodes::VKEY_H;
            case XK_I:
            case XK_i:
                return KeyboardCodes::VKEY_I;
            case XK_J:
            case XK_j:
                return KeyboardCodes::VKEY_J;
            case XK_K:
            case XK_k:
                return KeyboardCodes::VKEY_K;
            case XK_L:
            case XK_l:
                return KeyboardCodes::VKEY_L;
            case XK_M:
            case XK_m:
                return KeyboardCodes::VKEY_M;
            case XK_N:
            case XK_n:
                return KeyboardCodes::VKEY_N;
            case XK_O:
            case XK_o:
                return KeyboardCodes::VKEY_O;
            case XK_P:
            case XK_p:
                return KeyboardCodes::VKEY_P;
            case XK_Q:
            case XK_q:
                return KeyboardCodes::VKEY_Q;
            case XK_R:
            case XK_r:
                return KeyboardCodes::VKEY_R;
            case XK_S:
            case XK_s:
                return KeyboardCodes::VKEY_S;
            case XK_T:
            case XK_t:
                return KeyboardCodes::VKEY_T;
            case XK_U:
            case XK_u:
                return KeyboardCodes::VKEY_U;
            case XK_V:
            case XK_v:
                return KeyboardCodes::VKEY_V;
            case XK_W:
            case XK_w:
                return KeyboardCodes::VKEY_W;
            case XK_X:
            case XK_x:
                return KeyboardCodes::VKEY_X;
            case XK_Y:
            case XK_y:
                return KeyboardCodes::VKEY_Y;
            case XK_Z:
            case XK_z:
                return KeyboardCodes::VKEY_Z;

            case XK_0:
            case XK_1:
            case XK_2:
            case XK_3:
            case XK_4:
            case XK_5:
            case XK_6:
            case XK_7:
            case XK_8:
            case XK_9:
                return static_cast<KeyboardCodes>(KeyboardCodes::VKEY_0 + ($keySymbol - XK_0));

            case XK_parenright:
                return KeyboardCodes::VKEY_0;
            case XK_exclam:
                return KeyboardCodes::VKEY_1;
            case XK_at:
                return KeyboardCodes::VKEY_2;
            case XK_numbersign:
                return KeyboardCodes::VKEY_3;
            case XK_dollar:
                return KeyboardCodes::VKEY_4;
            case XK_percent:
                return KeyboardCodes::VKEY_5;
            case XK_asciicircum:
                return KeyboardCodes::VKEY_6;
            case XK_ampersand:
                return KeyboardCodes::VKEY_7;
            case XK_asterisk:
                return KeyboardCodes::VKEY_8;
            case XK_parenleft:
                return KeyboardCodes::VKEY_9;

            case XK_KP_0:
            case XK_KP_1:
            case XK_KP_2:
            case XK_KP_3:
            case XK_KP_4:
            case XK_KP_5:
            case XK_KP_6:
            case XK_KP_7:
            case XK_KP_8:
            case XK_KP_9:
                return static_cast<KeyboardCodes>(KeyboardCodes::VKEY_NUMPAD0 + ($keySymbol - XK_KP_0));

            case XK_multiply:
            case XK_KP_Multiply:
                return KeyboardCodes::VKEY_MULTIPLY;
            case XK_KP_Add:
                return KeyboardCodes::VKEY_ADD;
            case XK_KP_Separator:
                return KeyboardCodes::VKEY_SEPARATOR;
            case XK_KP_Subtract:
                return KeyboardCodes::VKEY_SUBTRACT;
            case XK_KP_Decimal:
                return KeyboardCodes::VKEY_DECIMAL;
            case XK_KP_Divide:
                return KeyboardCodes::VKEY_DIVIDE;
            case XK_KP_Equal:
            case XK_equal:
            case XK_plus:
                return KeyboardCodes::VKEY_OEM_PLUS;
            case XK_comma:
            case XK_less:
                return KeyboardCodes::VKEY_OEM_COMMA;
            case XK_minus:
            case XK_underscore:
                return KeyboardCodes::VKEY_OEM_MINUS;
            case XK_greater:
            case XK_period:
                return KeyboardCodes::VKEY_OEM_PERIOD;
            case XK_colon:
            case XK_semicolon:
                return KeyboardCodes::VKEY_OEM_1;
            case XK_question:
            case XK_slash:
                return KeyboardCodes::VKEY_OEM_2;
            case XK_asciitilde:
            case XK_quoteleft:
                return KeyboardCodes::VKEY_OEM_3;
            case XK_bracketleft:
            case XK_braceleft:
                return KeyboardCodes::VKEY_OEM_4;
            case XK_backslash:
            case XK_bar:
                return KeyboardCodes::VKEY_OEM_5;
            case XK_bracketright:
            case XK_braceright:
                return KeyboardCodes::VKEY_OEM_6;
            case XK_quoteright:
            case XK_quotedbl:
                return KeyboardCodes::VKEY_OEM_7;
            case XK_ISO_Level5_Shift:
                return KeyboardCodes::VKEY_OEM_8;
            case XK_Shift_L:
            case XK_Shift_R:
                return KeyboardCodes::VKEY_SHIFT;
            case XK_Control_L:
            case XK_Control_R:
                return KeyboardCodes::VKEY_CONTROL;
            case XK_Meta_L:
            case XK_Meta_R:
            case XK_Alt_L:
            case XK_Alt_R:
                return KeyboardCodes::VKEY_MENU;
            case XK_ISO_Level3_Shift:
                return KeyboardCodes::VKEY_ALTGR;
            case XK_Multi_key:
                return KeyboardCodes::VKEY_COMPOSE;
            case XK_Pause:
                return KeyboardCodes::VKEY_PAUSE;
            case XK_Caps_Lock:
                return KeyboardCodes::VKEY_CAPITAL;
            case XK_Num_Lock:
                return KeyboardCodes::VKEY_NUMLOCK;
            case XK_Scroll_Lock:
                return KeyboardCodes::VKEY_SCROLL;
            case XK_Select:
                return KeyboardCodes::VKEY_SELECT;
            case XK_Print:
                return KeyboardCodes::VKEY_PRINT;
            case XK_Execute:
                return KeyboardCodes::VKEY_EXECUTE;
            case XK_Insert:
            case XK_KP_Insert:
                return KeyboardCodes::VKEY_INSERT;
            case XK_Help:
                return KeyboardCodes::VKEY_HELP;
            case XK_Super_L:
                return KeyboardCodes::VKEY_LWIN;
            case XK_Super_R:
                return KeyboardCodes::VKEY_RWIN;
            case XK_Menu:
                return KeyboardCodes::VKEY_APPS;
            case XK_F1:
            case XK_F2:
            case XK_F3:
            case XK_F4:
            case XK_F5:
            case XK_F6:
            case XK_F7:
            case XK_F8:
            case XK_F9:
            case XK_F10:
            case XK_F11:
            case XK_F12:
            case XK_F13:
            case XK_F14:
            case XK_F15:
            case XK_F16:
            case XK_F17:
            case XK_F18:
            case XK_F19:
            case XK_F20:
            case XK_F21:
            case XK_F22:
            case XK_F23:
            case XK_F24:
                return static_cast<KeyboardCodes>(KeyboardCodes::VKEY_F1 + ($keySymbol - XK_F1));
            case XK_KP_F1:
            case XK_KP_F2:
            case XK_KP_F3:
            case XK_KP_F4:
                return static_cast<KeyboardCodes>(KeyboardCodes::VKEY_F1 + ($keySymbol - XK_KP_F1));

            case XK_guillemotleft:
            case XK_guillemotright:
            case XK_degree:
            case XK_ugrave:
            case XK_Ugrave:
            case XK_brokenbar:
                return KeyboardCodes::VKEY_OEM_102;
            case XF86XK_Tools:
                return KeyboardCodes::VKEY_F13;
            case XF86XK_Launch5:
                return KeyboardCodes::VKEY_F14;
            case XF86XK_Launch6:
                return KeyboardCodes::VKEY_F15;
            case XF86XK_Launch7:
                return KeyboardCodes::VKEY_F16;
            case XF86XK_Launch8:
                return KeyboardCodes::VKEY_F17;
            case XF86XK_Launch9:
                return KeyboardCodes::VKEY_F18;
            case XF86XK_Refresh:
            case XF86XK_History:
            case XF86XK_OpenURL:
            case XF86XK_AddFavorite:
            case XF86XK_Go:
            case XF86XK_ZoomIn:
            case XF86XK_ZoomOut:
                return KeyboardCodes::VKEY_UNKNOWN;
            case XF86XK_Back:
                return KeyboardCodes::VKEY_BROWSER_BACK;
            case XF86XK_Forward:
                return KeyboardCodes::VKEY_BROWSER_FORWARD;
            case XF86XK_Reload:
                return KeyboardCodes::VKEY_BROWSER_REFRESH;
            case XF86XK_Stop:
                return KeyboardCodes::VKEY_BROWSER_STOP;
            case XF86XK_Search:
                return KeyboardCodes::VKEY_BROWSER_SEARCH;
            case XF86XK_Favorites:
                return KeyboardCodes::VKEY_BROWSER_FAVORITES;
            case XF86XK_HomePage:
                return KeyboardCodes::VKEY_BROWSER_HOME;
            case XF86XK_AudioMute:
                return KeyboardCodes::VKEY_VOLUME_MUTE;
            case XF86XK_AudioLowerVolume:
                return KeyboardCodes::VKEY_VOLUME_DOWN;
            case XF86XK_AudioRaiseVolume:
                return KeyboardCodes::VKEY_VOLUME_UP;
            case XF86XK_AudioNext:
                return KeyboardCodes::VKEY_MEDIA_NEXT_TRACK;
            case XF86XK_AudioPrev:
                return KeyboardCodes::VKEY_MEDIA_PREV_TRACK;
            case XF86XK_AudioStop:
                return KeyboardCodes::VKEY_MEDIA_STOP;
            case XF86XK_AudioPlay:
                return KeyboardCodes::VKEY_MEDIA_PLAY_PAUSE;
            case XF86XK_Mail:
                return KeyboardCodes::VKEY_MEDIA_LAUNCH_MAIL;
            case XF86XK_LaunchA:
                return KeyboardCodes::VKEY_MEDIA_LAUNCH_APP1;
            case XF86XK_LaunchB:
            case XF86XK_Calculator:
                return KeyboardCodes::VKEY_MEDIA_LAUNCH_APP2;
            case XF86XK_WLAN:
                return KeyboardCodes::VKEY_WLAN;
            case XF86XK_PowerOff:
                return KeyboardCodes::VKEY_POWER;
            case XF86XK_MonBrightnessDown:
                return KeyboardCodes::VKEY_BRIGHTNESS_DOWN;
            case XF86XK_MonBrightnessUp:
                return KeyboardCodes::VKEY_BRIGHTNESS_UP;
            case XF86XK_KbdBrightnessDown:
                return KeyboardCodes::VKEY_KBD_BRIGHTNESS_DOWN;
            case XF86XK_KbdBrightnessUp:
                return KeyboardCodes::VKEY_KBD_BRIGHTNESS_UP;

            default:
                return KeyboardCodes::VKEY_UNKNOWN;
        }
    }

    KeyboardCodes GdkEventToWindowsKeyCode(const GdkEventKey *$event) {
        static const unsigned int kHardwareCodeToGDKKeyval[] = {
                0,                 // 0x00:
                0,                 // 0x01:
                0,                 // 0x02:
                0,                 // 0x03:
                0,                 // 0x04:
                0,                 // 0x05:
                0,                 // 0x06:
                0,                 // 0x07:
                0,                 // 0x08:
                0,                 // 0x09: GDK_Escape
                GDK_1,             // 0x0A: GDK_1
                GDK_2,             // 0x0B: GDK_2
                GDK_3,             // 0x0C: GDK_3
                GDK_4,             // 0x0D: GDK_4
                GDK_5,             // 0x0E: GDK_5
                GDK_6,             // 0x0F: GDK_6
                GDK_7,             // 0x10: GDK_7
                GDK_8,             // 0x11: GDK_8
                GDK_9,             // 0x12: GDK_9
                GDK_0,             // 0x13: GDK_0
                GDK_minus,         // 0x14: GDK_minus
                GDK_equal,         // 0x15: GDK_equal
                0,                 // 0x16: GDK_BackSpace
                0,                 // 0x17: GDK_Tab
                GDK_q,             // 0x18: GDK_q
                GDK_w,             // 0x19: GDK_w
                GDK_e,             // 0x1A: GDK_e
                GDK_r,             // 0x1B: GDK_r
                GDK_t,             // 0x1C: GDK_t
                GDK_y,             // 0x1D: GDK_y
                GDK_u,             // 0x1E: GDK_u
                GDK_i,             // 0x1F: GDK_i
                GDK_o,             // 0x20: GDK_o
                GDK_p,             // 0x21: GDK_p
                GDK_bracketleft,   // 0x22: GDK_bracketleft
                GDK_bracketright,  // 0x23: GDK_bracketright
                0,                 // 0x24: GDK_Return
                0,                 // 0x25: GDK_Control_L
                GDK_a,             // 0x26: GDK_a
                GDK_s,             // 0x27: GDK_s
                GDK_d,             // 0x28: GDK_d
                GDK_f,             // 0x29: GDK_f
                GDK_g,             // 0x2A: GDK_g
                GDK_h,             // 0x2B: GDK_h
                GDK_j,             // 0x2C: GDK_j
                GDK_k,             // 0x2D: GDK_k
                GDK_l,             // 0x2E: GDK_l
                GDK_semicolon,     // 0x2F: GDK_semicolon
                GDK_apostrophe,    // 0x30: GDK_apostrophe
                GDK_grave,         // 0x31: GDK_grave
                0,                 // 0x32: GDK_Shift_L
                GDK_backslash,     // 0x33: GDK_backslash
                GDK_z,             // 0x34: GDK_z
                GDK_x,             // 0x35: GDK_x
                GDK_c,             // 0x36: GDK_c
                GDK_v,             // 0x37: GDK_v
                GDK_b,             // 0x38: GDK_b
                GDK_n,             // 0x39: GDK_n
                GDK_m,             // 0x3A: GDK_m
                GDK_comma,         // 0x3B: GDK_comma
                GDK_period,        // 0x3C: GDK_period
                GDK_slash,         // 0x3D: GDK_slash
                0,                 // 0x3E: GDK_Shift_R
                0,                 // 0x3F:
                0,                 // 0x40:
                0,                 // 0x41:
                0,                 // 0x42:
                0,                 // 0x43:
                0,                 // 0x44:
                0,                 // 0x45:
                0,                 // 0x46:
                0,                 // 0x47:
                0,                 // 0x48:
                0,                 // 0x49:
                0,                 // 0x4A:
                0,                 // 0x4B:
                0,                 // 0x4C:
                0,                 // 0x4D:
                0,                 // 0x4E:
                0,                 // 0x4F:
                0,                 // 0x50:
                0,                 // 0x51:
                0,                 // 0x52:
                0,                 // 0x53:
                0,                 // 0x54:
                0,                 // 0x55:
                0,                 // 0x56:
                0,                 // 0x57:
                0,                 // 0x58:
                0,                 // 0x59:
                0,                 // 0x5A:
                0,                 // 0x5B:
                0,                 // 0x5C:
                0,                 // 0x5D:
                0,                 // 0x5E:
                0,                 // 0x5F:
                0,                 // 0x60:
                0,                 // 0x61:
                0,                 // 0x62:
                0,                 // 0x63:
                0,                 // 0x64:
                0,                 // 0x65:
                0,                 // 0x66:
                0,                 // 0x67:
                0,                 // 0x68:
                0,                 // 0x69:
                0,                 // 0x6A:
                0,                 // 0x6B:
                0,                 // 0x6C:
                0,                 // 0x6D:
                0,                 // 0x6E:
                0,                 // 0x6F:
                0,                 // 0x70:
                0,                 // 0x71:
                0,                 // 0x72:
                GDK_Super_L,       // 0x73: GDK_Super_L
                GDK_Super_R,       // 0x74: GDK_Super_R
        };

        const KeyboardCodes windowsKeyCode = KeyboardCodeFromXKeySymbol($event->keyval);
        if (windowsKeyCode != 0) {
            return windowsKeyCode;
        }

        if ($event->hardware_keycode < arraysize(kHardwareCodeToGDKKeyval)) {
            const int keyValue = kHardwareCodeToGDKKeyval[$event->hardware_keycode];
            if (keyValue != 0) {
                return KeyboardCodeFromXKeySymbol(keyValue);
            }
        }

        return KeyboardCodeFromXKeySymbol($event->keyval);
    }

    KeyboardCodes GetWindowsKeyCodeWithoutLocation(const KeyboardCodes $keyCode) {
        switch ($keyCode) {
            case KeyboardCodes::VKEY_LCONTROL:
            case KeyboardCodes::VKEY_RCONTROL:
                return KeyboardCodes::VKEY_CONTROL;
            case KeyboardCodes::VKEY_LSHIFT:
            case KeyboardCodes::VKEY_RSHIFT:
                return KeyboardCodes::VKEY_SHIFT;
            case KeyboardCodes::VKEY_LMENU:
            case KeyboardCodes::VKEY_RMENU:
                return KeyboardCodes::VKEY_MENU;
            default:
                return $keyCode;
        }
    }

    int GetControlCharacter(const KeyboardCodes $keyCode, const bool $shift) {
        if ($keyCode >= KeyboardCodes::VKEY_A && $keyCode <= KeyboardCodes::VKEY_Z) {
            return $keyCode - KeyboardCodes::VKEY_A + 1;
        }
        if ($shift) {
            switch ($keyCode) {
                case KeyboardCodes::VKEY_2:
                    return 0;
                case KeyboardCodes::VKEY_6:
                    return 0x1E;
                case KeyboardCodes::VKEY_OEM_MINUS:
                    return 0x1F;
                default:
                    return 0;
            }
        } else {
            switch ($keyCode) {
                case KeyboardCodes::VKEY_OEM_4:
                    return 0x1B;
                case KeyboardCodes::VKEY_OEM_5:
                    return 0x1C;
                case KeyboardCodes::VKEY_OEM_6:
                    return 0x1D;
                case KeyboardCodes::VKEY_RETURN:
                    return 0x0A;
                default:
                    return 0;
            }
        }
    }
}

#endif  // LINUX_PLATFORM

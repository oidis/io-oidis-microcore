/* ********************************************************************************************************* *
 *
 * Copyright (c) 2016 The Chromium Embedded Framework Authors
 * Copyright (c) 2018 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_MICROCORE_HARDWARE_KEYBOARD_OSX_HPP_
#define IO_OIDIS_MICROCORE_HARDWARE_KEYBOARD_OSX_HPP_

#ifdef MAC_PLATFORM

#import <AppKit/NSEvent.h>
#import <AppKit/NSTextInputClient.h>
#import <AppKit/NSTextInputContext.h>

@interface ComWuiFrameworkMicrocoreHardware_Keyboard : NSObject<NSTextInputClient> {
 @private // NOLINT
    Io::Oidis::Microcore::Windows::Window *window;
    BOOL hasMarkedText;
    BOOL hadMarkedText;
    BOOL handlingKeypress;
    BOOL unmarkTextCalled;
    NSRange markedRange;
    NSRange selectedRange;
    std::string textToBeInserted;
    CefString markedText;
    std::vector<CefCompositionUnderline> underlines;
    CefRange setMarkedTextReplacementRange;
    NSTextInputContext *textInputContext;
    CefRange compositionRange;
    std::vector<CefRect> compositionBounds;
}

+ (CefKeyEvent)CreateKeyEvent:(NSEvent *)$event; // NOLINT

+ (BOOL)IsCommand:(NSEvent *)$event; // NOLINT

+ (BOOL)IsCommandCopy:(NSEvent *)$event; // NOLINT

+ (BOOL)IsCommandCut:(NSEvent *)$event; // NOLINT

+ (BOOL)IsCommandPaste:(NSEvent *)$event; // NOLINT

+ (BOOL)IsCommandQuit:(NSEvent *)$event; // NOLINT

+ (BOOL)IsNumericEvent:(NSEvent *)$event; // NOLINT

+ (BOOL)IsKeyUpEvent:(NSEvent *)event; // NOLINT

- (id)Initialize:(Io::Oidis::Microcore::Windows::Window *)$window;

- (NSTextInputContext *)getTextInputContext; // NOLINT

- (void)HandleKeyEventBeforeTextInputClient:(NSEvent *)$keyEvent; // NOLINT

- (void)HandleKeyEventAfterTextInputClient:(CefKeyEvent)$keyEvent; // NOLINT

@end

#endif  // MAC_PLATFORM

#endif  // IO_OIDIS_MICROCORE_HARDWARE_KEYBOARD_OSX_HPP_

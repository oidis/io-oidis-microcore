/* ********************************************************************************************************* *
 *
 * Copyright (c) 2016 The Chromium Embedded Framework Authors
 * Copyright (c) 2018 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifdef MAC_PLATFORM

#include "../sourceFilesMap.hpp"

extern "C" {
    // AppKit exports this symbol but does not make them available in API or SPI headers
    extern NSString *NSTextInputReplacementRangeAttributeName;
}

namespace Exceptions = Io::Oidis::Microcore::Exceptions::Keyboard;

@implementation ComWuiFrameworkMicrocoreHardware_Keyboard

+ (CefKeyEvent)CreateKeyEvent:(NSEvent *)$event { // NOLINT
    CefKeyEvent keyEvent;

    if ($event.type == NSKeyDown || $event.type == NSKeyUp) {
        NSString *characters = $event.characters;

        if (characters.length > 0) {
            keyEvent.character = [characters characterAtIndex:0];
        }

        characters = [$event charactersIgnoringModifiers];
        if (characters.length > 0) {
            keyEvent.unmodified_character = [characters characterAtIndex:0];
        }
  }

  if ($event.type == NSFlagsChanged) {
      keyEvent.character = 0;
      keyEvent.unmodified_character = 0;
  }

  keyEvent.native_key_code = $event.keyCode;

  return keyEvent;
}

+ (BOOL)IsCommand:(NSEvent *)$event { // NOLINT
    if ($event.type != NSFlagsChanged) {
        if (($event.modifierFlags & NSDeviceIndependentModifierFlagsMask) == NSCommandKeyMask) {
            return YES;
        }
    }

    return NO;
}

+ (BOOL)IsCommandCopy:(NSEvent *)$event { // NOLINT
    return [ComWuiFrameworkMicrocoreHardware_Keyboard isCommandWithKeys:$event keys:@"c"];
}

+ (BOOL)IsCommandCut:(NSEvent *)$event { // NOLINT
    return [ComWuiFrameworkMicrocoreHardware_Keyboard isCommandWithKeys:$event keys:@"x"];
}

+ (BOOL)IsCommandPaste:(NSEvent *)$event { // NOLINT
    return [ComWuiFrameworkMicrocoreHardware_Keyboard isCommandWithKeys:$event keys:@"v"];
}

+ (BOOL)IsCommandQuit:(NSEvent *)$event { // NOLINT
    return [ComWuiFrameworkMicrocoreHardware_Keyboard isCommandWithKeys:$event keys:@"q"];
}

+ (BOOL)isCommandWithKeys:(NSEvent *)$event keys:(NSString *)$keys { // NOLINT
    return [ComWuiFrameworkMicrocoreHardware_Keyboard IsCommand:$event] && [[$event charactersIgnoringModifiers] isEqualToString:$keys];
}

+ (BOOL)IsNumericEvent:(NSEvent *)$event { // NOLINT
    if ($event.modifierFlags & NSNumericPadKeyMask) {
        return true;
    }

    switch ($event.keyCode) {
        case 71:  // Clear
        case 81:  // =
        case 75:  // /
        case 67:  // *
        case 78:  // -
        case 69:  // +
        case 76:  // Enter
        case 65:  // .
        case 82:  // 0
        case 83:  // 1
        case 84:  // 2
        case 85:  // 3
        case 86:  // 4
        case 87:  // 5
        case 88:  // 6
        case 89:  // 7
        case 91:  // 8
        case 92:  // 9
        return true;
    }

    return false;
}

+ (BOOL)IsKeyUpEvent:(NSEvent *)event { // NOLINT
    if (event.type != NSFlagsChanged) {
        return event.type == NSKeyUp;
    }

    switch (event.keyCode) {
        case 54:  // Right Command
        case 55:  // Left Command
            return (event.modifierFlags & NSCommandKeyMask) == 0;

        case 57:  // Capslock
            return (event.modifierFlags & NSAlphaShiftKeyMask) == 0;

        case 56:  // Left Shift
        case 60:  // Right Shift
            return (event.modifierFlags & NSShiftKeyMask) == 0;

        case 58:  // Left Alt
        case 61:  // Right Alt
            return (event.modifierFlags & NSAlternateKeyMask) == 0;

        case 59:  // Left Ctrl
        case 62:  // Right Ctrl
            return (event.modifierFlags & NSControlKeyMask) == 0;

        case 63:  // Function
            return (event.modifierFlags & NSFunctionKeyMask) == 0;
    }

    return false;
}

- (id)Initialize:(Io::Oidis::Microcore::Windows::Window *)$window { // NOLINT
    if (self = [super init]) {
        window = $window;
        hasMarkedText = NO;
        hadMarkedText = NO;
        handlingKeypress = NO;
        unmarkTextCalled = NO;
        markedRange = NSMakeRange(NSNotFound, 0);
        selectedRange = NSMakeRange(NSNotFound, 0);

        textInputContext = [[NSTextInputContext alloc] initWithClient:self];
        if (textInputContext == nil) {
            throw Exceptions::KeyboardError{"Failed to create NSTextInputContext instance"};
        }
    }

    return self;
}

- (NSTextInputContext *)getTextInputContext { // NOLINT
    return textInputContext;
}

- (void)HandleKeyEventBeforeTextInputClient:(NSEvent *)$keyEvent { // NOLINT
    hadMarkedText = hasMarkedText;
    handlingKeypress = YES;

    textToBeInserted.clear();
    markedText.clear();
    underlines.clear();
    setMarkedTextReplacementRange = CefRange(UINT32_MAX, UINT32_MAX);
    unmarkTextCalled = NO;
}

- (void)HandleKeyEventAfterTextInputClient:(CefKeyEvent)$keyEvent { // NOLINT
    handlingKeypress = NO;

    if (!hasMarkedText && !hadMarkedText && textToBeInserted.length() <= 1) {
        $keyEvent.type = KEYEVENT_KEYDOWN;

        window->getBrowser()->GetHost()->SendKeyEvent($keyEvent);

        if ($keyEvent.modifiers & EVENTFLAG_IS_KEY_PAD) {
            if ($keyEvent.native_key_code == 71) {
                return;
            }
        }

        $keyEvent.type = KEYEVENT_CHAR;
        window->getBrowser()->GetHost()->SendKeyEvent($keyEvent);
    }

    BOOL textInserted = NO;
    if (textToBeInserted.length() > ((hasMarkedText || hadMarkedText) ? 0u : 1u)) {
        window->getBrowser()->GetHost()->ImeCommitText(textToBeInserted, CefRange(UINT32_MAX, UINT32_MAX), 0);
        textToBeInserted.clear();
    }

    if (hasMarkedText && markedText.length()) {
        window->getBrowser()->GetHost()->ImeSetComposition(markedText,
                                              underlines,
                                              setMarkedTextReplacementRange,
                                              CefRange(selectedRange.location, NSMaxRange(selectedRange)));
    } else if (hadMarkedText && !hasMarkedText && !textInserted) {
        if (unmarkTextCalled) {
            window->getBrowser()->GetHost()->ImeFinishComposingText(false);
        } else {
            window->getBrowser()->GetHost()->ImeCancelComposition();
        }
    }

    setMarkedTextReplacementRange = CefRange(UINT32_MAX, UINT32_MAX);
}

- (void)dealloc {
    [textInputContext release];
    textInputContext = nil;

    [super dealloc];
}

- (NSAttributedString*)attributedSubstringForProposedRange:(const NSRange)$range actualRange:(NSRangePointer)$actualRange { // NOLINT
    return nil;
}

- (NSUInteger)characterIndexForPoint:(NSPoint)$point { // NOLINT
    return NSNotFound;
}

- (void)doCommandBySelector:(SEL)$selector { // NOLINT
}

- (void)extractUnderlines:(NSAttributedString *)$string { // NOLINT
}

- (NSRect)firstRectForCharacterRange:(const NSRange)$range actualRange:(NSRangePointer)$actualRange { // NOLINT
    NSRect rect = [self screenRectFromViewRect:[self firstViewRectForCharacterRange:$range actualRange:$actualRange]];

    if (rect.origin.y >= rect.size.height) {
        rect.origin.y -= rect.size.height;
    } else {
        rect.origin.y = 0;
    }

    return rect;
}

- (NSRect)firstViewRectForCharacterRange:(const NSRange)$range actualRange:(NSRangePointer)$actualRange { // NOLINT
    NSRect rect = NSMakeRect(0, 0, 0, 0);
    NSUInteger location = $range.location;

    if (location == NSNotFound) {
        location = markedRange.location;
    }

    if (location >= markedRange.location) {
        location -= markedRange.location;
    }

    if (location < compositionBounds.size()) {
        const CefRect & compositionRectangle = compositionBounds[location];
        rect = NSMakeRect(compositionRectangle.x, compositionRectangle.y, compositionRectangle.width, compositionRectangle.height);
    }

    if ($actualRange) {
        *$actualRange = NSMakeRange(location, $range.length);
    }

  return rect;
}

- (BOOL)hasMarkedText {
    return hasMarkedText;
}

- (void)insertText:(id)$string replacementRange:(NSRange)$replacementRange { // NOLINT
    // TODO(nxf45876): extract into common method
    const BOOL isAttributedString = [$string isKindOfClass:[NSAttributedString class]];
    NSString *rawText = isAttributedString ? [$string string] : $string;

    if (handlingKeypress) {
        textToBeInserted.append([rawText UTF8String]);
    } else {
        cef_range_t range = {static_cast<int>($replacementRange.location), static_cast<int>(NSMaxRange($replacementRange))};
        window->getBrowser()->GetHost()->ImeCommitText([rawText UTF8String], range, 0);
    }

    hasMarkedText = NO;
}

- (NSRange)markedRange {
    // TODO(nxf45876): extract into common function
    return markedRange.location != NSNotFound && markedRange.length > 0 ? markedRange : NSMakeRange(NSNotFound, 0);
}

- (NSRect)screenRectFromViewRect:(const NSRect)$rect { // NOLINT
    int screenX = 0;
    int screenY = 0;

    window->getBrowser()->GetHost()->GetClient()->GetRenderHandler()->GetScreenPoint(window->getBrowser(),
                                                                                     $rect.origin.x,
                                                                                     $rect.origin.y,
                                                                                     screenX,
                                                                                     screenY);

    return {NSMakePoint(screenX, screenY), $rect.size};
}

- (NSRange)selectedRange {
    return selectedRange.location != NSNotFound && selectedRange.length > 0 ? selectedRange : NSMakeRange(NSNotFound, 0);
}

- (void)setMarkedText:(id)$string selectedRange:(const NSRange)$selectedRange replacementRange:(NSRange)$replacementRange { // NOLINT
    const BOOL isAttributedString = [$string isKindOfClass:[NSAttributedString class]];
    NSString *rawText = isAttributedString ? [$string string] : $string;
    const int length = rawText.length;

    selectedRange = $selectedRange;
    markedText = [rawText UTF8String];
    hasMarkedText = (length > 0);
    underlines.clear();

    if (isAttributedString) {
        [self extractUnderlines:$string];
    } else {
        cef_composition_underline_t line = {{0, length}, 0xFF000000, 0, false};
        underlines.emplace_back(std::move(line));
    }

    if (handlingKeypress) {
        setMarkedTextReplacementRange = {static_cast<int>($replacementRange.location), static_cast<int>(NSMaxRange($replacementRange))};
    } else {
        const CefRange replacementRange($replacementRange.location, NSMaxRange($replacementRange));
        const CefRange selectionRange($selectedRange.location, NSMaxRange($selectedRange));

        window->getBrowser()->GetHost()->ImeSetComposition(markedText, underlines, replacementRange, selectionRange);
    }
}

- (void)unmarkText {
    hasMarkedText = NO;
    markedText.clear();
    underlines.clear();

    if (!handlingKeypress) {
        window->getBrowser()->GetHost()->ImeFinishComposingText(false);
    } else {
        unmarkTextCalled = YES;
    }
}

- (NSArray *)validAttributesForMarkedText { // NOLINT
    return [NSArray arrayWithObjects:NSUnderlineStyleAttributeName,
                                     NSUnderlineColorAttributeName,
                                     NSMarkedClauseSegmentAttributeName,
                                     NSTextInputReplacementRangeAttributeName,
                                     nil];
}

@end

#endif  // MAC_PLATFORM

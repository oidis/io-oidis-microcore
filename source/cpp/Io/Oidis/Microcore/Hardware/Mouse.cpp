/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014 The Chromium Embedded Framework Authors
 * Copyright (c) 2018 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

namespace Io::Oidis::Microcore::Hardware::Mouse {
    CefMouseEvent GetCefMouseEvent(const int $x, const int $y, const uint32 $modifiers) {
        CefMouseEvent mouseEvent;
        mouseEvent.x = $x;
        mouseEvent.y = $y;
        mouseEvent.modifiers = $modifiers;

        return mouseEvent;
    }
}

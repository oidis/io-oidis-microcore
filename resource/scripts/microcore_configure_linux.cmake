# * ********************************************************************************************************* *
# *
# * Copyright (c) 2018 NXP
# * Copyright (c) 2019 Oidis
# *
# * SPDX-License-Identifier: BSD-3-Clause
# * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
# * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
# *
# * ********************************************************************************************************* *

find_package (PkgConfig REQUIRED)
pkg_check_modules (GTK2 REQUIRED gtk+-2.0 gtkglext-1.0)

list(APPEND _includeDirectories ${GTK2_INCLUDE_DIRS})

list(APPEND _compileDefinitions OUT_OF_MEM=ENOMEM)

list(APPEND _libraries
        GL
        pthread
        ${GTK2_LIBRARIES})


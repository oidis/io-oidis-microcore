/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

const Loader = Io.Oidis.Builder.Loader;
const LogIt = Io.Oidis.Commons.Utils.LogIt;
const ObjectEncoder = Io.Oidis.Commons.Utils.ObjectEncoder;
const UpdatesManagerConnector = Io.Oidis.Services.Connectors.UpdatesManagerConnector;

const terminal = Loader.getInstance().getTerminal();
const properties = Loader.getInstance().getAppProperties();
const fileSystem = Loader.getInstance().getFileSystemHandler();

function buildTask($workingDirectory, $buildPath, $targetName, $config, $callback) {
    terminal.Spawn("cmake", [
        "--build", $buildPath,
        "--target", $targetName,
        "--config", $config
    ], $workingDirectory, ($exitCode) => {
        if ($exitCode === 0) {
            $callback();
        } else {
            LogIt.Error("Build task has failed");
        }
    });
}

function generateFromSources($workingDirectory, $generator, $configuration, $installPath, $callback) {
    const sourcePath = ".";
    const buildPath = properties.projectBase + "/build/resource/libs/com-wui-framework-microcore-msvc";
    const cefBinaryPath = properties.projectBase + "/dependencies/cef";

    terminal.Spawn("cmake", [
        "-B" + buildPath,
        "-H" + sourcePath,
        "-G", "\"" + $generator + "\"",
        "-DUSE_SANDBOX:BOOL=OFF",
        "-DUSE_ATL:BOOL=ON",
        "-DCMAKE_INSTALL_PREFIX:PATH=" + $installPath,
        "-DCMAKE_BUILD_TYPE:STRING=" + $configuration,
        "-DCEF_BINARY_PATH:PATH=" + cefBinaryPath,
        "-DUSE_CEF_CMAKE_FUNCTIONS:BOOL=ON"
    ], $workingDirectory, ($exitCode) => {
        if ($exitCode === 0) {
            buildTask($workingDirectory, buildPath, "com-wui-framework-microcore-msvc", $configuration, () => {
                buildTask($workingDirectory, buildPath, "install", $configuration,() => {
                    $callback();
                });
            });
        } else {
            LogIt.Error("CMake generation has failed");
        }
    });
}

function deployToServer($installPath, $version, $callback) {
    const version = $version.replace(/\./g, "-");
    const archiveName = "com-wui-framework-microcore-msvc-" + version + ".zip";
    const archivePath = properties.projectBase + "/build/deployment/" + archiveName;

    LogIt.Info("Going to deploy " + archivePath + ", version " + $version + " resolved to: " + version);

    fileSystem.Pack($installPath, {
        output: archivePath,
        type  : "zip",
        override: true
    }, () => {
        const fs = require("fs");
        const buffer = require("buffer");

        const contents = fs.readFileSync(archivePath);
        const size =  fs.statSync(archivePath).size;
        const data = new buffer.Buffer.from(contents);
        const client = new UpdatesManagerConnector(false, "https://hub.wuiframework.com/connector.config.jsonp");
        client.getEvents().OnError(($eventArgs) => {
            LogIt.Error($eventArgs.Exception().ToString());
        });

        const chunk  = {
            data : data.toString("base64"),
            end  : size,
            id   : archiveName,
            index: 0,
            name : ObjectEncoder.Base64(archiveName),
            size : size,
            start: 0
        };

        LogIt.Info("Going to upload chunk " + chunk.id + " with size of " + chunk.size + " bytes");

        client.UploadFile(chunk)
            .Then(($status) => {
                if (!$status) {
                    LogIt.Error("Deployment has failed");
                } else {
                    LogIt.Info("Deployment successful");

                    $callback();
                }
            });
    });
}

/**
 * Install script attributes might be:
 *     - build-from-sources => optional, generates project file and builds from sources
 *     - deploy => optional, deploys to the server
 *     - <deploy-version> => optional (mandatory when "deploy" used) => specifies version in archive file name
 *
 * Example: ["build-from-sources", "deploy", "2018.3.1.0"] => builds library, creates archive
 *                                                            "com-wui-framework-microcore-msvc-2018.zip" and deploys it
 */
Process = function ($cwd, $args, $done) {
    const workingDirectory = properties.resources + "/libs/com-wui-framework-microcore-msvc";
    const installPath = properties.projectBase + "/dependencies/com-wui-framework-microcore-msvc";

    const getDeployVersion = function () {
        return $args[$args.indexOf("deploy") + 1];
    };

    const generateAndBuild = function ($callback) {
        generateFromSources(workingDirectory, "Visual Studio 15 2017 Win64", "Release", installPath, $callback);
    };

    const deploy = function () {
        deployToServer(installPath, getDeployVersion(), $done);
    };

    const build = function () {
        generateAndBuild($done);
    };

    const buildAndDeploy = function () {
        generateAndBuild(deploy);
    };

    const shouldBuild = $args.indexOf("build-from-sources") !== -1;
    const shouldDeploy = $args.indexOf("deploy") !== -1;

    if (shouldBuild && shouldDeploy) {
        buildAndDeploy();
    } else if (shouldBuild) {
        build();
    } else if (shouldDeploy) {
        deploy();
    } else {
        $done();
    }
};

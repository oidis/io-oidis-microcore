# * ********************************************************************************************************* *
# *
# * Copyright (c) 2018 NXP
# * Copyright (c) 2019 Oidis
# *
# * SPDX-License-Identifier: BSD-3-Clause
# * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
# * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
# *
# * ********************************************************************************************************* *

include(resource/scripts/microcore_compiler_check.cmake)

set(_includeDirectories)
set(_compileDefinitions WRAPPING_CEF_SHARED)
set(_compileOptions -Wno-deprecated -Wno-pragmas -Wno-unknown-pragmas -Wno-placement-new -Wno-comment)
set(_libraries)

if (CMAKE_CXX_COMPILER_ID MATCHES "Clang")
    list(APPEND _compileOptions -Wno-c++17-extensions)
elseif (CMAKE_CXX_COMPILER_ID MATCHES "GNU")
    list(APPEND _compileOptions -Wno-misleading-indentation)
endif ()

if (WIN32)
    include(resource/scripts/microcore_configure_windows.cmake)
elseif (APPLE)
    include(resource/scripts/microcore_configure_osx.cmake)
elseif (UNIX)
    include(resource/scripts/microcore_configure_linux.cmake)
endif ()

# * ********************************************************************************************************* *
# *
# * Copyright (c) 2018 NXP
# * Copyright (c) 2019 Oidis
# *
# * SPDX-License-Identifier: BSD-3-Clause
# * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
# * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
# *
# * ********************************************************************************************************* *

macro(_check_framework _framework)
    string(FIND ${_framework} "-NOTFOUND" _found)
    string(REPLACE "-NOTFOUND" "" _base_name ${_framework})
    string(REPLACE "_framework_" "" _base_name ${_base_name})

    if (${_found} EQUAL -1)
        message(STATUS "Framework ${_base_name} found")
    else ()
        message(FATAL_ERROR "Framework ${_base_name} was not found")
    endif ()
endmacro()

find_library(_framework_opengl OpenGL)
find_library(_framework_cocoa Cocoa)
# CEF framework itself is being linked during the "cef_configure" step (location is known only there)

_check_framework(${_framework_opengl})
_check_framework(${_framework_cocoa})

list(APPEND _compileDefinitions OUT_OF_MEM=ENOMEM)

list(REMOVE_ITEM _compileOptions -Wno-placement-new)
list(APPEND _compileOptions -x objective-c++)

list(APPEND _libraries
        ${_framework_opengl}
        ${_framework_cocoa}
        ${_framework_cef})

# This is a work-around for known issue in CMake where *sometimes* randomly the CMake on OS X fails to find threads package,
# was trying to solve this issues properly, but to no avail
set(CMAKE_THREAD_LIBS_INIT -lpthread)

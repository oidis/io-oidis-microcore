# * ********************************************************************************************************* *
# *
# * Copyright (c) 2018 NXP
# * Copyright (c) 2019 Oidis
# *
# * SPDX-License-Identifier: BSD-3-Clause
# * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
# * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
# *
# * ********************************************************************************************************* *

list(APPEND _includeDirectories ${CMAKE_SOURCE_DIR}/resource/libs/com-wui-framework-microcore-msvc/source/cpp/Io/Oidis/Microcore)

list(APPEND _compileDefinitions
        OUT_OF_MEM=ERROR_NOT_ENOUGH_MEMORY
        _WIN32_WINNT=0x0A00
        UNICODE
        WIN32_LEAN_AND_MEAN)

list(APPEND _libraries
        opengl32
        version
        ws2_32
        wsock32
        Dwmapi)
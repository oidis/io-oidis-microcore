# CEF to chromiumRE patch for MinGW.

>  This patch allows to build CEF project with MinGW without any direct changes inside CEF project files.

## Requirements

This library requires the Chromium Embedded Framework ([CEF](https://bitbucket.org/chromiumembedded/cef)) binary distribution.

Currently supported/tested builds of CEF:
* 3.3396.1782

## Documentation

This patch contains all necessary overridden files of original CEF project to allow build with MinGW toolchain.

## History

### v2018.0.1
Initial release. Compatible with [CEF branch 3396](https://bitbucket.org/chromiumembedded/cef/wiki/BranchesAndBuilding).

## Licence

This software is owned or controlled by NXP Semiconductors. 
The use of this software is governed by the BSD-3-Clause Licence distributed with this material.
  
See the `LICENSE.txt` file for more details.

---

Author Karel Burda,
Copyright (c) 2016 [Freescale Semiconductor, Inc.](http://freescale.com/),
Copyright (c) 2017 [NXP](http://nxp.com/)
Copyright (c) 2019 [Oidis](https://www.oidis.org/)

# * ********************************************************************************************************* *
# *
# * Copyright (c) 2016 The Chromium Embedded Framework Authors
# * Copyright (c) 2016 Freescale Semiconductor, Inc.
# * Copyright (c) 2017 NXP
# * Copyright (c) 2019 Oidis
# *
# * SPDX-License-Identifier: BSD-3-Clause
# * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
# * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
# *
# * ********************************************************************************************************* *

if (NOT DEFINED _CEF_ROOT_EXPLICIT)
    message(FATAL_ERROR "Use find_package(CEF) to load this file.")
endif ()

macro(PRINT_CEF_CONFIG)
    message(STATUS "*** CEF CONFIGURATION SETTINGS ***")
    message(STATUS "Generator:                    ${CMAKE_GENERATOR}")
    message(STATUS "Platform:                     ${CMAKE_SYSTEM_NAME}")
    message(STATUS "Project architecture:         ${PROJECT_ARCH}")

    if (${CMAKE_GENERATOR} STREQUAL "Ninja" OR ${CMAKE_GENERATOR} STREQUAL "Unix Makefiles")
        message(STATUS "Build type:                   ${CMAKE_BUILD_TYPE}")
    endif ()

    message(STATUS "Binary distribution root:     ${_CEF_ROOT}")

    if (OS_MACOSX)
        message(STATUS "Base SDK:                     ${CMAKE_OSX_SYSROOT}")
        message(STATUS "Target SDK:                   ${CEF_TARGET_SDK}")
    endif ()

    set(_libraries ${CEF_STANDARD_LIBS})
    if (OS_WINDOWS AND USE_SANDBOX)
        list(APPEND _libraries ${CEF_SANDBOX_STANDARD_LIBS})
    endif ()
    message(STATUS "Standard libraries:           ${_libraries}")

    message(STATUS "Compile defines:              ${CEF_COMPILER_DEFINES}")
    message(STATUS "Compile defines (Debug):      ${CEF_COMPILER_DEFINES_DEBUG}")
    message(STATUS "Compile defines (Release):    ${CEF_COMPILER_DEFINES_RELEASE}")
    message(STATUS "C compile flags:              ${CEF_COMPILER_FLAGS} ${CEF_C_COMPILER_FLAGS}")
    message(STATUS "C compile flags (Debug):      ${CEF_COMPILER_FLAGS_DEBUG} ${CEF_C_COMPILER_FLAGS_DEBUG}")
    message(STATUS "C compile flags (Release):    ${CEF_COMPILER_FLAGS_RELEASE} ${CEF_C_COMPILER_FLAGS_RELEASE}")
    message(STATUS "C++ compile flags:            ${CEF_COMPILER_FLAGS} ${CEF_CXX_COMPILER_FLAGS}")
    message(STATUS "C++ compile flags (Debug):    ${CEF_COMPILER_FLAGS_DEBUG} ${CEF_CXX_COMPILER_FLAGS_DEBUG}")
    message(STATUS "C++ compile flags (Release):  ${CEF_COMPILER_FLAGS_RELEASE} ${CEF_CXX_COMPILER_FLAGS_RELEASE}")
    message(STATUS "Exe link flags:               ${CEF_LINKER_FLAGS} ${CEF_EXE_LINKER_FLAGS}")
    message(STATUS "Exe link flags (Debug):       ${CEF_LINKER_FLAGS_DEBUG} ${CEF_EXE_LINKER_FLAGS_DEBUG}")
    message(STATUS "Exe link flags (Release):     ${CEF_LINKER_FLAGS_RELEASE} ${CEF_EXE_LINKER_FLAGS_RELEASE}")
    message(STATUS "Shared link flags:            ${CEF_LINKER_FLAGS} ${CEF_SHARED_LINKER_FLAGS}")
    message(STATUS "Shared link flags (Debug):    ${CEF_LINKER_FLAGS_DEBUG} ${CEF_SHARED_LINKER_FLAGS_DEBUG}")
    message(STATUS "Shared link flags (Release):  ${CEF_LINKER_FLAGS_RELEASE} ${CEF_SHARED_LINKER_FLAGS_RELEASE}")

    if (OS_LINUX OR OS_WINDOWS)
        message(STATUS "CEF Binary files:             ${CEF_BINARY_FILES}")
        message(STATUS "CEF Resource files:           ${CEF_RESOURCE_FILES}")
    endif ()
endmacro()

macro(COPY_FILES target file_list source_dir target_dir)
    foreach (FILENAME ${file_list})
        set(source_file ${source_dir}/${FILENAME})
        set(target_file ${target_dir}/${FILENAME})
        if (IS_DIRECTORY ${source_file})
            add_custom_command(
                    TARGET ${target}
                    POST_BUILD
                    COMMAND ${CMAKE_COMMAND} -E copy_directory "${source_file}" "${target_file}"
                    VERBATIM
            )
        else ()
            add_custom_command(
                    TARGET ${target}
                    POST_BUILD
                    COMMAND ${CMAKE_COMMAND} -E copy_if_different "${source_file}" "${target_file}"
                    VERBATIM
            )
        endif ()
    endforeach ()
endmacro()

macro(ADD_LOGICAL_TARGET target debug_lib release_lib)
    add_library(${target} ${CEF_LIBTYPE} IMPORTED)
    set_target_properties(${target} PROPERTIES
            IMPORTED_LOCATION "${release_lib}"
            IMPORTED_LOCATION_DEBUG "${debug_lib}"
            IMPORTED_LOCATION_RELEASE "${release_lib}"
            )
endmacro()

macro(COPY_FRAMEWORK framework target)
    set(_destination "${CMAKE_SOURCE_DIR}/build/target/${target}.app/Contents/Frameworks/Chromium Embedded Framework.framework")

    add_custom_command(
            TARGET ${target}
            POST_BUILD
            COMMAND ${CMAKE_COMMAND} -E copy_directory
            "${framework}"
            "${_destination}"
            COMMENT "Copying '${framework}' to the '${_destination}' for target ${target}"
            VERBATIM
    )
endmacro()

Release Name: com-wui-framework-microcore-msvc v2019.2.0

com-wui-framework-microcore-msvc
Description: Library that implements MSVC-specific functionality of microcore and provides bridge with microcore (built under the GCC).
Author: Oidis
License: BSD-3-Clause. See LICENSE.txt
Format: shared library
Location: [application-name][.dll]

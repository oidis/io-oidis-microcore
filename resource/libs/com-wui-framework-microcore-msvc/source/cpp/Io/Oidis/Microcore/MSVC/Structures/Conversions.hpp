/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_MICROCORE_MSVC_STRUCTURES_CONVERSIONS_HPP_
#define IO_OIDIS_MICROCORE_MSVC_STRUCTURES_CONVERSIONS_HPP_

#include <string>

#include <include/cef_drag_data.h> 

#include "MSVC/Structures/DragData.hpp"

namespace Io { namespace Oidis { namespace Microcore { namespace MSVC { namespace Structures {
    inline CefRefPtr<CefDragData> CreateDragData(DragData $dragData) {
        const CefRefPtr<CefDragData> dragData = CefDragData::Create();

        dragData->SetLinkURL($dragData.linkUrl);
        dragData->SetLinkTitle($dragData.linkTitle);
        dragData->SetFragmentText($dragData.fragmentText);
        dragData->SetFragmentHtml($dragData.fragmentHtml);
        dragData->SetFragmentBaseURL($dragData.fragmentBaseUrl);

        return dragData;
    }

    inline DragData CreateDragData(CefRefPtr<CefDragData> $dragData) {
        DragData dragData;

        if ($dragData) {
            dragData.fragmentText = strdup($dragData->GetFragmentText().ToString().c_str());
            dragData.isLink = $dragData->IsLink();
            dragData.linkUrl = strdup($dragData->GetLinkURL().ToString().c_str());
            dragData.linkTitle = strdup($dragData->GetLinkTitle().ToString().c_str());
            dragData.fragmentHtml = strdup($dragData->GetFragmentHtml().ToString().c_str());
            dragData.fragmentBaseUrl = strdup($dragData->GetFragmentBaseURL().ToString().c_str());
            dragData.bufferSize = $dragData->GetFileContents(nullptr);
        }

        return dragData;
    }

    inline void DestroyDragData(DragData *$dragData) {
        if ($dragData) {
            LOG(INFO) << "Destroying drag data";

            delete $dragData->fragmentText;
            delete $dragData->linkUrl;
            delete $dragData->linkTitle;
            delete $dragData->fragmentHtml;
            delete $dragData->fragmentBaseUrl;

            $dragData->fragmentText = nullptr;
            $dragData->linkUrl = nullptr;
            $dragData->linkTitle = nullptr;
            $dragData->fragmentHtml = nullptr;
            $dragData->fragmentBaseUrl = nullptr;
        }
    }

    inline CefMouseEvent CreateCefMouseEvent(int $x, int $y, uint32 $modifiers) {
        CefMouseEvent event;

        event.x = $x;
        event.y = $y;
        event.modifiers = $modifiers;

        return event;
    }
}}}}}

#endif  // IO_OIDIS_MICROCORE_MSVC_STRUCTURES_CONVERSIONS_HPP_

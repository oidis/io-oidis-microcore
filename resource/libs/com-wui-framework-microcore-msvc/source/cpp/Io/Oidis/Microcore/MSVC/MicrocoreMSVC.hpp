/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include <include/cef_drag_data.h>
#include <include/cef_render_handler.h>

#include "MSVC/Callbacks/Callbacks.hpp"
#include "MSVC/Structures/Conversions.hpp"
#include "MSVC/Structures/DragData.hpp"

#ifndef IO_OIDIS_MICROCORE_MSVC_MICROCOREMSVC_HPP_
#define IO_OIDIS_MICROCORE_MSVC_MICROCOREMSVC_HPP_

namespace Io { namespace Oidis { namespace Microcore { namespace MSVC {
    extern "C" {
        __declspec(dllexport) const char * __cdecl GetVersion();

        __declspec(dllexport) bool __cdecl Initialize(const char *$id,
                                                      Callbacks::OnDrop $onDrop,
                                                      Callbacks::OnDragLeave $onDragLeave,
                                                      Callbacks::OnDragOver $onDragOver,
                                                      Callbacks::OnDragEnter $onDragEnter,
                                                      HWND $hwnd);

        __declspec(dllexport) bool __cdecl Destroy(HWND $hwnd);

        __declspec(dllexport) bool __cdecl HasTarget();

        __declspec(dllexport) CefBrowserHost::DragOperationsMask __cdecl StartDragging(Structures::DragData $dragData,
                                                                                       CefRenderHandler::DragOperationsMask $allowedOps,
                                                                                       int $x,
                                                                                       int $y);
    }
}}}}

#endif  // IO_OIDIS_MICROCORE_MSVC_MICROCOREMSVC_HPP_

/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014 The Chromium Embedded Framework Authors
 * Copyright (c) 2018 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "MSVC/DragAndDrop/DragAndDrop.hpp"

#include <algorithm>
#include <string>

#include <shellapi.h>
#include <shlobj.h>
#include <windowsx.h>

#include "MSVC/Commons/BytesWriteHandler.hpp"
#include "MSVC/Structures/Conversions.hpp"

namespace Io { namespace Oidis { namespace Microcore { namespace MSVC { namespace DragAndDrop {
    bool IsKeyDown(WPARAM $wparam) {
        return (GetKeyState($wparam) & 0x8000) != 0;
    }

    int GetCefMouseModifiers(WPARAM $wparam) {
        int modifiers = 0;
        if ($wparam & MK_CONTROL) {
            modifiers |= EVENTFLAG_CONTROL_DOWN;
        }
        if ($wparam & MK_SHIFT) {
            modifiers |= EVENTFLAG_SHIFT_DOWN;
        }
        if (IsKeyDown(VK_MENU)) {
            modifiers |= EVENTFLAG_ALT_DOWN;
        }
        if ($wparam & MK_LBUTTON) {
            modifiers |= EVENTFLAG_LEFT_MOUSE_BUTTON;
        }
        if ($wparam & MK_MBUTTON) {
            modifiers |= EVENTFLAG_MIDDLE_MOUSE_BUTTON;
        }
        if ($wparam & MK_RBUTTON) {
            modifiers |= EVENTFLAG_RIGHT_MOUSE_BUTTON;
        }

        // Low bit set from GetKeyState indicates "toggled".
        if (::GetKeyState(VK_NUMLOCK) & 1) {
            modifiers |= EVENTFLAG_NUM_LOCK_ON;
        }
        if (::GetKeyState(VK_CAPITAL) & 1) {
            modifiers |= EVENTFLAG_CAPS_LOCK_ON;
        }

        return modifiers;
    }

    DWORD DragOperationToDropEffect(CefRenderHandler::DragOperation $allowedOps) {
        DWORD effect = DROPEFFECT_NONE;
        if ($allowedOps & DRAG_OPERATION_COPY) {
            effect |= DROPEFFECT_COPY;
        }
        if ($allowedOps & DRAG_OPERATION_LINK) {
            effect |= DROPEFFECT_LINK;
        }
        if ($allowedOps & DRAG_OPERATION_MOVE) {
            effect |= DROPEFFECT_MOVE;
        }
        return effect;
    }

    CefRenderHandler::DragOperationsMask DropEffectToDragOperation(DWORD $effect) {
        DWORD operation = DRAG_OPERATION_NONE;
        if ($effect & DROPEFFECT_COPY) {
            operation |= DRAG_OPERATION_COPY;
        }
        if ($effect & DROPEFFECT_LINK) {
            operation |= DRAG_OPERATION_LINK;
        }
        if ($effect & DROPEFFECT_MOVE) {
            operation |= DRAG_OPERATION_MOVE;
        }
        return static_cast<CefRenderHandler::DragOperationsMask>(operation);
    }

    CefMouseEvent ToMouseEvent(POINTL $p, DWORD $keyState, HWND $hWnd) {
        CefMouseEvent ev;
        POINT screen_point = { $p.x, $p.y };
        ScreenToClient($hWnd, &screen_point);
        ev.x = screen_point.x;
        ev.y = screen_point.y;
        ev.modifiers = GetCefMouseModifiers($keyState);
        return ev;
    }

    void GetStorageForBytes(STGMEDIUM *$storage, const void *$data, size_t $bytes) {
        HANDLE handle = GlobalAlloc(GPTR, static_cast<int>($bytes));
        if (handle) {
            memcpy(handle, $data, $bytes);
        }

        $storage->hGlobal = handle;
        $storage->tymed = TYMED_HGLOBAL;
        $storage->pUnkForRelease = NULL;
    }

    template <typename T>
    void GetStorageForString(STGMEDIUM *$stgmed, const std::basic_string<T> &$data) {
        GetStorageForBytes(
            $stgmed, $data.c_str(),
            ($data.size() + 1) * sizeof(typename std::basic_string<T>::value_type));
    }

    void GetStorageForFileDescriptor(STGMEDIUM *$storage, const std::wstring &$fileName) {
        DCHECK(!$fileName.empty());
        HANDLE hdata = GlobalAlloc(GPTR, sizeof(FILEGROUPDESCRIPTOR));

        FILEGROUPDESCRIPTOR *descriptor = reinterpret_cast<FILEGROUPDESCRIPTOR*>(hdata);
        descriptor->cItems = 1;
        descriptor->fgd[0].dwFlags = FD_LINKUI;
        wcsncpy_s(descriptor->fgd[0].cFileName, MAX_PATH, $fileName.c_str(),
            std::min($fileName.size(), static_cast<size_t>(MAX_PATH - 1u)));

        $storage->tymed = TYMED_HGLOBAL;
        $storage->hGlobal = hdata;
        $storage->pUnkForRelease = NULL;
    }

    // Helper method for converting from text/html to MS CF_HTML.
    // Documentation for the CF_HTML format is available at
    // http://msdn.microsoft.com/en-us/library/aa767917(VS.85).aspx
    std::string HtmlToCFHtml(const std::string &$html, const std::string &$baseUrl) {
        if ($html.empty()) {
            return std::string();
        }

#define MAX_DIGITS 10
#define MAKE_NUMBER_FORMAT_1(digits) MAKE_NUMBER_FORMAT_2(digits)
#define MAKE_NUMBER_FORMAT_2(digits) "%0" #digits "u"
#define NUMBER_FORMAT MAKE_NUMBER_FORMAT_1(MAX_DIGITS)

        static const char* header =
            "Version:0.9\r\n"
            "StartHTML:" NUMBER_FORMAT
            "\r\n"
            "EndHTML:" NUMBER_FORMAT
            "\r\n"
            "StartFragment:" NUMBER_FORMAT
            "\r\n"
            "EndFragment:" NUMBER_FORMAT "\r\n";
        static const char* source_url_prefix = "SourceURL:";

        static const char* start_markup = "<html>\r\n<body>\r\n<!--StartFragment-->";
        static const char* end_markup = "<!--EndFragment-->\r\n</body>\r\n</html>";

        // Calculate offsets
        size_t start_html_offset =
            strlen(header) - strlen(NUMBER_FORMAT) * 4 + MAX_DIGITS * 4;
        if (!$baseUrl.empty()) {
            start_html_offset +=
                strlen(source_url_prefix) + $baseUrl.length() + 2;  // Add 2 for \r\n.
        }
        size_t start_fragment_offset = start_html_offset + strlen(start_markup);
        size_t end_fragment_offset = start_fragment_offset + $html.length();
        size_t end_html_offset = end_fragment_offset + strlen(end_markup);
        char raw_result[1024];
        _snprintf(raw_result, sizeof(1024), header, start_html_offset,
            end_html_offset, start_fragment_offset, end_fragment_offset);
        std::string result = raw_result;
        if (!$baseUrl.empty()) {
            result.append(source_url_prefix);
            result.append($baseUrl);
            result.append("\r\n");
        }
        result.append(start_markup);
        result.append($html);
        result.append(end_markup);

#undef MAX_DIGITS
#undef MAKE_NUMBER_FORMAT_1
#undef MAKE_NUMBER_FORMAT_2
#undef NUMBER_FORMAT

        return result;
    }

    void CFHtmlExtractMetadata(const std::string &$cfHtml,
                               std::string *$baseUrl,
                               size_t *$htmlStart,
                               size_t *$fragmentStart,
                               size_t *$fragmentEnd) {
        // Obtain base_url if present.
        if ($baseUrl) {
            static std::string src_url_str("SourceURL:");
            size_t line_start = $cfHtml.find(src_url_str);
            if (line_start != std::string::npos) {
                size_t src_end = $cfHtml.find("\n", line_start);
                size_t src_start = line_start + src_url_str.length();
                if (src_end != std::string::npos && src_start != std::string::npos) {
                    *$baseUrl = $cfHtml.substr(src_start, src_end - src_start);
                }
            }
        }

        // Find the markup between "<!--StartFragment-->" and "<!--EndFragment-->".
        // If the comments cannot be found, like copying from OpenOffice Writer,
        // we simply fall back to using StartFragment/EndFragment bytecount values
        // to determine the fragment indexes.
        std::string cf_html_lower = $cfHtml;
        size_t markup_start = cf_html_lower.find("<html", 0);
        if ($htmlStart) {
            *$htmlStart = markup_start;
        }
        size_t tag_start = $cfHtml.find("<!--StartFragment", markup_start);
        if (tag_start == std::string::npos) {
            static std::string start_fragment_str("StartFragment:");
            size_t start_fragment_start = $cfHtml.find(start_fragment_str);
            if (start_fragment_start != std::string::npos) {
                *$fragmentStart =
                    static_cast<size_t>(atoi($cfHtml.c_str() + start_fragment_start +
                        start_fragment_str.length()));
            }

            static std::string end_fragment_str("EndFragment:");
            size_t end_fragment_start = $cfHtml.find(end_fragment_str);
            if (end_fragment_start != std::string::npos) {
                *$fragmentEnd = static_cast<size_t>(atoi(
                    $cfHtml.c_str() + end_fragment_start + end_fragment_str.length()));
            }
        } else {
            *$fragmentStart = $cfHtml.find('>', tag_start) + 1;
            size_t tag_end = $cfHtml.rfind("<!--EndFragment", std::string::npos);
            *$fragmentEnd = $cfHtml.rfind('<', tag_end);
        }
    }

    void CFHtmlToHtml(const std::string &$cfHtml, std::string *$html, std::string *$baseUrl) {
        size_t frag_start = std::string::npos;
        size_t frag_end = std::string::npos;

        CFHtmlExtractMetadata($cfHtml, $baseUrl, NULL, &frag_start, &frag_end);

        if ($html && frag_start != std::string::npos && frag_end != std::string::npos) {
            *$html = $cfHtml.substr(frag_start, frag_end - frag_start);
        }
    }

    const DWORD moz_url_format = ::RegisterClipboardFormat(L"text/x-moz-url");
    const DWORD html_format = ::RegisterClipboardFormat(L"HTML Format");
    const DWORD file_desc_format = ::RegisterClipboardFormat(CFSTR_FILEDESCRIPTOR);
    const DWORD file_contents_format = ::RegisterClipboardFormat(CFSTR_FILECONTENTS);

    CefRefPtr<CefDragData> DataObjectToDragData(IDataObject *$dataObject) {
        CefRefPtr<CefDragData> drag_data = CefDragData::Create();
        IEnumFORMATETC* enumFormats = NULL;
        HRESULT res = $dataObject->EnumFormatEtc(DATADIR_GET, &enumFormats);
        if (res != S_OK)
            return drag_data;
        enumFormats->Reset();
        const int kCelt = 10;

        ULONG celtFetched;
        do {
            celtFetched = kCelt;
            FORMATETC rgelt[kCelt];
            res = enumFormats->Next(kCelt, rgelt, &celtFetched);
            for (unsigned i = 0; i < celtFetched; i++) {
                CLIPFORMAT format = rgelt[i].cfFormat;
                if (!(format == CF_UNICODETEXT || format == CF_TEXT ||
                    format == moz_url_format || format == html_format ||
                    format == CF_HDROP) ||
                    rgelt[i].tymed != TYMED_HGLOBAL)
                    continue;
                STGMEDIUM medium;
                if ($dataObject->GetData(&rgelt[i], &medium) == S_OK) {
                    if (!medium.hGlobal) {
                        ReleaseStgMedium(&medium);
                        continue;
                    }
                    void* hGlobal = GlobalLock(medium.hGlobal);
                    if (!hGlobal) {
                        ReleaseStgMedium(&medium);
                        continue;
                    }
                    if (format == CF_UNICODETEXT) {
                        CefString text;
                        text.FromWString((std::wstring::value_type*)hGlobal);
                        drag_data->SetFragmentText(text);
                    } else if (format == CF_TEXT) {
                        CefString text;
                        text.FromString((std::string::value_type*)hGlobal);
                        drag_data->SetFragmentText(text);
                    } else if (format == moz_url_format) {
                        std::wstring html((std::wstring::value_type*)hGlobal);
                        size_t pos = html.rfind('\n');
                        CefString url(html.substr(0, pos));
                        CefString title(html.substr(pos + 1));
                        drag_data->SetLinkURL(url);
                        drag_data->SetLinkTitle(title);
                    } else if (format == html_format) {
                        std::string cf_html((std::string::value_type*)hGlobal);
                        std::string base_url;
                        std::string html;
                        CFHtmlToHtml(cf_html, &html, &base_url);
                        drag_data->SetFragmentHtml(html);
                        drag_data->SetFragmentBaseURL(base_url);
                    }
                    if (format == CF_HDROP) {
                        HDROP hdrop = (HDROP)hGlobal;
                        const int kMaxFilenameLen = 4096;
                        const unsigned num_files = DragQueryFileW(hdrop, 0xffffffff, 0, 0);
                        for (unsigned int x = 0; x < num_files; ++x) {
                            wchar_t filename[kMaxFilenameLen];
                            if (!DragQueryFileW(hdrop, x, filename, kMaxFilenameLen))
                                continue;
                            WCHAR* name = wcsrchr(filename, '\\');
                            drag_data->AddFile(filename, (name ? name + 1 : filename));
                        }
                    }
                    if (medium.hGlobal) {
                        GlobalUnlock(medium.hGlobal);
                    }
                    if (format == CF_HDROP) {
                        DragFinish((HDROP)hGlobal);
                    } else {
                        ReleaseStgMedium(&medium);
                    }
                }
            }
        } while (res == S_OK);
        enumFormats->Release();
        return drag_data;
    }

    CComPtr<DropTargetWin> DropTargetWin::Create(const std::string &$id,
                                                 Callbacks::OnDrop $onDrop,
                                                 Callbacks::OnDragLeave $onDragLeave,
                                                 Callbacks::OnDragOver $onDragOver,
                                                 Callbacks::OnDragEnter $onDragEnter,
                                                 HWND $hWnd) {
        return CComPtr<DropTargetWin>(new DropTargetWin($id, $onDrop, $onDragLeave, $onDragOver, $onDragEnter, $hWnd));
    }

    CefBrowserHost::DragOperationsMask DropTargetWin::StartDragging(Structures::DragData $dragData,
                                                                    CefRenderHandler::DragOperationsMask $allowedOps,
                                                                    int $x,
                                                                    int $y) {
        LOG(INFO) << "StartDragging on ID " << id;
        const CefRefPtr<CefDragData> dragData = Structures::CreateDragData($dragData);

        CComPtr<IDataObject> dataObject;
        DWORD resEffect = DROPEFFECT_NONE;

        const int kMaxDataObjects = 10;
        FORMATETC fmtetcs[kMaxDataObjects];
        STGMEDIUM stgmeds[kMaxDataObjects];
        FORMATETC fmtetc = { 0, NULL, DVASPECT_CONTENT, -1, TYMED_HGLOBAL };
        int curr_index = 0;

        CefString text = dragData->GetFragmentText();
        if (!text.empty()) {
            fmtetc.cfFormat = CF_UNICODETEXT;
            fmtetcs[curr_index] = fmtetc;
            GetStorageForString(&stgmeds[curr_index], text.ToWString());
            curr_index++;
        }
        if (dragData->IsLink() && !dragData->GetLinkURL().empty()) {
            std::wstring x_moz_url_str = dragData->GetLinkURL().ToWString();
            x_moz_url_str += '\n';
            x_moz_url_str += dragData->GetLinkTitle().ToWString();
            fmtetc.cfFormat = moz_url_format;
            fmtetcs[curr_index] = fmtetc;
            GetStorageForString(&stgmeds[curr_index], x_moz_url_str);
            curr_index++;
        }
        CefString html = dragData->GetFragmentHtml();
        if (!html.empty()) {
            CefString base_url = dragData->GetFragmentBaseURL();
            std::string cfhtml = HtmlToCFHtml(html.ToString(), base_url.ToString());
            fmtetc.cfFormat = html_format;
            fmtetcs[curr_index] = fmtetc;
            GetStorageForString(&stgmeds[curr_index], cfhtml);
            curr_index++;
        }

        size_t bufferSize = dragData->GetFileContents(NULL);
        if (bufferSize) {
            CefRefPtr<Commons::BytesWriteHandler> handler = new Commons::BytesWriteHandler(bufferSize);
            CefRefPtr<CefStreamWriter> writer = CefStreamWriter::CreateForHandler(handler.get());
            dragData->GetFileContents(writer);
            DCHECK_EQ(handler->GetDataSize(), static_cast<int64>(bufferSize));
            CefString fileName = dragData->GetFileName();
            GetStorageForFileDescriptor(&stgmeds[curr_index], fileName.ToWString());
            fmtetc.cfFormat = file_desc_format;
            fmtetcs[curr_index] = fmtetc;
            curr_index++;
            GetStorageForBytes(&stgmeds[curr_index], handler->GetData(), handler->GetDataSize());
            fmtetc.cfFormat = file_contents_format;
            fmtetcs[curr_index] = fmtetc;
            curr_index++;
        }

        DCHECK_LT(curr_index, kMaxDataObjects);
        CComPtr<DataObjectWin> obj = DataObjectWin::Create(fmtetcs, stgmeds, curr_index);
        dataObject = obj.Detach();
        CComPtr<DropSourceWin> dropSource = DropSourceWin::Create();
        DWORD effect = DragOperationToDropEffect($allowedOps);
        this->current_drag_data_ = dragData->Clone();
        this->current_drag_data_->ResetFileContents();
        HRESULT res = DoDragDrop(dataObject, dropSource, effect, &resEffect);
        if (res != DRAGDROP_S_DROP) {
            resEffect = DROPEFFECT_NONE;
        }
        this->current_drag_data_ = nullptr;

        const CefRenderHandler::DragOperationsMask mask = DropEffectToDragOperation(resEffect);

        return mask;
    }

    HRESULT DropTargetWin::DragEnter(IDataObject *$dataObject,
        DWORD $keyState,
        POINTL $cursorPosition,
        DWORD *$effect) {
        LOG(INFO) << "DragEnter on id " << id;
        if (!this->onDragEnter) {
            return E_UNEXPECTED;
        }

        CefRefPtr<CefDragData> drag_data = this->current_drag_data_;
        if (!drag_data) {
            drag_data = DataObjectToDragData($dataObject);
        }
        CefMouseEvent ev = ToMouseEvent($cursorPosition, $keyState, hWnd);
        CefBrowserHost::DragOperationsMask mask = DropEffectToDragOperation(*$effect);

        Structures::DragData dragData = Structures::CreateDragData(drag_data);
        mask = this->onDragEnter(id.c_str(), dragData, ev.x, ev.y, ev.modifiers, mask);
        Structures::DestroyDragData(&dragData);
        *$effect = DragOperationToDropEffect(mask);
        return S_OK;
    }

    HRESULT DropTargetWin::DragOver(DWORD $keyState, POINTL $cursorPosition, DWORD *$effect) {
        LOG(INFO) << "DragEnter on id " << id;
        if (!this->onDragOver) {
            return E_UNEXPECTED;
        }
        CefMouseEvent ev = ToMouseEvent($cursorPosition, $keyState, hWnd);
        CefBrowserHost::DragOperationsMask mask = DropEffectToDragOperation(*$effect);
        mask = this->onDragOver(this->id.c_str(), ev.x, ev.y, ev.modifiers, mask);
        *$effect = DragOperationToDropEffect(mask);
        return S_OK;
    }

    HRESULT DropTargetWin::DragLeave() {
        LOG(INFO) << "DragLeave on id " << id;
        if (!this->onDragLeave) {
            return E_UNEXPECTED;
        }
        this->onDragLeave(this->id.c_str());
        return S_OK;
    }

    HRESULT DropTargetWin::Drop(IDataObject *$dataObject, DWORD $keyState, POINTL $cursorPosition, DWORD *$effect) {
        LOG(INFO) << "Drop on id " << id;
        if (!this->onDrop) {
            return E_UNEXPECTED;
        }
        CefMouseEvent ev = ToMouseEvent($cursorPosition, $keyState, hWnd);
        CefBrowserHost::DragOperationsMask mask = DropEffectToDragOperation(*$effect);
        mask = this->onDrop(this->id.c_str(), ev.x, ev.y, ev.modifiers, mask);
        *$effect = DragOperationToDropEffect(mask);
        return S_OK;
    }

    CComPtr<DropSourceWin> DropSourceWin::Create() {
        return CComPtr<DropSourceWin>(new DropSourceWin());
    }

    HRESULT DropSourceWin::GiveFeedback(DWORD $dwEffect) {
        return DRAGDROP_S_USEDEFAULTCURSORS;
    }

    HRESULT DropSourceWin::QueryContinueDrag(BOOL $fEscapePressed, DWORD $grfKeyState) {
        if ($fEscapePressed) {
            return DRAGDROP_S_CANCEL;
        }

        if (!($grfKeyState & MK_LBUTTON)) {
            return DRAGDROP_S_DROP;
        }

        return S_OK;
    }

    HRESULT DragEnumFormatEtc::CreateEnumFormatEtc(UINT $cfmt,
                                                   FORMATETC *$afmt,
                                                   IEnumFORMATETC **$ppEnumFormatEtc) {
        if ($cfmt == 0 || $afmt == 0 || $ppEnumFormatEtc == 0) {
            return E_INVALIDARG;
        }

        *$ppEnumFormatEtc = new DragEnumFormatEtc($afmt, $cfmt);

        return (*$ppEnumFormatEtc) ? S_OK : E_OUTOFMEMORY;
    }

    HRESULT DragEnumFormatEtc::Next(ULONG $celt,
                                    FORMATETC *$pFormatEtc,
                                    ULONG *$pceltFetched) {
        ULONG copied = 0;

        // copy the FORMATETC structures into the caller's buffer
        while (this->m_nIndex < this->m_nNumFormats && copied < $celt) {
            DeepCopyFormatEtc(&$pFormatEtc[copied], &this->m_pFormatEtc[this->m_nIndex]);
            copied++;
            this->m_nIndex++;
        }

        // store result
        if ($pceltFetched != 0) {
            *$pceltFetched = copied;
        }

        // did we copy all that was requested?
        return (copied == $celt) ? S_OK : S_FALSE;
    }

    HRESULT DragEnumFormatEtc::Skip(ULONG $celt) {
        this->m_nIndex += $celt;
        return (this->m_nIndex <= this->m_nNumFormats) ? S_OK : S_FALSE;
    }

    HRESULT DragEnumFormatEtc::Reset() {
        this->m_nIndex = 0;
        return S_OK;
    }

    HRESULT DragEnumFormatEtc::Clone(IEnumFORMATETC **$ppEnumFormatEtc) {
        HRESULT hResult;

        // make a duplicate enumerator
        hResult = CreateEnumFormatEtc(this->m_nNumFormats, this->m_pFormatEtc, $ppEnumFormatEtc);

        if (hResult == S_OK) {
            // manually set the index state
            reinterpret_cast<DragEnumFormatEtc*>(*$ppEnumFormatEtc)->m_nIndex = this->m_nIndex;
        }

        return hResult;
    }

    DragEnumFormatEtc::DragEnumFormatEtc(FORMATETC *$pFormatEtc, int $nNumFormats) {
        AddRef();

        this->m_nIndex = 0;
        this->m_nNumFormats = $nNumFormats;
        this->m_pFormatEtc = new FORMATETC[$nNumFormats];

        // make a new copy of each FORMATETC structure
        for (int i = 0; i < $nNumFormats; i++) {
            DeepCopyFormatEtc(&this->m_pFormatEtc[i], &$pFormatEtc[i]);
        }
    }

    DragEnumFormatEtc::~DragEnumFormatEtc() {
        // first free any DVTARGETDEVICE structures
        for (ULONG i = 0; i < this->m_nNumFormats; i++) {
            if (this->m_pFormatEtc[i].ptd)
                CoTaskMemFree(this->m_pFormatEtc[i].ptd);
        }

        // now free the main array
        delete[] this->m_pFormatEtc;
    }

    void DragEnumFormatEtc::DeepCopyFormatEtc(FORMATETC *$dest, FORMATETC *$source) {
        // copy the source FORMATETC into dest
        *$dest = *$source;
        if ($source->ptd) {
            // allocate memory for the DVTARGETDEVICE if necessary
            $dest->ptd = reinterpret_cast<DVTARGETDEVICE*>(
                CoTaskMemAlloc(sizeof(DVTARGETDEVICE)));

            // copy the contents of the source DVTARGETDEVICE into dest->ptd
            *($dest->ptd) = *($source->ptd);
        }
    }

    CComPtr<DataObjectWin> DataObjectWin::Create(FORMATETC *$fmtetc, STGMEDIUM *$stgmed, int $count) {
        return CComPtr<DataObjectWin>(new DataObjectWin($fmtetc, $stgmed, $count));
    }

    HRESULT DataObjectWin::GetDataHere(FORMATETC *$pFormatEtc, STGMEDIUM *$pmedium) {
        return E_NOTIMPL;
    }

    HRESULT DataObjectWin::QueryGetData(FORMATETC *$pFormatEtc) {
        return (LookupFormatEtc($pFormatEtc) == -1) ? DV_E_FORMATETC : S_OK;
    }

    HRESULT DataObjectWin::GetCanonicalFormatEtc(FORMATETC *$pFormatEct, FORMATETC *$pFormatEtcOut) {
        $pFormatEtcOut->ptd = NULL;
        return E_NOTIMPL;
    }

    HRESULT DataObjectWin::SetData(FORMATETC *$pFormatEtc, STGMEDIUM *$pMedium, BOOL $fRelease) {
        return E_NOTIMPL;
    }

    HRESULT DataObjectWin::DAdvise(FORMATETC *$pFormatEtc, DWORD $advf, IAdviseSink *, DWORD *) {
        return E_NOTIMPL;
    }

    HRESULT DataObjectWin::DUnadvise(DWORD $dwConnection) {
        return E_NOTIMPL;
    }

    HRESULT DataObjectWin::EnumDAdvise(IEnumSTATDATA **$ppEnumAdvise) {
        return E_NOTIMPL;
    }

    HRESULT DataObjectWin::EnumFormatEtc(DWORD $dwDirection, IEnumFORMATETC **$ppEnumFormatEtc) {
        return DragEnumFormatEtc::CreateEnumFormatEtc(this->m_nNumFormats, this->m_pFormatEtc, $ppEnumFormatEtc);
    }

    HRESULT DataObjectWin::GetData(FORMATETC *$pFormatEtc, STGMEDIUM *$pMedium) {
        int idx = -1;

        // try to match the specified FORMATETC with one of our supported formats
        if ((idx = LookupFormatEtc($pFormatEtc)) == -1) {
            return DV_E_FORMATETC;
            }

        // found a match - transfer data into supplied storage medium
        $pMedium->tymed = this->m_pFormatEtc[idx].tymed;
        $pMedium->pUnkForRelease = 0;

        // copy the data into the caller's storage medium
        switch (this->m_pFormatEtc[idx].tymed) {
        case TYMED_HGLOBAL:
            $pMedium->hGlobal = DupGlobalMem(this->m_pStgMedium[idx].hGlobal);
            break;

        default:
            return DV_E_FORMATETC;
        }
        return S_OK;
    }

    HGLOBAL DataObjectWin::DupGlobalMem(HGLOBAL $hMem) {
        DWORD len = GlobalSize($hMem);
        PVOID source = GlobalLock($hMem);
        PVOID dest = GlobalAlloc(GMEM_FIXED, len);

        memcpy(dest, source, len);
        GlobalUnlock($hMem);
        return dest;
    }

    int DataObjectWin::LookupFormatEtc(FORMATETC *$pFormatEtc) {
        // check each of our formats in turn to see if one matches
        for (int i = 0; i < this->m_nNumFormats; i++) {
            if ((this->m_pFormatEtc[i].tymed & $pFormatEtc->tymed) &&
                this->m_pFormatEtc[i].cfFormat == $pFormatEtc->cfFormat &&
                this->m_pFormatEtc[i].dwAspect == $pFormatEtc->dwAspect) {
                // return index of stored format
                return i;
            }
        }

        // error, format not found
        return -1;
    }

    DataObjectWin::DataObjectWin(FORMATETC *$fmtetc, STGMEDIUM *$stgmed, int $count)
        : ref_count_(0) {
        this->m_nNumFormats = $count;

        this->m_pFormatEtc = new FORMATETC[$count];
        this->m_pStgMedium = new STGMEDIUM[$count];

        for (int i = 0; i < $count; i++) {
            this->m_pFormatEtc[i] = $fmtetc[i];
            this->m_pStgMedium[i] = $stgmed[i];
        }
    }
}
}}}}

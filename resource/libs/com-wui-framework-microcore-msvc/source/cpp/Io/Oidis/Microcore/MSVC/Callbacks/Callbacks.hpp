/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_MICROCORE_MSVC_CALLBACKS_CALLBACKS_HPP_
#define IO_OIDIS_MICROCORE_MSVC_CALLBACKS_CALLBACKS_HPP_

#include <cstdint>
#include <type_traits>

#include <include/cef_browser.h>

#include "MSVC/Structures/DragData.hpp"

namespace Io { namespace Oidis { namespace Microcore { namespace MSVC { namespace Callbacks {
    // Cannot use std::function because of the GCC-MSVC compatibility, using plain function pointers instead
    using OnDrop = std::add_pointer<CefBrowserHost::DragOperationsMask __cdecl(const char *id,
                                                                               int x,
                                                                               int y,
                                                                               uint32 mouseModifiers,
                                                                               CefBrowserHost::DragOperationsMask mask)>::type;

    using OnDragLeave = std::add_pointer<void __cdecl(const char *id)>::type;

    using OnDragOver = std::add_pointer<CefBrowserHost::DragOperationsMask __cdecl(const char *id,
                                                                                   int x,
                                                                                   int y,
                                                                                   uint32 mouseModifiers,
                                                                                   CefBrowserHost::DragOperationsMask mask)>::type;

    using OnDragEnter = std::add_pointer<CefBrowserHost::DragOperationsMask __cdecl(const char *id,
                                                                                    Structures::DragData dragData,
                                                                                    int x,
                                                                                    int y,
                                                                                    uint32 mouseModifiers,
                                                                                    CefBrowserHost::DragOperationsMask mask)>::type;
}}}}}

#endif  // IO_OIDIS_MICROCORE_MSVC_CALLBACKS_CALLBACKS_HPP_

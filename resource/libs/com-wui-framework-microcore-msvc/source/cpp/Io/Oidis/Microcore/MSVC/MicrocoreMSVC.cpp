/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include <mutex>

#include <atlcomcli.h>

#include "MSVC/MicrocoreMSVC.hpp"
#include "MSVC/DragAndDrop/DragAndDrop.hpp"
#include "MSVC/Version/Version.hpp"

namespace Io { namespace Oidis { namespace Microcore { namespace MSVC {
    std::mutex protection;
    CComPtr<DragAndDrop::DropTargetWin> dropTarget = nullptr;

    const char * __cdecl GetVersion() {
        return Io::Oidis::Microcore::MSVC::Version::currentVersion;
    }

    bool __cdecl Initialize(const char *$id,
                            Callbacks::OnDrop $onDrop,
                            Callbacks::OnDragLeave $onDragLeave,
                            Callbacks::OnDragOver $onDragOver,
                            Callbacks::OnDragEnter $onDragEnter,
                            HWND $hwnd) {
        std::lock_guard<decltype(protection)> lock{protection};

        LOG(INFO) << "Initialize with id: " << $id << " and HWND: " << $hwnd;

        if ($onDrop == nullptr || $onDragLeave == nullptr || $onDragOver == nullptr || $onDragEnter == nullptr) {
            LOG(ERROR) << "Some of the callbacks are not call-able";

            return false;
        }

        dropTarget = DragAndDrop::DropTargetWin::Create($id, $onDrop, $onDragLeave, $onDragOver, $onDragEnter, $hwnd);

        if (dropTarget == nullptr) {
            LOG(ERROR) << "Failed to create a DropTargetWin instance";

            return false;
        }

        const auto errorCode = RegisterDragDrop($hwnd, dropTarget);
        if (errorCode == S_OK) {
            LOG(INFO) << "Registered as a drag-and-drop target";

            return true;
        } else {
            LOG(ERROR) << "Failed to register as a drag and drop target, error: " << errorCode;

            return false;
        }
    }

    bool __cdecl Destroy(HWND $hwnd) {
        std::lock_guard<decltype(protection)> lock{protection};

        LOG(INFO) << "Destroy for HWND: " << $hwnd;

        const auto errorCode = RevokeDragDrop($hwnd);
        if (errorCode == S_OK) {
            LOG(INFO) << "Target revoked";

            dropTarget = nullptr;

            return true;
        } else {
            LOG(ERROR) << "Failed to revoke drag and drop target, error: " << errorCode;

            return false;
        }
    }

    bool __cdecl HasTarget() {
        std::lock_guard<decltype(protection)> lock{ protection };

        return dropTarget != nullptr;
    }

    CefBrowserHost::DragOperationsMask __cdecl StartDragging(Structures::DragData $dragData,
                                                             CefRenderHandler::DragOperationsMask $allowedOps,
                                                             int $x,
                                                             int $y) {
        std::lock_guard<decltype(protection)> lock{ protection };

        const CefBrowserHost::DragOperationsMask mask = dropTarget->StartDragging($dragData, $allowedOps, $x, $y);

        return mask;
    }

}}}}

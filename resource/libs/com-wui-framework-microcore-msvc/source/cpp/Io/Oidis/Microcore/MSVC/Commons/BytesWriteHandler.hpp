/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014 The Chromium Embedded Framework Authors
 * Copyright (c) 2018 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_MICROCORE_MSVC_COMMONS_BYTESWRITEHANDLER_HPP_
#define IO_OIDIS_MICROCORE_MSVC_COMMONS_BYTESWRITEHANDLER_HPP_

#include <base/cef_lock.h>
#include <cef_stream.h>

namespace Io { namespace Oidis { namespace Microcore { namespace MSVC { namespace Commons {
    class BytesWriteHandler : public CefWriteHandler {
     public:
         explicit BytesWriteHandler(size_t $grow);

         ~BytesWriteHandler();

         size_t Write(const void *$ptr, size_t $size, size_t $n) override;

         int Seek(int64 $offset, int $whence) override;

         int64 Tell() override;

         int Flush() override;

         bool MayBlock() override { return false; }

         void *GetData() { return data_; }

         int64 GetDataSize() { return offset_; }

     private:
         size_t Grow(size_t $size);

         size_t grow_;
         void *data_;
         int64 datasize_;
         int64 offset_;
         base::Lock lock_;

         IMPLEMENT_REFCOUNTING(BytesWriteHandler);
         DISALLOW_COPY_AND_ASSIGN(BytesWriteHandler);
    };
}}}}}

#endif  // IO_OIDIS_MICROCORE_MSVC_COMMONS_BYTESWRITEHANDLER_HPP_

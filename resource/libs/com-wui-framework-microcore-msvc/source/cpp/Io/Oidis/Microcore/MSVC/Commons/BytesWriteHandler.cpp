/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014 The Chromium Embedded Framework Authors
 * Copyright (c) 2018 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "MSVC/Commons/BytesWriteHandler.hpp"

namespace Io { namespace Oidis { namespace Microcore { namespace MSVC { namespace Commons {
    BytesWriteHandler::BytesWriteHandler(size_t $grow)
        : grow_($grow), datasize_($grow), offset_(0) {
        DCHECK_GT($grow, 0U);
        this->data_ = malloc($grow);
        DCHECK(this->data_ != NULL);
    }

    BytesWriteHandler::~BytesWriteHandler() {
        if (this->data_) {
            free(this->data_);
        }
    }

    size_t BytesWriteHandler::Write(const void *$ptr, size_t $size, size_t $n) {
        base::AutoLock lock_scope(lock_);
        size_t rv;
        if (this->offset_ + static_cast<int64>($size * $n) >= this->datasize_ && this->Grow($size * $n) == 0) {
            rv = 0;
        } else {
            memcpy(reinterpret_cast<char*>(this->data_) + this->offset_, $ptr, $size * $n);
            this->offset_ += $size * $n;
            rv = $n;
        }

        return rv;
    }

    int BytesWriteHandler::Seek(int64 $offset, int $whence) {
        int rv = -1L;
        base::AutoLock lock_scope(lock_);
        switch ($whence) {
        case SEEK_CUR:
            if (this->offset_ + $offset > this->datasize_ || this->offset_ + $offset < 0) {
                break;
            }
            this->offset_ += $offset;
            rv = 0;
            break;
        case SEEK_END: {
            const int64 offset_abs = std::abs($offset);
            if (offset_abs > this->datasize_) {
                break;
            }
            this->offset_ = this->datasize_ - offset_abs;
            rv = 0;
            break;
        }
        case SEEK_SET:
            if ($offset > this->datasize_ || $offset < 0) {
                break;
            }
            this->offset_ = $offset;
            rv = 0;
            break;
        }

        return rv;
    }

    int64 BytesWriteHandler::Tell() {
        base::AutoLock lock_scope(lock_);
        return this->offset_;
    }

    int BytesWriteHandler::Flush() {
        return 0;
    }

    size_t BytesWriteHandler::Grow(size_t $size) {
        base::AutoLock lock_scope(lock_);
        size_t rv;
        size_t s = ($size > this->grow_ ? $size : this->grow_);
        void *tmp = realloc(this->data_, this->datasize_ + s);
        DCHECK(tmp != NULL);
        if (tmp) {
            this->data_ = tmp;
            this->datasize_ += s;
            rv = this->datasize_;
        } else {
            rv = 0;
        }

        return rv;
    }
}}}}}

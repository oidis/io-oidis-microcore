/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014 The Chromium Embedded Framework Authors
 * Copyright (c) 2018 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_MICROCORE_MSVC_DRAGANDDROP_DRAGANDDROP_HPP_
#define IO_OIDIS_MICROCORE_MSVC_DRAGANDDROP_DRAGANDDROP_HPP_

#include <atlcomcli.h>
#include <objidl.h>
#include <cstdio>

#include <cef_browser.h>
#include <cef_drag_data.h>
#include <cef_render_handler.h>

#include <internal/cef_ptr.h>

#include "MSVC/Callbacks/Callbacks.hpp"
#include "MSVC/Structures/DragData.hpp"

namespace Io { namespace Oidis { namespace Microcore { namespace MSVC { namespace DragAndDrop {
#define DEFAULT_QUERY_INTERFACE(__Class)                            \
  HRESULT __stdcall QueryInterface(const IID &$iid, void **$object) { \
    *$object = NULL;                                                 \
    if (IsEqualIID($iid, IID_IUnknown)) {                            \
      IUnknown* obj = this;                                         \
      *$object = obj;                                                \
    } else if (IsEqualIID($iid, IID_##__Class)) {                    \
      __Class* obj = this;                                          \
      *$object = obj;                                                \
    } else {                                                        \
      return E_NOINTERFACE;                                         \
    }                                                               \
    AddRef();                                                       \
    return S_OK;                                                    \
  }

#define IUNKNOWN_IMPLEMENTATION                     \
  ULONG __stdcall AddRef() { return ++ref_count_; } \
  ULONG __stdcall Release() {                       \
    if (--ref_count_ == 0) {                        \
      delete this;                                  \
      return 0U;                                    \
    }                                               \
    return ref_count_;                              \
  }                                                 \
                                                    \
 protected:                                         \
     ULONG ref_count_;

    class DropTargetWin : public IDropTarget {
     public:
         static CComPtr<DropTargetWin> Create(const std::string &$id,
                                              Callbacks::OnDrop $onDrop,
                                              Callbacks::OnDragLeave $onDragLeave,
                                              Callbacks::OnDragOver $onDragOver,
                                              Callbacks::OnDragEnter $onDragEnter,
                                              HWND $hWnd);

         CefBrowserHost::DragOperationsMask StartDragging(Structures::DragData $dragData,
                                                          CefRenderHandler::DragOperationsMask $allowedOps,
                                                          int $x,
                                                          int $y);

         HRESULT __stdcall DragEnter(IDataObject *$dataObject, DWORD $keyState, POINTL $cursorPosition, DWORD *$effect);

         HRESULT __stdcall DragOver(DWORD $keyState, POINTL $cursorPosition, DWORD *$effect);

         HRESULT __stdcall DragLeave();

         HRESULT __stdcall Drop(IDataObject *$dataObject, DWORD $keyState, POINTL $cursorPosition, DWORD *$effect);

         DEFAULT_QUERY_INTERFACE(IDropTarget)
         IUNKNOWN_IMPLEMENTATION

     protected:
         DropTargetWin(const std::string &$id,
                       Callbacks::OnDrop $onDrop,
                       Callbacks::OnDragLeave $onDragLeave,
                       Callbacks::OnDragOver $onDragOver,
                       Callbacks::OnDragEnter $onDragEnter,
                       HWND $hWnd)
            : ref_count_(0),
              id($id),
              onDrop($onDrop),
              onDragLeave($onDragLeave),
              onDragOver($onDragOver),
              onDragEnter($onDragEnter),
              hWnd($hWnd) {}

         virtual ~DropTargetWin() = default;

     private:
         HWND hWnd;
         CefRefPtr<CefDragData> current_drag_data_;
         std::string id;

         Callbacks::OnDrop onDrop = nullptr;
         Callbacks::OnDragLeave onDragLeave = nullptr;
         Callbacks::OnDragOver onDragOver = nullptr;
         Callbacks::OnDragEnter onDragEnter = nullptr;
    };

    class DropSourceWin : public IDropSource {
     public:
         static CComPtr<DropSourceWin> Create();

          HRESULT __stdcall GiveFeedback(DWORD $dwEffect);

          HRESULT __stdcall QueryContinueDrag(BOOL $fEscapePressed, DWORD $grfKeyState);

          DEFAULT_QUERY_INTERFACE(IDropSource)
          IUNKNOWN_IMPLEMENTATION

     protected:
          DropSourceWin() : ref_count_(0) {}

          virtual ~DropSourceWin() = default;
    };

    class DragEnumFormatEtc : public IEnumFORMATETC {
     public:
         static HRESULT CreateEnumFormatEtc(UINT $cfmt, FORMATETC *$afmt, IEnumFORMATETC **$ppEnumFormatEtc);

         HRESULT __stdcall Next(ULONG $celt,
                             FORMATETC *$pFormatEtc,
                             ULONG *$pceltFetched);
         HRESULT __stdcall Skip(ULONG $celt);
         HRESULT __stdcall Reset();
         HRESULT __stdcall Clone(IEnumFORMATETC **$ppEnumFormatEtc);

         DragEnumFormatEtc(FORMATETC *$pFormatEtc, int $nNumFormats);
         virtual ~DragEnumFormatEtc();

         static void DeepCopyFormatEtc(FORMATETC *$dest, FORMATETC *$source);

         DEFAULT_QUERY_INTERFACE(IEnumFORMATETC)
         IUNKNOWN_IMPLEMENTATION

     private:
         ULONG m_nIndex = 0; // current enumerator index
         ULONG m_nNumFormats = 0; // number of FORMATETC members
         FORMATETC *m_pFormatEtc = nullptr; // array of FORMATETC objects
    };

    class DataObjectWin : public IDataObject {
     public:
         static CComPtr<DataObjectWin> Create(FORMATETC *$fmtetc,
                                              STGMEDIUM *$stgmed,
                                              int $count);

         // IDataObject members
         HRESULT __stdcall GetDataHere(FORMATETC *$pFormatEtc, STGMEDIUM *$pmedium);
         HRESULT __stdcall QueryGetData(FORMATETC *$pFormatEtc);
         HRESULT __stdcall GetCanonicalFormatEtc(FORMATETC *$pFormatEct, FORMATETC *$pFormatEtcOut);
         HRESULT __stdcall SetData(FORMATETC *$pFormatEtc, STGMEDIUM *$pMedium, BOOL $fRelease);
         HRESULT __stdcall DAdvise(FORMATETC *$pFormatEtc, DWORD $advf, IAdviseSink *, DWORD *);
         HRESULT __stdcall DUnadvise(DWORD $dwConnection);
         HRESULT __stdcall EnumDAdvise(IEnumSTATDATA **$ppEnumAdvise);

         HRESULT __stdcall EnumFormatEtc(DWORD $dwDirection, IEnumFORMATETC **$ppEnumFormatEtc);
         HRESULT __stdcall GetData(FORMATETC *$pFormatEtc, STGMEDIUM *$pMedium);

         DEFAULT_QUERY_INTERFACE(IDataObject)
         IUNKNOWN_IMPLEMENTATION

     protected:
         int m_nNumFormats = 0;
         FORMATETC *m_pFormatEtc = nullptr;
         STGMEDIUM *m_pStgMedium = nullptr;

         static HGLOBAL DupGlobalMem(HGLOBAL $hMem);

         int LookupFormatEtc(FORMATETC *$pFormatEtc);

         explicit DataObjectWin(FORMATETC *$fmtetc, STGMEDIUM *$stgmed, int $count);
         virtual ~DataObjectWin() = default;
    };

    }
}}}}

#endif  // IO_OIDIS_MICROCORE_MSVC_DRAGANDDROP_DRAGANDDROP_HPP_

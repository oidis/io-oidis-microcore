/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_MICROCORE_MSVC_VERSION_VERSION_HPP_
#define IO_OIDIS_MICROCORE_MSVC_VERSION_VERSION_HPP_

namespace Io { namespace Oidis { namespace Microcore { namespace MSVC { namespace Version {
    constexpr const char *currentVersion = "2018.3.1.0";
}}}}}

#endif  // IO_OIDIS_MICROCORE_MSVC_VERSION_VERSION_HPP_

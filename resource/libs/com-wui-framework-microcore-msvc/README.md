# com-wui-framework-microcore-msvc v2019.2.0

> Library that implements MSVC-specific functionality of microcore and provides bridge with microcore (built under the GCC).

## History

### v2018.3.1.0
Improved structure. Proper version management.
### v2018.3.0.0
Initial release.

## License

This software is owned or controlled by Oidis.
The use of this software is governed by the BSD-3-Clause Licence distributed with this material.
  
See the `LICENSE.txt` file for more details.

---

Author Karel Burda
Copyright (c) 2018-2019 [NXP](http://nxp.com/)
Copyright (c) 2019 [Oidis](https://www.oidis.org/)

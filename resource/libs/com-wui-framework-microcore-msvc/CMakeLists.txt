# * ********************************************************************************************************* *
# *
# * Copyright (c) 2018 NXP
# * Copyright (c) 2019 Oidis
# *
# * SPDX-License-Identifier: BSD-3-Clause
# * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
# * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
# *
# * ********************************************************************************************************* *

# Usage: cmake -B<build-directory> -H. -DCMAKE_INSTALL_PREFIX:PATH=<install-path> -DCEF_BINARY_PATH:PATH=<path-to-binary-distribution> -DUSE_CEF_CMAKE_FUNCTIONS:BOOL=<ON/OFF>
# Paths have to be absolute
# Example: cmake -Bbuild -H. -DCMAKE_INSTALL_PREFIX:PATH=/foo/build/target -DCEF_BINARY_PATH:PATH=/bar/dependencies/cef -DUSE_CEF_CMAKE_FUNCTIONS:BOOL=ON
# Other definitions are optional, but often the "-DUSE_SANDBOX:BOOL=OFF" and forced "-DUSE_ATL:BOOL=ON" is desired
# This project requires at least MSVC 1914 with Active Template Library support and Windows 10 SDK

cmake_minimum_required(VERSION 3.8)

project(com-wui-framework-microcore-msvc VERSION 2019.2.0 LANGUAGES CXX)

if (NOT CEF_BINARY_PATH)
    message(FATAL_ERROR "CEF_BINARY_PATH not given")
endif ()

if (${MSVC_VERSION} LESS 1914)
    message(FATAL_ERROR "MSVC 1914 and higher is required")
endif ()

option(USE_CEF_CMAKE_FUNCTIONS "Specifies whether to use additonal CMake functions from the CEF" ON)

if (USE_CEF_CMAKE_FUNCTIONS)
    set(CMAKE_CONFIGURATION_TYPES Debug Release)
    set_property(GLOBAL PROPERTY OS_FOLDERS ON)
    set(CEF_ROOT ${CEF_BINARY_PATH})
    set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CEF_ROOT}/cmake")
    find_package(CEF REQUIRED)
    add_subdirectory(${CEF_LIBCEF_DLL_WRAPPER_PATH} libcef_dll_wrapper)
endif ()

add_library(${PROJECT_NAME} SHARED)

target_sources(${PROJECT_NAME}
                    PUBLIC
                        ${CMAKE_CURRENT_SOURCE_DIR}/source/cpp/Io/Oidis/Microcore/MSVC/MicrocoreMSVC.hpp

                    PRIVATE
                        ${CMAKE_CURRENT_SOURCE_DIR}/source/cpp/Io/Oidis/Microcore/MSVC/MicrocoreMSVC.cpp
                        ${CMAKE_CURRENT_SOURCE_DIR}/source/cpp/Io/Oidis/Microcore/MSVC/Callbacks/Callbacks.hpp
                        ${CMAKE_CURRENT_SOURCE_DIR}/source/cpp/Io/Oidis/Microcore/MSVC/Commons/BytesWriteHandler.hpp
                        ${CMAKE_CURRENT_SOURCE_DIR}/source/cpp/Io/Oidis/Microcore/MSVC/Commons/BytesWriteHandler.cpp
                        ${CMAKE_CURRENT_SOURCE_DIR}/source/cpp/Io/Oidis/Microcore/MSVC/DragAndDrop/DragAndDrop.hpp
                        ${CMAKE_CURRENT_SOURCE_DIR}/source/cpp/Io/Oidis/Microcore/MSVC/DragAndDrop/DragAndDrop.cpp
                        ${CMAKE_CURRENT_SOURCE_DIR}/source/cpp/Io/Oidis/Microcore/MSVC/Structures/Conversions.hpp
                        ${CMAKE_CURRENT_SOURCE_DIR}/source/cpp/Io/Oidis/Microcore/MSVC/Structures/DragData.hpp
                        ${CMAKE_CURRENT_SOURCE_DIR}/source/cpp/Io/Oidis/Microcore/MSVC/Version/Version.hpp)

target_include_directories(${PROJECT_NAME}
                                PRIVATE
                                    ${CMAKE_CURRENT_SOURCE_DIR}/source/cpp/Io/Oidis/Microcore)

target_include_directories(${PROJECT_NAME}
                                SYSTEM
                                    PRIVATE
                                        ${CEF_BINARY_PATH}/include)

target_link_libraries(${PROJECT_NAME}
                            PRIVATE
                                libcef_lib
                                libcef_dll_wrapper)

target_compile_features(${PROJECT_NAME}
                            PRIVATE
                                cxx_std_14)

target_compile_options(${PROJECT_NAME}
                            PRIVATE
                                $<$<CXX_COMPILER_ID:MSVC>:/MP>)

set_target_properties(${PROJECT_NAME}
                            PROPERTIES
                                VERSION ${PROJECT_VERSION}
                                OUTPUT_NAME ${PROJECT_NAME}-${PROJECT_VERSION})

if (USE_CEF_CMAKE_FUNCTIONS)
    set(CEF_TARGET ${PROJECT_NAME})
    ADD_LOGICAL_TARGET("libcef_lib" "${CEF_LIB_DEBUG}" "${CEF_LIB_RELEASE}")
    SET_CEF_TARGET_OUT_DIR()
    add_dependencies(${CEF_TARGET} libcef_dll_wrapper)
    SET_EXECUTABLE_TARGET_PROPERTIES(${CEF_TARGET})
    PRINT_CEF_CONFIG()
endif ()

configure_file(${CMAKE_CURRENT_SOURCE_DIR}/source/cpp/Io/Oidis/Microcore/MSVC/Version/Version.hpp.in
               ${CMAKE_CURRENT_SOURCE_DIR}/source/cpp/Io/Oidis/Microcore/MSVC/Version/Version.hpp
               @ONLY)

install(TARGETS ${PROJECT_NAME}
        DESTINATION ${CMAKE_INSTALL_PREFIX}
        LIBRARY)

install(FILES
            ${CMAKE_CURRENT_SOURCE_DIR}/LICENSE.txt
            ${CMAKE_CURRENT_SOURCE_DIR}/SW-Content-Register.txt
            ${CMAKE_CURRENT_SOURCE_DIR}/README.md
        DESTINATION ${CMAKE_INSTALL_PREFIX})
